#!/bin/bash

# set -x # Print commands.
set -e # Stop on error.

usage() {
  echo "usage: $0 GPU_ID NET DATASET" 1>&2
}

export PYTHONUNBUFFERED="True"

GPU_ID=$1
NET=$2
DATASET=$3

TAG="default"
EXTRA_SET_OPTIONS=""


if [ -z "$GPU_ID" ]; then
  echo "Missing arg: GPU_ID." 1>&2
  usage
  exit 1
fi

if [ -z "$DATASET" ]; then
  echo "Missing arg: DATASET." 1>&2
  usage
  exit 1
fi

# if [ -z "$NET" ]; then
if echo "$NET" | perl -e '<> !~ m/^(vgg16|res50|res101|res152)$/ ? exit 0 : exit 1'; then
  echo "Missing arg: NET." 1>&2
  usage
  exit 1
fi

case ${DATASET} in
  pascal_voc)
    TRAIN_IMDB="voc_2007_trainval"
    TEST_IMDB="voc_2007_test"
    STEPSIZE="[50000]"
    ITERS=70000
    ANCHORS="[8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  pascal_voc_0712)
    TRAIN_IMDB="voc_2007_trainval+voc_2012_trainval"
    TEST_IMDB="voc_2007_test"
    STEPSIZE="[80000]"
    ITERS=110000
    ANCHORS="[8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  coco)
    TRAIN_IMDB="coco_2014_train+coco_2014_valminusminival"
    TEST_IMDB="coco_2014_minival"
    STEPSIZE="[350000]"
    ITERS=490000
    ANCHORS="[4,8,16,32]"
    RATIOS="[0.5,1,2]"
    ;;
  google_nrg_charlie)
    TRAIN_IMDB="google_nrg_train"
    TEST_IMDB="google_nrg_test"
    STEPSIZE="[350000]" # Reduce the learning rate after n. iterations.
    ITERS=490000 # 8120 training images * (+-)60 epochs
    ANCHORS="[0.5,1.0,1.5,2.0]"
    RATIOS="[1,2,3]"
    EXTRA_SET_OPTIONS="TRAIN.BG_THRESH_LO 0.00"
    ;;
  google_nrg_delta)
    # Training with the anchors Thiago suggested. But I forgot to turn KEEP_AREA off.
    TRAIN_IMDB="google_nrg_train"
    TEST_IMDB="google_nrg_test"
    STEPSIZE="[50000]" # Reduce the learning rate after n. iterations.
    ITERS=70000 # 8120 training images
    ANCHORS="[1,2,4]"
    RATIOS="[1,2,3]"
    EXTRA_SET_OPTIONS="TRAIN.BG_THRESH_LO 0.00"
    ;;
  google_nrg_echo)
    # Training with the anchors Thiago suggested. The anchors will be created by not preserving the area.
    TAG="echo"
    TRAIN_IMDB="google_nrg_train"
    TEST_IMDB="google_nrg_test"
    STEPSIZE="[50000]" # Reduce the learning rate after n. iterations.
    ITERS=70000 # 8120 training images
    ANCHORS="[1,2,4]"
    RATIOS="[1,2,3]"
    EXTRA_SET_OPTIONS="TRAIN.BG_THRESH_LO 0.00 KEEP_AREA False"
    ;;
  google_nrg_golf)
    # Training with the anchors Thiago suggested. The anchors will be created by not preserving the area.
    TAG="golf"
    TRAIN_IMDB="google_nrg_train"
    TEST_IMDB="google_nrg_test"
    STEPSIZE="[50000]" # Reduce the learning rate after n. iterations.
    ITERS=70000 # 8120 training images
    ANCHORS="[1.0,1.5,2.0,4.0]"
    RATIOS="[1,2,3]"
    EXTRA_SET_OPTIONS="TRAIN.BG_THRESH_LO 0.00"
    ;;
   google_nrgy_alpha)
    # Training with the anchors Thiago suggested. The anchors will be created by not preserving the area.
    TAG="alpha"
    TRAIN_IMDB="google_nrgy_train"
    TEST_IMDB="google_nrgy_test"
    STEPSIZE="[50000]" # Reduce the learning rate after n. iterations.
    ITERS=70000 # 8120 training images
    ANCHORS="[1,2,4]"
    RATIOS="[1,2,3]"
    EXTRA_SET_OPTIONS="TRAIN.BG_THRESH_LO 0.00 KEEP_AREA False"
    ;;
  *)
    echo "Invalid choice of DATASET." 1>&2
    usage
    exit 1
    ;;
esac

LOG="experiments/logs/${NET}_${TRAIN_IMDB}_${TAG}_${NET}.txt.`date +'%Y-%m-%d_%H-%M-%S'`"
exec &> >(tee -a "$LOG")
echo "Logging output to $LOG"

NET_FINAL=output/${NET}/${TRAIN_IMDB}/${TAG}/${NET}_faster_rcnn_iter_${ITERS}.ckpt

if [ ! -f ${NET_FINAL}.index ]; then
  CUDA_VISIBLE_DEVICES=${GPU_ID} time python ./tools/trainval_net.py \
    --weight data/imagenet_weights/${NET}.ckpt \
    --imdb ${TRAIN_IMDB} \
    --imdbval ${TEST_IMDB} \
    --iters ${ITERS} \
    --cfg experiments/cfgs/${NET}.yml \
    --tag ${TAG} \
    --net ${NET} \
    --set ANCHOR_SCALES ${ANCHORS} ANCHOR_RATIOS ${RATIOS} \
    TRAIN.STEPSIZE ${STEPSIZE} ${EXTRA_ARGS} ${EXTRA_SET_OPTIONS}
fi

# ./experiments/scripts/test_faster_rcnn.sh $@
