#!/bin/bash

test_all_xtop_rules() {
	bash possatti/run_xtop.sh --rule xtop "$1" "$2" "$3"
	bash possatti/run_xtop.sh --rule fat_tl "$1" "$2" "$3"
	bash possatti/run_xtop.sh --rule fat_twins "$1" "$2" "$3"
}

test_all_xtop_rules 0 Y2018_10_NRGR "output/res101/y2018_10_google_aug00_nrgr_train/golf_s3/res101_faster_rcnn_iter_70000.ckpt" # lcad52 # vini
test_all_xtop_rules 0 Y2018_10_NRGR "output/res101/y2018_10_google_aug00_nrgr_train/golf_s4/res101_faster_rcnn_iter_70000.ckpt" # lcad52 # vini
test_all_xtop_rules 0 Y2018_10_NRGR "output/res101/y2018_10_google_aug00_nrgr_train/golf_s5/res101_faster_rcnn_iter_70000.ckpt" # lcad52 # vini
