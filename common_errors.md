## Instalação

Na hora de instalar, eu tava achando que instalava com `python setup.py install`, mas o correto é entrar em `lib/` e dar `make`, pq aí ele vai compilar os arquivos necessário *inplace*.

Lembre também de modificar o parâmetro `arch` do `setup.py` para corresponder com a sua placa de vídeo. Refência para as placas que eu já usei:
 - Titan X: `-arch=sm_52`
 - GeForce GTX 660: `-arch=sm_30`

--------------------------------------------------------------------------------

## PDB prompt

Antes de qualquer *step* o programa para num prompt `(Pdb)` (Python Debbuger), e fica ali pra sempre.

Para solucionar isso, é só diminuir o `TRAIN.BG_THRESH_LO` para `0.00`. Você pode modificar o `config.py`, ou alterar o `train_faster_rcnn.sh` para passar essa configuração no `--set` do `python ./tools/trainval_net.py`.

--------------------------------------------------------------------------------

## CUB reduce errorPTX JIT compiler library not found

### Problema
```
InternalError (see above for traceback): CUB reduce errorPTX JIT compiler library not found
    [[Node: resnet_v1_101_2/block3/unit_1/bottleneck_v1/conv2/kernel/Regularizer/l2_regularizer/L2Loss = L2Loss[T=DT_FLOAT, _class=["loc:@resne...egularizer"], _device="/job:localhost/replica:0/task:0/device:GPU:0"](resnet_v1_101/block3/unit_1/bottleneck_v1/conv2/weights/read)]]
```

### Solução

Eu tive esse erro ao executar na máquina do Rodrigo. Aparentemente havia aluns links simbólicos quebrados, em `/usr/lib/nvidia-384/`. Eu tive que refazer o link: `/usr/lib/nvidia-384/libnvidia-ptxjitcompiler.so.1 -> libnvidia-ptxjitcompiler.so.384.130`.

 - https://stackoverflow.com/questions/47258882/theano-gpu-support-ptx-jit-compiler-library-not-found

--------------------------------------------------------------------------------

## CUB reduce errorPTX JIT compiler library not found

### Problema
```
InvalidArgumentError (see above for traceback): Incompatible shapes: [256,12] vs. [0,12]
         [[Node: gradients/LOSS_default/mul_9_grad/Mul_1 = Mul[T=DT_FLOAT, _device="/job:localhost/replica:0/task:0/device:GPU:0"](gradients/LOSS_default/Sum_1_grad/Tile, resnet_v1_101_3/rpn_rois/proposal_target/_1323)]]
         [[Node: LOSS_default/Mean_2/_1347 = _Recv[client_terminated=false, recv_device="/job:localhost/replica:0/task:0/device:CPU:0", send_device="/job:localhost/replica:0/task:0/device:GPU:0", send_device_incarnation=1, tensor_name="edge_3408_LOSS_default/Mean_2", tensor_type=DT_FLOAT, _device="/job:localhost/replica:0/task:0/device:CPU:0"]()]]
```

### Solução

WIP (Só tive esse problema uma vez.)

--------------------------------------------------------------------------------

## Failed to create session

### Problema
```
2018-08-21 15:03:59.268954: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1030] Found device 0 with properties:
name: TITAN Xp major: 6 minor: 1 memoryClockRate(GHz): 1.582
pciBusID: 0000:25:00.0
totalMemory: 11.90GiB freeMemory: 11.74GiB
2018-08-21 15:03:59.334186: E tensorflow/core/common_runtime/direct_session.cc:170] Internal: failed initializing StreamExecutor for CUDA device ordinal 1: Internal: failed call to cuDevicePrimaryCtxRetain: CUDA_ERROR_INVALID_DEVICE
...
  File "/home/lucaspossatti/venvs/tf/lib/python3.4/site-packages/tensorflow/python/client/session.py", line 622, in __init__
    self._session = tf_session.TF_NewDeprecatedSession(opts, status)
  File "/home/lucaspossatti/venvs/tf/lib/python3.4/site-packages/tensorflow/python/framework/errors_impl.py", line 473, in __exit__
    c_api.TF_GetCode(self.status.status))
tensorflow.python.framework.errors_impl.InternalError: Failed to create session.
```

### Solução
Apesar de ter detectado apenas a Titan X, eu acho que ele estava na verdade tentando usar a Quadro. Antes de rodar o script, eu executei `export CUDA_VISIBLE_DEVICES=0` para tentar forçar a Titan X e funcionou. Lembrando que o índice do `CUDA_VISIBLE_DEVICES` não tem nada a ver com o índice da saída do `nvidia-smi`.
