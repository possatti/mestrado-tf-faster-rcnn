Total images: 3330
Confusion matrix:
[[ 328    0    0]
 [ 265  935   11]
 [ 168   39 1189]]
Accuracy per class:
 - None: 1.0
 - Red: 0.7720891824938068
 - Green: 0.8517191977077364
Micro accuracy: 0.8354344122657581
Macro accuracy: 0.8746027934005144
