Total images: 9351
Confusion matrix:
[[1914   41  194]
 [ 815 2543  636]
 [ 273  412 2523]]
Accuracy per class:
 - None: 0.8906468124709167
 - Red: 0.6367050575863795
 - Green: 0.7864713216957606
Micro accuracy: 0.7464442305635761
Macro accuracy: 0.7712743972510191
