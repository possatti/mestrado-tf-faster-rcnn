Total images: 3330
Confusion matrix:
[[ 324    0    4]
 [ 186 1021    4]
 [ 216    7 1173]]
Accuracy per class:
 - None: 0.9878048780487805
 - Red: 0.8431048720066061
 - Green: 0.8402578796561605
Micro accuracy: 0.8579216354344122
Macro accuracy: 0.890389209903849
