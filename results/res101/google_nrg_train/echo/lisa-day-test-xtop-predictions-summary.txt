Total images: 3330
Confusion matrix:
[[ 328    0    0]
 [ 164 1040    7]
 [ 178   19 1199]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8587943848059455
 - Green: 0.8588825214899714
Micro accuracy: 0.8746166950596252
Macro accuracy: 0.9058923020986388
