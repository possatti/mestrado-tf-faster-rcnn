
## GOOGLE_TL_TEST

Total images: 4898
Confusion matrix:
[[1050   16   83]
 [  41  513   13]
 [  90   24 3068]]
Accuracy per class:
 - None: 0.9138381201044387
 - Red: 0.9047619047619048
 - Green: 0.9641734758013828
Micro accuracy: 0.9454879542670478
Macro accuracy: 0.927591166889242

## IARA

Total images: 10346
Confusion matrix:
[[2897   25   32]
 [   9 3686    0]
 [  21   34 3640]]
Accuracy per class:
 - None: 0.9807041299932295
 - Red: 0.9975642760487144
 - Green: 0.9851150202976996
Micro accuracy: 0.9883023975251354
Macro accuracy: 0.9877944754465479

## LARA

Total images: 9351
Confusion matrix:
[[2080   14   55]
 [2391 1213  390]
 [ 800  495 1913]]
Accuracy per class:
 - None: 0.9678920428106096
 - Red: 0.30370555833750623
 - Green: 0.5963216957605985
Micro accuracy: 0.5567319003315153
Macro accuracy: 0.622639765636238

## LISA_DAY_TRAIN


## LISA_DAY_TEST

Total images: 3330
Confusion matrix:
[[ 328    0    0]
 [ 254  941   16]
 [ 197   13 1186]]
Accuracy per class:
 - None: 1.0
 - Red: 0.7770437654830719
 - Green: 0.8495702005730659
Micro accuracy: 0.8364565587734242
Macro accuracy: 0.8755379886853794

## AMBIGUOUS_CASES

