Total images: 9351
Confusion matrix:
[[2080   14   55]
 [2391 1213  390]
 [ 800  495 1913]]
Accuracy per class:
 - None: 0.9678920428106096
 - Red: 0.30370555833750623
 - Green: 0.5963216957605985
Micro accuracy: 0.5567319003315153
Macro accuracy: 0.622639765636238
