Comparação dos casos difíceis Squeezenet vs. Faster RCNN
========================================================

## IARA

Squeezenet:
    0       3        2
    0       0        0
    1       40      97
Macro: 0.351449275362

Faster R-CNN:
[[  0   5   0]
 [  0   0   0]
 [  0  27 111]]
Macro accuracy: 0.40217391304347827

## LARA:

Squeezenet:
    23       2        58
    35       48      398
    7         16      574
Macro: 0.446124856793

Faster R-CNN:
[[ 23   2  58]
 [  0  92 389]
 [  2 460 135]]
Macro accuracy: 0.23150242608982088

## LISA:

Squeezenet:
    0       0         0
    0       296     2801
    0       2         293
Macro: 0.544398351603

Faster R-CNN:
[[   0    0    0]
 [   1 2815  281]
 [   0  140  155]]
Macro accuracy: 0.7171839341516941
