Alpha
-----

Resultados com a predição do semáforo da via no top da imagem ("xtop") usando a Faster R-CNN (ResNet 101, modelo "alpha") treinado no "google_nrg_train".

Parametros dos testes:
	- nms_thresh: 0.3
	- conf_thresh: 0.8

### Teste no "google_nrg_test":

Total images: 4898
Confusion matrix:
[[1040   21   88]
 [  38  514   15]
 [  71   23 3088]]
Accuracy per class:
 - None: 0.9051348999129678
 - Red: 0.9065255731922398
 - Green: 0.9704588309239472
Micro accuracy: 0.9477337688852593
Macro accuracy: 0.9273731013430516

### Teste na IARA (Traffic Lights e My logs):

Total images: 10346
Confusion matrix:
[[2802   42  110]
 [   1 3694    0]
 [   7   48 3640]]
Accuracy per class:
 - None: 0.9485443466486121
 - Red: 0.9997293640054127
 - Green: 0.9851150202976996
Micro accuracy: 0.979891724671307
Macro accuracy: 0.9777962436505748

### Teste na LARA:

Total images: 9351
Confusion matrix:
[[2080    8   61]
 [2268 1335  391]
 [ 685  482 2041]]
Accuracy per class:
 - None: 0.9678920428106096
 - Red: 0.3342513770655984
 - Green: 0.6362219451371571
Micro accuracy: 0.5834670088760561
Macro accuracy: 0.6461217883377884

### Teste na LISA (day_train):

Total images: 13067
Confusion matrix:
[[ 594    0   24]
 [ 286 8883  294]
 [ 131  180 2115]]
Accuracy per class:
 - None: 0.9611650485436893
 - Red: 0.9387086547606467
 - Green: 0.8718054410552349
Micro accuracy: 0.9268409690573279
Macro accuracy: 0.9238930481198571

### Teste na LISA (day_test):

Total images: 3330
Confusion matrix:
[[ 327    0    1]
 [ 243  945   23]
 [ 164   14 1218]]
Accuracy per class:
 - None: 0.9969512195121951
 - Red: 0.7803468208092486
 - Green: 0.8724928366762178
Micro accuracy: 0.848381601362862
Macro accuracy: 0.8832636256658871

### Teste no `traffic_lights_ambiguous.csv`:

Total images: 4696
Confusion matrix:
[[  23    7   58]
 [   1 2907  670]
 [   2  627  401]]
Accuracy per class:
 - None: 0.26136363636363635
 - Red: 0.8124650642817216
 - Green: 0.38932038834951455
Micro accuracy: 0.7093270868824532
Macro accuracy: 0.4877163629982908

### Teste no `iara_nrg_tl_ambiguous.csv`:

Total images: 143
Confusion matrix:
[[  0   5   0]
 [  0   0   0]
 [  0  27 111]]
Accuracy per class:
 - None: 0.0
 - Red: nan
 - Green: 0.8043478260869565
Micro accuracy: 0.7762237762237763
Macro accuracy: nan (0.40217391304347827)

### Teste no `lara_nrg_tl_ambiguous.csv`:

Total images: 1161
Confusion matrix:
[[ 23   2  58]
 [  0  92 389]
 [  2 460 135]]
Accuracy per class:
 - None: 0.27710843373493976
 - Red: 0.19126819126819128
 - Green: 0.22613065326633167
Micro accuracy: 0.2153316106804479
Macro accuracy: 0.23150242608982088

### Teste no `lisa_nrg_tl_ambiguous.csv`:

Total images: 3392
Confusion matrix:
[[   0    0    0]
 [   1 2815  281]
 [   0  140  155]]
Accuracy per class:
 - None: nan
 - Red: 0.9089441394898289
 - Green: 0.5254237288135594
Micro accuracy: 0.8755896226415094
Macro accuracy: nan (0.7171839341516941)
