Total images: 9565
Confusion matrix:
[[2077   12   60]
 [ 747 3053  408]
 [ 531  265 2412]]
Accuracy per class:
 - None: 0.9664960446719404
 - Red: 0.7255228136882129
 - Green: 0.7518703241895262
Micro accuracy: 0.7884997386304234
Macro accuracy: 0.8146297275165598
