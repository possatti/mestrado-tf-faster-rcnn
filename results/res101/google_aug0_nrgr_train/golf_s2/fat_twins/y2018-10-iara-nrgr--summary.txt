Total images: 10553
Confusion matrix:
[[2911   33   10]
 [  74 3817   13]
 [  44   52 3599]]
Accuracy per class:
 - None: 0.9854434664861205
 - Red: 0.9777151639344263
 - Green: 0.9740189445196211
Micro accuracy: 0.9785842888278214
Macro accuracy: 0.9790591916467227
