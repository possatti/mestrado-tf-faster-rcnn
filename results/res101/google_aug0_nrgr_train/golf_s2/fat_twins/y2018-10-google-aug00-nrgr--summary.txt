Total images: 3227
Confusion matrix:
[[ 538    6   55]
 [  24  372   10]
 [  46   20 2156]]
Accuracy per class:
 - None: 0.8981636060100167
 - Red: 0.916256157635468
 - Green: 0.9702970297029703
Micro accuracy: 0.9501084598698482
Macro accuracy: 0.9282389311161516
