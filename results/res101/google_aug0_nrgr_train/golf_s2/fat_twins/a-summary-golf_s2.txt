
## Y2018_10_GOOGLE_AUG00_NRGR

Total images: 3227
Confusion matrix:
[[ 538    6   55]
 [  24  372   10]
 [  46   20 2156]]
Accuracy per class:
 - None: 0.8981636060100167
 - Red: 0.916256157635468
 - Green: 0.9702970297029703
Micro accuracy: 0.9501084598698482
Macro accuracy: 0.9282389311161516

## Y2018_10_IARA_NRGR

Total images: 10553
Confusion matrix:
[[2911   33   10]
 [  74 3817   13]
 [  44   52 3599]]
Accuracy per class:
 - None: 0.9854434664861205
 - Red: 0.9777151639344263
 - Green: 0.9740189445196211
Micro accuracy: 0.9785842888278214
Macro accuracy: 0.9790591916467227

## Y2018_10_LARA_NRGR

Total images: 9565
Confusion matrix:
[[2077   12   60]
 [ 747 3052  409]
 [ 531  422 2255]]
Accuracy per class:
 - None: 0.9664960446719404
 - Red: 0.7252851711026616
 - Green: 0.702930174563591
Micro accuracy: 0.7719811813904861
Macro accuracy: 0.798237130112731

## Y2018_10_LISA_DAY_TEST_NRGR

Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 223 1100    7]
 [ 301   34 1061]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8270676691729323
 - Green: 0.7600286532951289
Micro accuracy: 0.814996725605763
Macro accuracy: 0.862365440822687

## Y2018_10_LISA_DAY_TRAIN_NRGR

Total images: 12672
Confusion matrix:
[[ 608    0   10]
 [ 121 8142 1365]
 [ 123  190 2113]]
Accuracy per class:
 - None: 0.9838187702265372
 - Red: 0.8456584960531782
 - Green: 0.8709810387469085
Micro accuracy: 0.8572443181818182
Macro accuracy: 0.9001527683422079

## Y2018_10_VITORIA

Total images: 31869
Confusion matrix:
[[ 2056   658   834]
 [   13  7788 11803]
 [   28  2490  6199]]
Accuracy per class:
 - None: 0.5794813979706878
 - Red: 0.39726586410936543
 - Green: 0.7111391533784559
Micro accuracy: 0.5034045624274374
Macro accuracy: 0.5626288051528364

