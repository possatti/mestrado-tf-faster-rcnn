Total images: 31869
Confusion matrix:
[[ 1986   735   827]
 [   16 13799  5789]
 [   33  3353  5331]]
Accuracy per class:
 - None: 0.5597519729425028
 - Red: 0.7038869618445215
 - Green: 0.6115636113341746
Micro accuracy: 0.6625874674448524
Macro accuracy: 0.625067515373733
