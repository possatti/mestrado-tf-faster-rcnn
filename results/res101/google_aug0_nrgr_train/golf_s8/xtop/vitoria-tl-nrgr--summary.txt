Total images: 31869
Confusion matrix:
[[ 1986   732   830]
 [   16  8332 11256]
 [   33  2457  6227]]
Accuracy per class:
 - None: 0.5597519729425028
 - Red: 0.4250153029993879
 - Green: 0.7143512676379489
Micro accuracy: 0.519156547114751
Macro accuracy: 0.5663728478599465
