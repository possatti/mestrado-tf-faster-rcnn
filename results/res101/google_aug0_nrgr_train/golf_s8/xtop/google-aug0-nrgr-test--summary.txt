Total images: 3227
Confusion matrix:
[[ 530   13   56]
 [  28  365   13]
 [  40   16 2166]]
Accuracy per class:
 - None: 0.8848080133555927
 - Red: 0.8990147783251231
 - Green: 0.9747974797479748
Micro accuracy: 0.9485590331577316
Macro accuracy: 0.9195400904762302
