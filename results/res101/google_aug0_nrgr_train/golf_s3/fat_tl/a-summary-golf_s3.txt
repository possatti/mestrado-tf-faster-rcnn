
## GOOGLE_AUG0_NRGR

Total images: 3227
Confusion matrix:
[[ 528   11   60]
 [  24  367   15]
 [  43   53 2126]]
Accuracy per class:
 - None: 0.8814691151919867
 - Red: 0.9039408866995073
 - Green: 0.9567956795679567
Micro accuracy: 0.9361636194607995
Macro accuracy: 0.9140685604864837

## IARA_AUG0_NRGR

Total images: 10553
Confusion matrix:
[[2908   31   15]
 [  72 3821   11]
 [  64  118 3513]]
Accuracy per class:
 - None: 0.984427894380501
 - Red: 0.9787397540983607
 - Green: 0.950744248985115
Micro accuracy: 0.9705297071922676
Macro accuracy: 0.9713039658213255

## TL640_LARA_NRGR

Total images: 9565
Confusion matrix:
[[2067    7   75]
 [ 911 2878  419]
 [ 494  267 2447]]
Accuracy per class:
 - None: 0.9618427175430433
 - Red: 0.68393536121673
 - Green: 0.7627805486284289
Micro accuracy: 0.7728175640355462
Macro accuracy: 0.8028528757960673

## TL640_LISA_DAY_TEST_NRGR

Total images: 3054
Confusion matrix:
[[ 326    2    0]
 [ 261 1040   29]
 [ 272   44 1080]]
Accuracy per class:
 - None: 0.9939024390243902
 - Red: 0.7819548872180451
 - Green: 0.7736389684813754
Micro accuracy: 0.8009168303863785
Macro accuracy: 0.8498320982412703

## TL640_LISA_DAY_TRAIN_NRGR

Total images: 12672
Confusion matrix:
[[ 593    2   23]
 [ 132 6704 2792]
 [ 108  166 2152]]
Accuracy per class:
 - None: 0.959546925566343
 - Red: 0.6963024511840465
 - Green: 0.8870568837592745
Micro accuracy: 0.7456597222222222
Macro accuracy: 0.847635420169888

## VITORIA_TL_NRGR

Total images: 31869
Confusion matrix:
[[ 1940   733   875]
 [   10 13599  5995]
 [   17  3211  5489]]
Accuracy per class:
 - None: 0.5467869222096956
 - Red: 0.6936849622526015
 - Green: 0.6296891132270277
Micro accuracy: 0.6598261633562396
Macro accuracy: 0.6233869992297749

