Total images: 3054
Confusion matrix:
[[ 326    2    0]
 [ 261 1064    5]
 [ 272   45 1079]]
Accuracy per class:
 - None: 0.9939024390243902
 - Red: 0.8
 - Green: 0.7729226361031518
Micro accuracy: 0.8084479371316307
Macro accuracy: 0.8556083583758474
