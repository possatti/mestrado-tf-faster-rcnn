Total images: 12672
Confusion matrix:
[[ 593    0   25]
 [ 132 9204  292]
 [ 108  196 2122]]
Accuracy per class:
 - None: 0.959546925566343
 - Red: 0.9559617781470711
 - Green: 0.8746908491343776
Micro accuracy: 0.9405776515151515
Macro accuracy: 0.9300665176159306
