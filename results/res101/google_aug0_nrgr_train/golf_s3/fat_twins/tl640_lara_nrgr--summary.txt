Total images: 9565
Confusion matrix:
[[2067    7   75]
 [ 911 2876  421]
 [ 494  386 2328]]
Accuracy per class:
 - None: 0.9618427175430433
 - Red: 0.6834600760456274
 - Green: 0.7256857855361596
Micro accuracy: 0.7601672765290121
Macro accuracy: 0.7903295263749435
