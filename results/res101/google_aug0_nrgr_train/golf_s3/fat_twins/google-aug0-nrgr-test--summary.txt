Total images: 3227
Confusion matrix:
[[ 528   10   61]
 [  24  370   12]
 [  43   25 2154]]
Accuracy per class:
 - None: 0.8814691151919867
 - Red: 0.9113300492610837
 - Green: 0.9693969396939695
Micro accuracy: 0.9457700650759219
Macro accuracy: 0.9207320347156799
