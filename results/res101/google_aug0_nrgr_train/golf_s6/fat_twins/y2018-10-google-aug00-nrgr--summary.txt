Total images: 3227
Confusion matrix:
[[ 531    7   61]
 [  33  364    9]
 [  41   21 2160]]
Accuracy per class:
 - None: 0.8864774624373957
 - Red: 0.896551724137931
 - Green: 0.9720972097209721
Micro accuracy: 0.9466997211031918
Macro accuracy: 0.9183754654320996
