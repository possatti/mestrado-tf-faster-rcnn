Total images: 12672
Confusion matrix:
[[ 596    1   21]
 [ 129 8472 1027]
 [ 116  200 2110]]
Accuracy per class:
 - None: 0.9644012944983819
 - Red: 0.8799335272122975
 - Green: 0.8697444352844188
Micro accuracy: 0.8821022727272727
Macro accuracy: 0.9046930856650327
