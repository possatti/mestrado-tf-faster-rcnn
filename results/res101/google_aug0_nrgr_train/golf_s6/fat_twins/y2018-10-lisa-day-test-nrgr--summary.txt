Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 237 1084    9]
 [ 330   18 1048]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8150375939849624
 - Green: 0.7507163323782235
Micro accuracy: 0.8055009823182712
Macro accuracy: 0.8552513087877287
