Total images: 9565
Confusion matrix:
[[2056   22   71]
 [ 682 3098  428]
 [ 475  340 2393]]
Accuracy per class:
 - None: 0.9567240577012563
 - Red: 0.7362167300380228
 - Green: 0.7459476309226932
Micro accuracy: 0.789022477783586
Macro accuracy: 0.8129628062206575
