Total images: 3227
Confusion matrix:
[[ 531    7   61]
 [  33  361   12]
 [  41   15 2166]]
Accuracy per class:
 - None: 0.8864774624373957
 - Red: 0.8891625615763546
 - Green: 0.9747974797479748
Micro accuracy: 0.9476293771304617
Macro accuracy: 0.9168125012539083
