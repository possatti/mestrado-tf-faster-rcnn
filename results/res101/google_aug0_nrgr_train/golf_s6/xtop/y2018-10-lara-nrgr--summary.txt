Total images: 9565
Confusion matrix:
[[2056   22   71]
 [ 682 3103  423]
 [ 475  389 2344]]
Accuracy per class:
 - None: 0.9567240577012563
 - Red: 0.7374049429657795
 - Green: 0.7306733167082294
Micro accuracy: 0.7844223732357554
Macro accuracy: 0.8082674391250885
