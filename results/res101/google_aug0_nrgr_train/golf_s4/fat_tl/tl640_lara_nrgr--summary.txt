Total images: 9565
Confusion matrix:
[[2072    3   74]
 [ 771 3006  431]
 [ 482  162 2564]]
Accuracy per class:
 - None: 0.9641693811074918
 - Red: 0.7143536121673004
 - Green: 0.7992518703241895
Micro accuracy: 0.7989545216936749
Macro accuracy: 0.825924954532994
