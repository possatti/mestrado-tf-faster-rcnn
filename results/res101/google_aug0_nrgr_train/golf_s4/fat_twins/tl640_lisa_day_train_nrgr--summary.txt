Total images: 12672
Confusion matrix:
[[ 599    0   19]
 [ 146 8005 1477]
 [ 121  125 2180]]
Accuracy per class:
 - None: 0.9692556634304207
 - Red: 0.8314291649356045
 - Green: 0.8985985160758451
Micro accuracy: 0.851010101010101
Macro accuracy: 0.8997611148139567
