Total images: 9565
Confusion matrix:
[[2067    7   75]
 [ 667 3114  427]
 [ 428  416 2364]]
Accuracy per class:
 - None: 0.9618427175430433
 - Red: 0.7400190114068441
 - Green: 0.7369077306733167
Micro accuracy: 0.788813382122321
Macro accuracy: 0.8129231532077347
