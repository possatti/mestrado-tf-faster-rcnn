Total images: 10553
Confusion matrix:
[[2886   49   19]
 [  68 3834    2]
 [  46   57 3592]]
Accuracy per class:
 - None: 0.976980365605958
 - Red: 0.9820696721311475
 - Green: 0.9721244925575101
Micro accuracy: 0.9771628920686061
Macro accuracy: 0.9770581767648719
