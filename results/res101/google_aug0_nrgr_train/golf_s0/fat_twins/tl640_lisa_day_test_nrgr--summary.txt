Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 181 1146    3]
 [ 288   16 1092]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8616541353383459
 - Green: 0.7822349570200573
Micro accuracy: 0.8402095612311722
Macro accuracy: 0.8812963641194678
