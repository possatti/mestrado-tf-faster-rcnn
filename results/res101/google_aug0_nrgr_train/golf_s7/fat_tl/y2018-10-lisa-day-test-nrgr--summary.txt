Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 207 1100   23]
 [ 295    9 1092]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8270676691729323
 - Green: 0.7822349570200573
Micro accuracy: 0.825147347740668
Macro accuracy: 0.8697675420643298
