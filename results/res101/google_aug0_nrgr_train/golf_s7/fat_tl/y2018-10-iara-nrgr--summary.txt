Total images: 10553
Confusion matrix:
[[2903   33   18]
 [  46 3820   38]
 [  68  112 3515]]
Accuracy per class:
 - None: 0.9827352742044685
 - Red: 0.9784836065573771
 - Green: 0.9512855209742895
Micro accuracy: 0.9701506680564769
Macro accuracy: 0.9708348005787117
