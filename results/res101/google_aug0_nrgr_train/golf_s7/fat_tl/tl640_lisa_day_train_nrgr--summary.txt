Total images: 12672
Confusion matrix:
[[ 589   13   16]
 [ 107 7458 2063]
 [ 114  191 2121]]
Accuracy per class:
 - None: 0.9530744336569579
 - Red: 0.7746157041960947
 - Green: 0.8742786479802144
Micro accuracy: 0.80239898989899
Macro accuracy: 0.867322928611089
