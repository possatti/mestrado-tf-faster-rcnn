Total images: 3227
Confusion matrix:
[[ 531    8   60]
 [  26  366   14]
 [  36   20 2166]]
Accuracy per class:
 - None: 0.8864774624373957
 - Red: 0.9014778325123153
 - Green: 0.9747974797479748
Micro accuracy: 0.9491788038425782
Macro accuracy: 0.9209175915658953
