Total images: 9565
Confusion matrix:
[[2050   23   76]
 [ 610 3166  432]
 [ 416  407 2385]]
Accuracy per class:
 - None: 0.9539320614239181
 - Red: 0.7523764258555133
 - Green: 0.7434538653366584
Micro accuracy: 0.7946680606377418
Macro accuracy: 0.81658745087203
