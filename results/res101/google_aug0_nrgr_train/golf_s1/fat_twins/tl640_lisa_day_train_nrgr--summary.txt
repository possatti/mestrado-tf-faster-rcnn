Total images: 12672
Confusion matrix:
[[ 611    0    7]
 [  97 8452 1079]
 [ 143  177 2106]]
Accuracy per class:
 - None: 0.988673139158576
 - Red: 0.8778562525965933
 - Green: 0.8680956306677658
Micro accuracy: 0.8813920454545454
Macro accuracy: 0.9115416741409783
