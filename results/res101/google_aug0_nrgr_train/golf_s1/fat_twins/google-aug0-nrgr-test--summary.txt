Total images: 3227
Confusion matrix:
[[ 535   10   54]
 [  30  368    8]
 [  46   27 2149]]
Accuracy per class:
 - None: 0.8931552587646077
 - Red: 0.9064039408866995
 - Green: 0.9671467146714672
Micro accuracy: 0.9457700650759219
Macro accuracy: 0.9222353047742581
