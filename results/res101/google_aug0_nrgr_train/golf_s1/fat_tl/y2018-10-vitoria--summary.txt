Total images: 31869
Confusion matrix:
[[ 2017   702   829]
 [   13 13741  5850]
 [   42  3394  5281]]
Accuracy per class:
 - None: 0.568489289740699
 - Red: 0.7009283819628647
 - Green: 0.6058276930136515
Micro accuracy: 0.6601713263673162
Macro accuracy: 0.6250817882390717
