Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 233 1097    0]
 [ 329    9 1058]]
Accuracy per class:
 - None: 1.0
 - Red: 0.824812030075188
 - Green: 0.7578796561604585
Micro accuracy: 0.8130320890635232
Macro accuracy: 0.8608972287452156
