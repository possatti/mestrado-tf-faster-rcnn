Total images: 12672
Confusion matrix:
[[ 611    0    7]
 [  97 9251  280]
 [ 143  192 2091]]
Accuracy per class:
 - None: 0.988673139158576
 - Red: 0.9608433734939759
 - Green: 0.8619126133553174
Micro accuracy: 0.9432607323232324
Macro accuracy: 0.9371430420026231
