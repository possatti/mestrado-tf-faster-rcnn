Total images: 3061
Confusion matrix:
[[   0    0    0]
 [   0 2533  195]
 [   0   75  258]]
Accuracy per class:
 - None: nan
 - Red: 0.928519061584
 - Green: 0.774774774775
Micro accuracy: 0.911793531526
Macro accuracy: 0.851646918179
