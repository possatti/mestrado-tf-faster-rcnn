
## Y2018_10_GOOGLE_AUG00_NRGR

Total images: 3227
Confusion matrix:
[[ 524    9   66]
 [  27  368   11]
 [  38   23 2161]]
Accuracy per class:
 - None: 0.8747913188647746
 - Red: 0.9064039408866995
 - Green: 0.9725472547254725
Micro accuracy: 0.9460799504183453
Macro accuracy: 0.9179141714923155

## Y2018_10_IARA_NRGR

Total images: 10553
Confusion matrix:
[[2878   58   18]
 [  54 3840   10]
 [  70   52 3573]]
Accuracy per class:
 - None: 0.974272173324306
 - Red: 0.9836065573770492
 - Green: 0.9669824086603518
Micro accuracy: 0.9751729366057046
Macro accuracy: 0.974953713120569

## Y2018_10_LARA_NRGR

Total images: 9565
Confusion matrix:
[[2022   42   85]
 [ 549 3224  435]
 [ 444  424 2340]]
Accuracy per class:
 - None: 0.940902745463006
 - Red: 0.7661596958174905
 - Green: 0.729426433915212
Micro accuracy: 0.7930998431782541
Macro accuracy: 0.8121629583985696

## Y2018_10_LISA_DAY_TEST_NRGR

Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 207 1118    5]
 [ 255   29 1112]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8406015037593985
 - Green: 0.7965616045845272
Micro accuracy: 0.8375900458415193
Macro accuracy: 0.8790543694479753

## Y2018_10_LISA_DAY_TRAIN_NRGR

Total images: 12672
Confusion matrix:
[[ 600    5   13]
 [  91 8617  920]
 [  94  129 2203]]
Accuracy per class:
 - None: 0.970873786407767
 - Red: 0.8949937681761528
 - Green: 0.9080791426215994
Micro accuracy: 0.9011994949494949
Macro accuracy: 0.9246488990685063

## Y2018_10_VITORIA

Total images: 31869
Confusion matrix:
[[ 1889   784   875]
 [    4  9458 10142]
 [   14  2506  6197]]
Accuracy per class:
 - None: 0.5324126268320181
 - Red: 0.48245256070189757
 - Green: 0.710909716645635
Micro accuracy: 0.5505036242116162
Macro accuracy: 0.5752583013931836

