Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 207 1119    4]
 [ 255   29 1112]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8413533834586466
 - Green: 0.7965616045845272
Micro accuracy: 0.8379174852652259
Macro accuracy: 0.8793049960143913
