Total images: 2726
Confusion matrix:
[[   0    0    0]
 [ 207 1119    4]
 [ 255   29 1112]]
Accuracy per class:
 - None: nan
 - Red: 0.8413533834586466
 - Green: 0.7965616045845272
Micro accuracy: 0.8184152604548789
Macro accuracy: 0.8189574940215869
