
## Y2018_10_GOOGLE_AUG00_NRGR

Total images: 3227
Confusion matrix:
[[ 524   10   65]
 [  27  363   16]
 [  38   55 2129]]
Accuracy per class:
 - None: 0.8747913188647746
 - Red: 0.8940886699507389
 - Green: 0.9581458145814582
Micro accuracy: 0.934614192748683
Macro accuracy: 0.909008601132324

## Y2018_10_IARA_NRGR

Total images: 10553
Confusion matrix:
[[2878   58   18]
 [  54 3835   15]
 [  70  105 3520]]
Accuracy per class:
 - None: 0.974272173324306
 - Red: 0.9823258196721312
 - Green: 0.952638700947226
Micro accuracy: 0.9696768691367383
Macro accuracy: 0.9697455646478877

## Y2018_10_IARA_NRGR_DIFFICULT

Total images: 143
Confusion matrix:
[[ 0  5  0]
 [ 0  0  0]
 [ 0 65 73]]
Accuracy per class:
 - None: 0.0
 - Red: nan
 - Green: 0.5289855072463768
Micro accuracy: 0.5104895104895105
Macro accuracy: 0.2644927536231884

## Y2018_10_LARA_NRGR

Total images: 9565
Confusion matrix:
[[2022   43   84]
 [ 549 3237  422]
 [ 444  260 2504]]
Accuracy per class:
 - None: 0.940902745463006
 - Red: 0.7692490494296578
 - Green: 0.7805486284289277
Micro accuracy: 0.811604809200209
Macro accuracy: 0.8302334744405305

## Y2018_10_LARA_NRGR_DIFFICULT

Total images: 1161
Confusion matrix:
[[  4  20  59]
 [  0 106 375]
 [  2 186 409]]
Accuracy per class:
 - None: 0.04819277108433735
 - Red: 0.2203742203742204
 - Green: 0.6850921273031826
Micro accuracy: 0.4470284237726098
Macro accuracy: 0.3178863729205801

## Y2018_10_LISA_DAY_TEST_NRGR

Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 207 1103   20]
 [ 255   35 1106]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8293233082706767
 - Green: 0.7922636103151862
Micro accuracy: 0.8307138179436804
Macro accuracy: 0.8738623061952876

## Y2018_10_LISA_DAY_TRAIN_NRGR

Total images: 12672
Confusion matrix:
[[ 600    2   16]
 [  91 7278 2259]
 [  94  157 2175]]
Accuracy per class:
 - None: 0.970873786407767
 - Red: 0.7559202326547569
 - Green: 0.8965375103050288
Micro accuracy: 0.7933238636363636
Macro accuracy: 0.8744438431225176

## Y2018_10_LISA_DAY_TRAIN_NRGR_DIFFICULT

Total images: 3392
Confusion matrix:
[[   0    0    0]
 [   0  962 2135]
 [   0  108  187]]
Accuracy per class:
 - None: nan
 - Red: 0.31062318372618664
 - Green: 0.6338983050847458
Micro accuracy: 0.3387382075471698
Macro accuracy: 0.47226074440546617

## Y2018_10_VITORIA

Total images: 31869
Confusion matrix:
[[ 1889   778   881]
 [    4  7883 11717]
 [   14  3624  5079]]
Accuracy per class:
 - None: 0.5324126268320181
 - Red: 0.4021118139155274
 - Green: 0.5826545829987381
Micro accuracy: 0.4660014434089554
Macro accuracy: 0.5057263412487613

