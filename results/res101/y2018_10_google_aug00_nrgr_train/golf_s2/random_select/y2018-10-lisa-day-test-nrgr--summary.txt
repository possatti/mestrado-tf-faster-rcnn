Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 207 1103   20]
 [ 255   35 1106]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8293233082706767
 - Green: 0.7922636103151862
Micro accuracy: 0.8307138179436804
Macro accuracy: 0.8738623061952876
