Total images: 618
Confusion matrix:
[[600   5  13]
 [  0   0   0]
 [  0   0   0]]
Accuracy per class:
 - None: 0.970873786407767
 - Red: nan
 - Green: nan
Micro accuracy: 0.970873786407767
Macro accuracy: 0.970873786407767
