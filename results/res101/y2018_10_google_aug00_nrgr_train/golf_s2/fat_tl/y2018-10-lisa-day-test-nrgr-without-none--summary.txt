Total images: 2726
Confusion matrix:
[[   0    0    0]
 [ 207 1090   33]
 [ 255   30 1111]]
Accuracy per class:
 - None: nan
 - Red: 0.8195488721804511
 - Green: 0.7958452722063037
Micro accuracy: 0.8074101247248716
Macro accuracy: 0.8076970721933774
