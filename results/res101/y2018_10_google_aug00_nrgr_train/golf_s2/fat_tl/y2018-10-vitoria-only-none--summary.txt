Total images: 3548
Confusion matrix:
[[1889  786  873]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.5324126268320181
 - Red: nan
 - Green: nan
Micro accuracy: 0.5324126268320181
Macro accuracy: 0.5324126268320181
