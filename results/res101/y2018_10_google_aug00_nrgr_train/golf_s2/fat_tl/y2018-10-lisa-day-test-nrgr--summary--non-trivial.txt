Total images: 235
Confusion matrix:
[[  0   0   0]
 [  0 207  28]
 [  0   0   0]]
Accuracy per class:
 - None: nan
 - Red: 0.88085106383
 - Green: nan
Micro accuracy: 0.88085106383
Macro accuracy: 0.88085106383
