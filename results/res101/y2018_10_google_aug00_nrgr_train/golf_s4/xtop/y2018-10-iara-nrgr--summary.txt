Total images: 10553
Confusion matrix:
[[2902   33   19]
 [  76 3820    8]
 [  46   39 3610]]
Accuracy per class:
 - None: 0.982396750169262
 - Red: 0.9784836065573771
 - Green: 0.9769959404600812
Micro accuracy: 0.97905808774756
Macro accuracy: 0.9792920990622401
