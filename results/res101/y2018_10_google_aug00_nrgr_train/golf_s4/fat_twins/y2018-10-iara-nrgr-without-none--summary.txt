Total images: 7599
Confusion matrix:
[[   0    0    0]
 [  76 3820    8]
 [  46   47 3602]]
Accuracy per class:
 - None: nan
 - Red: 0.9784836065573771
 - Green: 0.974830852503383
Micro accuracy: 0.9767074615080932
Macro accuracy: 0.97665722953038
