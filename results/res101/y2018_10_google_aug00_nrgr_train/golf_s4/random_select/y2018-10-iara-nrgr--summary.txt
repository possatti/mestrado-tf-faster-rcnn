Total images: 10553
Confusion matrix:
[[2902   34   18]
 [  76 3819    9]
 [  46  102 3547]]
Accuracy per class:
 - None: 0.982396750169262
 - Red: 0.9782274590163934
 - Green: 0.9599458728010826
Micro accuracy: 0.9729934615749076
Macro accuracy: 0.973523360662246
