Total images: 143
Confusion matrix:
[[ 0  5  0]
 [ 0  0  0]
 [ 0 81 57]]
Accuracy per class:
 - None: 0.0
 - Red: nan
 - Green: 0.41304347826086957
Micro accuracy: 0.3986013986013986
Macro accuracy: 0.20652173913043478
