Total images: 10553
Confusion matrix:
[[2902   34   18]
 [  76 3820    8]
 [  46  109 3540]]
Accuracy per class:
 - None: 0.982396750169262
 - Red: 0.9784836065573771
 - Green: 0.9580514208389715
Micro accuracy: 0.9724249028712214
Macro accuracy: 0.972977259188537
