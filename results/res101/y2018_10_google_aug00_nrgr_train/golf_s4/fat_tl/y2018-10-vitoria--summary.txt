Total images: 31869
Confusion matrix:
[[ 2021   705   822]
 [   20 13634  5950]
 [   21  3311  5385]]
Accuracy per class:
 - None: 0.5696166854565953
 - Red: 0.6954703121811875
 - Green: 0.6177584031203396
Micro accuracy: 0.6602027048228686
Macro accuracy: 0.6276151335860408
