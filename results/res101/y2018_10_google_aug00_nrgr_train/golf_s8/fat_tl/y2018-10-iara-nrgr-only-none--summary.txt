Total images: 2954
Confusion matrix:
[[2890   48   16]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.9783344617467841
 - Red: nan
 - Green: nan
Micro accuracy: 0.9783344617467841
Macro accuracy: 0.9783344617467841
