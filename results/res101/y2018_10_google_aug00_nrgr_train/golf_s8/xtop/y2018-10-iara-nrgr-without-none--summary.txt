Total images: 7599
Confusion matrix:
[[   0    0    0]
 [  64 3833    7]
 [  69   55 3571]]
Accuracy per class:
 - None: nan
 - Red: 0.9818135245901639
 - Green: 0.9664411366711773
Micro accuracy: 0.9743387287801026
Macro accuracy: 0.9741273306306706
