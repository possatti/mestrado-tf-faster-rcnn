Total images: 12054
Confusion matrix:
[[   0    0    0]
 [ 108 9229  291]
 [ 115  230 2081]]
Accuracy per class:
 - None: nan
 - Red: 0.9585583714167013
 - Green: 0.857790601813685
Micro accuracy: 0.93827775012444
Macro accuracy: 0.9081744866151932
