Total images: 2726
Confusion matrix:
[[   0    0    0]
 [ 222 1105    3]
 [ 306   30 1060]]
Accuracy per class:
 - None: nan
 - Red: 0.8308270676691729
 - Green: 0.7593123209169055
Micro accuracy: 0.7942039618488628
Macro accuracy: 0.7950696942930392
