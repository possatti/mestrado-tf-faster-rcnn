Total images: 3227
Confusion matrix:
[[ 529   11   59]
 [  27  367   12]
 [  44   28 2150]]
Accuracy per class:
 - None: 0.8831385642737897
 - Red: 0.9039408866995073
 - Green: 0.9675967596759676
Micro accuracy: 0.943910753021382
Macro accuracy: 0.9182254035497549
