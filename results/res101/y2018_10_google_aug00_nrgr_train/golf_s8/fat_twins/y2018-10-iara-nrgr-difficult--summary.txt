Total images: 143
Confusion matrix:
[[  0   5   0]
 [  0   0   0]
 [  1  37 100]]
Accuracy per class:
 - None: 0.0
 - Red: nan
 - Green: 0.7246376811594203
Micro accuracy: 0.6993006993006993
Macro accuracy: 0.36231884057971014
