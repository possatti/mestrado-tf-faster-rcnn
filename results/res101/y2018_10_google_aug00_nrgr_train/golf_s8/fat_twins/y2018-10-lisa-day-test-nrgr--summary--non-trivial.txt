Total images: 235
Confusion matrix:
[[  0   0   0]
 [  0 232   3]
 [  0   0   0]]
Accuracy per class:
 - None: nan
 - Red: 0.987234042553
 - Green: nan
Micro accuracy: 0.987234042553
Macro accuracy: 0.987234042553
