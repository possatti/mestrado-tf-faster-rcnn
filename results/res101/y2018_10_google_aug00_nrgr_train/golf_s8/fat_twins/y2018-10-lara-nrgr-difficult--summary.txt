Total images: 1161
Confusion matrix:
[[  5  18  60]
 [  0  92 389]
 [  2 344 251]]
Accuracy per class:
 - None: 0.060240963855421686
 - Red: 0.19126819126819128
 - Green: 0.4204355108877722
Micro accuracy: 0.2997416020671835
Macro accuracy: 0.22398155533712838
