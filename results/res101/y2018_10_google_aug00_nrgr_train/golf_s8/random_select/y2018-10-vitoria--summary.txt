Total images: 31869
Confusion matrix:
[[ 1969   751   828]
 [    5  7932 11667]
 [   21  3605  5091]]
Accuracy per class:
 - None: 0.5549605411499436
 - Red: 0.40461130381554783
 - Green: 0.5840312033956636
Micro accuracy: 0.47042580564184633
Macro accuracy: 0.5145343494537183
