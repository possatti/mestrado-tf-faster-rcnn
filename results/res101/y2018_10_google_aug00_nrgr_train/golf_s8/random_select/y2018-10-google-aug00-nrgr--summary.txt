Total images: 3227
Confusion matrix:
[[ 529   11   59]
 [  27  357   22]
 [  44   59 2119]]
Accuracy per class:
 - None: 0.8831385642737897
 - Red: 0.8793103448275862
 - Green: 0.9536453645364537
Micro accuracy: 0.9312054539820267
Macro accuracy: 0.9053647578792766
