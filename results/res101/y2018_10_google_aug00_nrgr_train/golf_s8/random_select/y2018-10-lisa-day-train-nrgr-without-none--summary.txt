Total images: 12054
Confusion matrix:
[[   0    0    0]
 [ 108 7369 2151]
 [ 115  165 2146]]
Accuracy per class:
 - None: nan
 - Red: 0.7653718321562111
 - Green: 0.8845836768342952
Micro accuracy: 0.7893645262983242
Macro accuracy: 0.8249777544952531
