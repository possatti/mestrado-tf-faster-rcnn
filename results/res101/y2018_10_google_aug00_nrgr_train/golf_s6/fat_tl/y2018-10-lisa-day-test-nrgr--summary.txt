Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 209 1087   34]
 [ 283   18 1095]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8172932330827067
 - Green: 0.7843839541547278
Micro accuracy: 0.8218729535036018
Macro accuracy: 0.8672257290791449
