
## Y2018_10_GOOGLE_AUG00_NRGR

Total images: 3227
Confusion matrix:
[[ 521    9   69]
 [  24  369   13]
 [  38   14 2170]]
Accuracy per class:
 - None: 0.8697829716193656
 - Red: 0.9088669950738916
 - Green: 0.9765976597659766
Micro accuracy: 0.9482491478153083
Macro accuracy: 0.9184158754864112

## Y2018_10_IARA_NRGR

Total images: 10553
Confusion matrix:
[[2902   35   17]
 [  70 3832    2]
 [  62   53 3580]]
Accuracy per class:
 - None: 0.982396750169262
 - Red: 0.9815573770491803
 - Green: 0.9688768606224628
Micro accuracy: 0.9773524116365014
Macro accuracy: 0.9776103292803017

## Y2018_10_LARA_NRGR

Total images: 9565
Confusion matrix:
[[2041   12   96]
 [ 623 3157  428]
 [ 461  363 2384]]
Accuracy per class:
 - None: 0.9497440670079107
 - Red: 0.7502376425855514
 - Green: 0.743142144638404
Micro accuracy: 0.792681651855724
Macro accuracy: 0.8143746180772887

## Y2018_10_LISA_DAY_TEST_NRGR

Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 209 1116    5]
 [ 283   18 1095]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8390977443609022
 - Green: 0.7843839541547278
Micro accuracy: 0.8313686967910936
Macro accuracy: 0.8744938995052101

## Y2018_10_LISA_DAY_TRAIN_NRGR

Total images: 12672
Confusion matrix:
[[ 597    1   20]
 [ 111 9224  293]
 [  97  216 2113]]
Accuracy per class:
 - None: 0.9660194174757282
 - Red: 0.9580390527627752
 - Green: 0.8709810387469085
Micro accuracy: 0.9417613636363636
Macro accuracy: 0.9316798363284705

## Y2018_10_VITORIA

Total images: 31869
Confusion matrix:
[[ 1903   764   881]
 [   13  8410 11181]
 [   25  2438  6254]]
Accuracy per class:
 - None: 0.536358511837655
 - Red: 0.4289940828402367
 - Green: 0.7174486635310313
Micro accuracy: 0.5198468731369043
Macro accuracy: 0.5609337527363077

