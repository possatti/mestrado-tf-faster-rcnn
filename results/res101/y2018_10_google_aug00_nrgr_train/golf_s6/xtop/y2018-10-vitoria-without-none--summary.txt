Total images: 28321
Confusion matrix:
[[    0     0     0]
 [   13  8410 11181]
 [   25  2438  6254]]
Accuracy per class:
 - None: nan
 - Red: 0.4289940828402367
 - Green: 0.7174486635310313
Micro accuracy: 0.5177783270364748
Macro accuracy: 0.573221373185634
