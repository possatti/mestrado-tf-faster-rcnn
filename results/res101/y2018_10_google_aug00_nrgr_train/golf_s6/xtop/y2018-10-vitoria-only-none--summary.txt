Total images: 3548
Confusion matrix:
[[1903  764  881]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.536358511837655
 - Red: nan
 - Green: nan
Micro accuracy: 0.536358511837655
Macro accuracy: 0.536358511837655
