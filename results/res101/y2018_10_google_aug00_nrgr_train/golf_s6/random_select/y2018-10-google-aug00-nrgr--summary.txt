Total images: 3227
Confusion matrix:
[[ 521   10   68]
 [  24  366   16]
 [  38   45 2139]]
Accuracy per class:
 - None: 0.8697829716193656
 - Red: 0.9014778325123153
 - Green: 0.9626462646264626
Micro accuracy: 0.9377130461729161
Macro accuracy: 0.9113023562527145
