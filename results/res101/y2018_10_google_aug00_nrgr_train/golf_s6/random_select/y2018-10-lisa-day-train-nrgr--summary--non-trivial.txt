Total images: 3061
Confusion matrix:
[[   0    0    0]
 [   0 1072 1656]
 [   0  118  215]]
Accuracy per class:
 - None: nan
 - Red: 0.392961876833
 - Green: 0.645645645646
Micro accuracy: 0.420450833061
Macro accuracy: 0.519303761239
