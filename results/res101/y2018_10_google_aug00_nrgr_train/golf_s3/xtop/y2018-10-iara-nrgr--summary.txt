Total images: 10553
Confusion matrix:
[[2898   41   15]
 [  67 3835    2]
 [ 102   69 3524]]
Accuracy per class:
 - None: 0.981042654028436
 - Red: 0.9823258196721312
 - Green: 0.9537212449255751
Micro accuracy: 0.971951103951483
Macro accuracy: 0.9723632395420475
