Total images: 10553
Confusion matrix:
[[2898   41   15]
 [  67 3835    2]
 [ 102  117 3476]]
Accuracy per class:
 - None: 0.981042654028436
 - Red: 0.9823258196721312
 - Green: 0.9407307171853857
Micro accuracy: 0.9674026343219937
Macro accuracy: 0.9680330636286509
