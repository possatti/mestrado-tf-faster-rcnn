Total images: 235
Confusion matrix:
[[  0   0   0]
 [  0 209  26]
 [  0   0   0]]
Accuracy per class:
 - None: nan
 - Red: 0.889361702128
 - Green: nan
Micro accuracy: 0.889361702128
Macro accuracy: 0.889361702128
