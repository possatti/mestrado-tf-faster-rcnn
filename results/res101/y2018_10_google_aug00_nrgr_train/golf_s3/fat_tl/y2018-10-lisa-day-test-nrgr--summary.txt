Total images: 3054
Confusion matrix:
[[ 326    1    1]
 [ 266 1035   29]
 [ 305   25 1066]]
Accuracy per class:
 - None: 0.9939024390243902
 - Red: 0.7781954887218046
 - Green: 0.7636103151862464
Micro accuracy: 0.7946954813359528
Macro accuracy: 0.8452360809774805
