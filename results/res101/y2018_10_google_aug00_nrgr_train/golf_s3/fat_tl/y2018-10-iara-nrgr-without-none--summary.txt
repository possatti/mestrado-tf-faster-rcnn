Total images: 7599
Confusion matrix:
[[   0    0    0]
 [  67 3835    2]
 [ 102  117 3476]]
Accuracy per class:
 - None: nan
 - Red: 0.9823258196721312
 - Green: 0.9407307171853857
Micro accuracy: 0.9621002763521516
Macro accuracy: 0.9615282684287585
