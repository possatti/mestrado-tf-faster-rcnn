Total images: 9565
Confusion matrix:
[[2077    4   68]
 [ 828 2943  437]
 [ 493  269 2446]]
Accuracy per class:
 - None: 0.9664960446719404
 - Red: 0.6993821292775665
 - Green: 0.7624688279301746
Micro accuracy: 0.7805541035023523
Macro accuracy: 0.8094490006265606
