Total images: 2726
Confusion matrix:
[[   0    0    0]
 [ 266 1040   24]
 [ 305   31 1060]]
Accuracy per class:
 - None: nan
 - Red: 0.7819548872180451
 - Green: 0.7593123209169055
Micro accuracy: 0.7703595011005135
Macro accuracy: 0.7706336040674753
