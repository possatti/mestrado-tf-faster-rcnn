Total images: 235
Confusion matrix:
[[  0   0   0]
 [  0 216  19]
 [  0   0   0]]
Accuracy per class:
 - None: nan
 - Red: 0.91914893617
 - Green: nan
Micro accuracy: 0.91914893617
Macro accuracy: 0.91914893617
