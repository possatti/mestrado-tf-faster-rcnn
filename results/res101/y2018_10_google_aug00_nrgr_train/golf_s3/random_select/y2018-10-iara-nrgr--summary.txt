Total images: 10553
Confusion matrix:
[[2898   41   15]
 [  67 3835    2]
 [ 102  144 3449]]
Accuracy per class:
 - None: 0.981042654028436
 - Red: 0.9823258196721312
 - Green: 0.933423545331529
Micro accuracy: 0.964844120155406
Macro accuracy: 0.9655973396773655
