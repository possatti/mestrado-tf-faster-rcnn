Total images: 235
Confusion matrix:
[[  0   0   0]
 [  0 220  15]
 [  0   0   0]]
Accuracy per class:
 - None: nan
 - Red: 0.936170212766
 - Green: nan
Micro accuracy: 0.936170212766
Macro accuracy: 0.936170212766
