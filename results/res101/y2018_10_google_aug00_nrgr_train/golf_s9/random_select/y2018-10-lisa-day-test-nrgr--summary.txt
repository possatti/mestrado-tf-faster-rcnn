Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 196 1115   19]
 [ 267   49 1080]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8383458646616542
 - Green: 0.7736389684813754
Micro accuracy: 0.8261296660117878
Macro accuracy: 0.8706616110476766
