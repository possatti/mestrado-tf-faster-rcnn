Total images: 2149
Confusion matrix:
[[2052   19   78]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.9548627268496975
 - Red: nan
 - Green: nan
Micro accuracy: 0.9548627268496975
Macro accuracy: 0.9548627268496975
