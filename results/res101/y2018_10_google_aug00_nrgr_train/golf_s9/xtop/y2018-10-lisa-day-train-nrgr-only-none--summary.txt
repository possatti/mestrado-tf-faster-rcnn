Total images: 618
Confusion matrix:
[[594   3  21]
 [  0   0   0]
 [  0   0   0]]
Accuracy per class:
 - None: 0.9611650485436893
 - Red: nan
 - Green: nan
Micro accuracy: 0.9611650485436893
Macro accuracy: 0.9611650485436893
