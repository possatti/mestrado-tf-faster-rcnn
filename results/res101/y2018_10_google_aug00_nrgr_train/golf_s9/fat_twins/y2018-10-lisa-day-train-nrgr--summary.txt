Total images: 12672
Confusion matrix:
[[ 594    1   23]
 [  93 8356 1179]
 [ 118  231 2077]]
Accuracy per class:
 - None: 0.9611650485436893
 - Red: 0.8678853344412131
 - Green: 0.8561417971970322
Micro accuracy: 0.8701862373737373
Macro accuracy: 0.895064060060645
