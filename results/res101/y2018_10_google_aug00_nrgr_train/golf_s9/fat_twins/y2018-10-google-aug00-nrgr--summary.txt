Total images: 3227
Confusion matrix:
[[ 526    9   64]
 [  22  376    8]
 [  40   22 2160]]
Accuracy per class:
 - None: 0.8781302170283807
 - Red: 0.9261083743842364
 - Green: 0.9720972097209721
Micro accuracy: 0.948868918500155
Macro accuracy: 0.9254452670445298
