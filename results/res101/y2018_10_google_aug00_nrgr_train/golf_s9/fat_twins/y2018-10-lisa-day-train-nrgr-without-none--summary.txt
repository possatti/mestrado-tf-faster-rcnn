Total images: 12054
Confusion matrix:
[[   0    0    0]
 [  93 8356 1179]
 [ 118  231 2077]]
Accuracy per class:
 - None: nan
 - Red: 0.8678853344412131
 - Green: 0.8561417971970322
Micro accuracy: 0.865521818483491
Macro accuracy: 0.8620135658191226
