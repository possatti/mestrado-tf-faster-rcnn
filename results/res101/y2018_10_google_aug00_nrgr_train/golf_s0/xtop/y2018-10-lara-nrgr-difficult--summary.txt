Total images: 1161
Confusion matrix:
[[ 15   8  60]
 [  0  92 389]
 [  2 324 271]]
Accuracy per class:
 - None: 0.18072289156626506
 - Red: 0.19126819126819128
 - Green: 0.4539363484087102
Micro accuracy: 0.32558139534883723
Macro accuracy: 0.2753091437477222
