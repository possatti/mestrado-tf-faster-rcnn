Total images: 12054
Confusion matrix:
[[   0    0    0]
 [ 116 9231  281]
 [ 127  201 2098]]
Accuracy per class:
 - None: nan
 - Red: 0.9587660988782717
 - Green: 0.86479802143446
Micro accuracy: 0.9398539903766384
Macro accuracy: 0.9117820601563659
