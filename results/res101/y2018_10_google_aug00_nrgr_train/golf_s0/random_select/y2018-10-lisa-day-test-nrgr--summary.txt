Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 212 1112    6]
 [ 305   34 1057]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8360902255639098
 - Green: 0.7571633237822349
Micro accuracy: 0.8176162409954159
Macro accuracy: 0.8644178497820483
