Total images: 328
Confusion matrix:
[[328   0   0]
 [  0   0   0]
 [  0   0   0]]
Accuracy per class:
 - None: 1.0
 - Red: nan
 - Green: nan
Micro accuracy: 1.0
Macro accuracy: 1.0
