Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 212 1117    1]
 [ 305   36 1055]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8398496240601504
 - Green: 0.755730659025788
Micro accuracy: 0.8185985592665357
Macro accuracy: 0.8651934276953127
