Total images: 2954
Confusion matrix:
[[2905   32   17]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.9834123222748815
 - Red: nan
 - Green: nan
Micro accuracy: 0.9834123222748815
Macro accuracy: 0.9834123222748815
