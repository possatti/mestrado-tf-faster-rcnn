Total images: 7599
Confusion matrix:
[[   0    0    0]
 [  77 3812   15]
 [ 125  100 3470]]
Accuracy per class:
 - None: nan
 - Red: 0.9764344262295082
 - Green: 0.939106901217862
Micro accuracy: 0.9582839847348336
Macro accuracy: 0.9577706637236851
