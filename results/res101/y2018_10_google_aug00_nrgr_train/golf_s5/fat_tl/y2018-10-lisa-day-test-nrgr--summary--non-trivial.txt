Total images: 235
Confusion matrix:
[[  0   0   0]
 [  0 215  20]
 [  0   0   0]]
Accuracy per class:
 - None: nan
 - Red: 0.914893617021
 - Green: nan
Micro accuracy: 0.914893617021
Macro accuracy: 0.914893617021
