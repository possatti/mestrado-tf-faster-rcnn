Total images: 7599
Confusion matrix:
[[   0    0    0]
 [  77 3815   12]
 [ 125   58 3512]]
Accuracy per class:
 - None: nan
 - Red: 0.977202868852459
 - Green: 0.9504736129905278
Micro accuracy: 0.9642058165548099
Macro accuracy: 0.9638382409214934
