Total images: 2149
Confusion matrix:
[[2057   16   76]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.9571893904141461
 - Red: nan
 - Green: nan
Micro accuracy: 0.9571893904141461
Macro accuracy: 0.9571893904141461
