Total images: 2954
Confusion matrix:
[[2901   37   16]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.9820582261340555
 - Red: nan
 - Green: nan
Micro accuracy: 0.9820582261340555
Macro accuracy: 0.9820582261340555
