Total images: 12672
Confusion matrix:
[[ 599    0   19]
 [ 102 7296 2230]
 [ 120  164 2142]]
Accuracy per class:
 - None: 0.9692556634304207
 - Red: 0.7577897798088907
 - Green: 0.8829348722176422
Micro accuracy: 0.7920612373737373
Macro accuracy: 0.8699934384856512
