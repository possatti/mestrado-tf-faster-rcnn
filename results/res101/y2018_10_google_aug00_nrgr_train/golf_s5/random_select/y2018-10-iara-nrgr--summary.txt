Total images: 10553
Confusion matrix:
[[2901   37   16]
 [  77 3811   16]
 [ 125  105 3465]]
Accuracy per class:
 - None: 0.9820582261340555
 - Red: 0.9761782786885246
 - Green: 0.9377537212449256
Micro accuracy: 0.9643703212356676
Macro accuracy: 0.9653300753558351
