Total images: 3227
Confusion matrix:
[[ 537    9   53]
 [  28  361   17]
 [  44   48 2130]]
Accuracy per class:
 - None: 0.8964941569282137
 - Red: 0.8891625615763546
 - Green: 0.9585958595859586
Micro accuracy: 0.9383328168577626
Macro accuracy: 0.914750859363509
