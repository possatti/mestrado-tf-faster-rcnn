Total images: 7416
Confusion matrix:
[[   0    0    0]
 [ 915 2854  439]
 [ 471  306 2431]]
Accuracy per class:
 - None: nan
 - Red: 0.6782319391634981
 - Green: 0.7577930174563591
Micro accuracy: 0.7126483279395901
Macro accuracy: 0.7180124783099286
