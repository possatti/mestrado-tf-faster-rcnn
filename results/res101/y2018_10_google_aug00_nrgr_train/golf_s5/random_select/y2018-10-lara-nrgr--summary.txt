Total images: 9565
Confusion matrix:
[[2057   16   76]
 [ 915 2854  439]
 [ 471  306 2431]]
Accuracy per class:
 - None: 0.9571893904141461
 - Red: 0.6782319391634981
 - Green: 0.7577930174563591
Micro accuracy: 0.7675901725039206
Macro accuracy: 0.7977381156780011
