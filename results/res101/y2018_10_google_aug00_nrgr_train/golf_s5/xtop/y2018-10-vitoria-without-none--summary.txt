Total images: 28321
Confusion matrix:
[[    0     0     0]
 [    5  8597 11002]
 [   25  2427  6265]]
Accuracy per class:
 - None: nan
 - Red: 0.4385329524586819
 - Green: 0.7187105655615464
Micro accuracy: 0.5247696055930229
Macro accuracy: 0.5786217590101141
