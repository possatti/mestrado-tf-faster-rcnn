Total images: 7416
Confusion matrix:
[[   0    0    0]
 [ 915 2862  431]
 [ 471  374 2363]]
Accuracy per class:
 - None: nan
 - Red: 0.6801330798479087
 - Green: 0.7365960099750624
Micro accuracy: 0.7045577130528586
Macro accuracy: 0.7083645449114855
