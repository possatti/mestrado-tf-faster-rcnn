Total images: 235
Confusion matrix:
[[  0   0   0]
 [  0 235   0]
 [  0   0   0]]
Accuracy per class:
 - None: nan
 - Red: 1.0
 - Green: nan
Micro accuracy: 1.0
Macro accuracy: 1.0
