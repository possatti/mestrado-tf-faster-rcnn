Total images: 143
Confusion matrix:
[[  0   5   0]
 [  0   0   0]
 [  2  30 106]]
Accuracy per class:
 - None: 0.0
 - Red: nan
 - Green: 0.7681159420289855
Micro accuracy: 0.7412587412587412
Macro accuracy: 0.38405797101449274
