Total images: 328
Confusion matrix:
[[327   0   1]
 [  0   0   0]
 [  0   0   0]]
Accuracy per class:
 - None: 0.9969512195121951
 - Red: nan
 - Green: nan
Micro accuracy: 0.9969512195121951
Macro accuracy: 0.9969512195121951
