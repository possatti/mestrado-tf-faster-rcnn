Total images: 28321
Confusion matrix:
[[    0     0     0]
 [    3 14443  5158]
 [   23  3644  5050]]
Accuracy per class:
 - None: nan
 - Red: 0.736737400530504
 - Green: 0.5793277503728347
Micro accuracy: 0.6882878429433988
Macro accuracy: 0.6580325754516694
