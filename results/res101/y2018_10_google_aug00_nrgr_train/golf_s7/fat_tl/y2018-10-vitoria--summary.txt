Total images: 31869
Confusion matrix:
[[ 1956   760   832]
 [    3 14443  5158]
 [   23  3644  5050]]
Accuracy per class:
 - None: 0.5512965050732808
 - Red: 0.736737400530504
 - Green: 0.5793277503728347
Micro accuracy: 0.6730364931438074
Macro accuracy: 0.6224538853255398
