Total images: 10553
Confusion matrix:
[[2903   34   17]
 [  78 3824    2]
 [ 170  158 3367]]
Accuracy per class:
 - None: 0.9827352742044685
 - Red: 0.9795081967213115
 - Green: 0.9112313937753721
Micro accuracy: 0.9565052591680091
Macro accuracy: 0.9578249549003841
