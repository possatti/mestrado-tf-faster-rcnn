Total images: 618
Confusion matrix:
[[593   8  17]
 [  0   0   0]
 [  0   0   0]]
Accuracy per class:
 - None: 0.959546925566343
 - Red: nan
 - Green: nan
Micro accuracy: 0.959546925566343
Macro accuracy: 0.959546925566343
