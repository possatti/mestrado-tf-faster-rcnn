Total images: 9565
Confusion matrix:
[[2055   22   72]
 [ 628 3144  436]
 [ 420  477 2311]]
Accuracy per class:
 - None: 0.9562587249883667
 - Red: 0.747148288973384
 - Green: 0.7203865336658354
Micro accuracy: 0.785154208050183
Macro accuracy: 0.8079311825425287
