Total images: 3548
Confusion matrix:
[[1956  749  843]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.5512965050732808
 - Red: nan
 - Green: nan
Micro accuracy: 0.5512965050732808
Macro accuracy: 0.5512965050732808
