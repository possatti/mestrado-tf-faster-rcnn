
## Y2018_10_GOOGLE_AUG00_NRGR

Total images: 3227
Confusion matrix:
[[ 526   16   57]
 [  23  367   16]
 [  42   68 2112]]
Accuracy per class:
 - None: 0.8781302170283807
 - Red: 0.9039408866995073
 - Green: 0.9504950495049505
Micro accuracy: 0.9312054539820267
Macro accuracy: 0.910855384410946

## Y2018_10_IARA_NRGR

Total images: 10553
Confusion matrix:
[[2903   34   17]
 [  78 3823    3]
 [ 170  164 3361]]
Accuracy per class:
 - None: 0.9827352742044685
 - Red: 0.9792520491803278
 - Green: 0.9096075778078484
Micro accuracy: 0.9558419406803752
Macro accuracy: 0.9571983003975483

## Y2018_10_IARA_NRGR_DIFFICULT

Total images: 143
Confusion matrix:
[[ 0  5  0]
 [ 0  0  0]
 [ 0 75 63]]
Accuracy per class:
 - None: 0.0
 - Red: nan
 - Green: 0.45652173913043476
Micro accuracy: 0.4405594405594406
Macro accuracy: 0.22826086956521738

## Y2018_10_LARA_NRGR

Total images: 9565
Confusion matrix:
[[2055   22   72]
 [ 628 3123  457]
 [ 420  304 2484]]
Accuracy per class:
 - None: 0.9562587249883667
 - Red: 0.7421577946768061
 - Green: 0.7743142144638404
Micro accuracy: 0.8010454783063251
Macro accuracy: 0.8242435780430043

## Y2018_10_LARA_NRGR_DIFFICULT

Total images: 1161
Confusion matrix:
[[  4  19  60]
 [  0  92 389]
 [  1 208 388]]
Accuracy per class:
 - None: 0.04819277108433735
 - Red: 0.19126819126819128
 - Green: 0.6499162479061976
Micro accuracy: 0.4168819982773471
Macro accuracy: 0.2964590700862421

## Y2018_10_LISA_DAY_TEST_NRGR

Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 207 1114    9]
 [ 319   21 1056]]
Accuracy per class:
 - None: 1.0
 - Red: 0.837593984962406
 - Green: 0.7564469914040115
Micro accuracy: 0.8179436804191225
Macro accuracy: 0.8646803254554726

## Y2018_10_LISA_DAY_TRAIN_NRGR

Total images: 12672
Confusion matrix:
[[ 593   10   15]
 [ 103 7399 2126]
 [ 111  163 2152]]
Accuracy per class:
 - None: 0.959546925566343
 - Red: 0.7684877440797674
 - Green: 0.8870568837592745
Micro accuracy: 0.8005050505050505
Macro accuracy: 0.8716971844684617

## Y2018_10_LISA_DAY_TRAIN_NRGR_DIFFICULT

Total images: 3392
Confusion matrix:
[[   0    0    0]
 [   0 1096 2001]
 [   0  109  186]]
Accuracy per class:
 - None: nan
 - Red: 0.3538908621246367
 - Green: 0.6305084745762712
Micro accuracy: 0.3779481132075472
Macro accuracy: 0.492199668350454

## Y2018_10_VITORIA

Total images: 31869
Confusion matrix:
[[ 1956   749   843]
 [    3  8142 11459]
 [   23  3750  4944]]
Accuracy per class:
 - None: 0.5512965050732808
 - Red: 0.41532340338706386
 - Green: 0.5671676035333257
Micro accuracy: 0.4719947284194672
Macro accuracy: 0.5112625039978901

