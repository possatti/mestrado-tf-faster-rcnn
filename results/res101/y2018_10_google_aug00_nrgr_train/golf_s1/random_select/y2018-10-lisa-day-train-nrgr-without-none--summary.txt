Total images: 12054
Confusion matrix:
[[   0    0    0]
 [ 155 7232 2241]
 [ 123  138 2165]]
Accuracy per class:
 - None: nan
 - Red: 0.7511425010386373
 - Green: 0.8924154987633965
Micro accuracy: 0.7795752447320392
Macro accuracy: 0.821778999901017
