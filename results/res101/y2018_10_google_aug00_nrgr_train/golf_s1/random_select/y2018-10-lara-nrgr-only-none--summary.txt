Total images: 2149
Confusion matrix:
[[2076    6   67]
 [   0    0    0]
 [   0    0    0]]
Accuracy per class:
 - None: 0.9660307119590508
 - Red: nan
 - Green: nan
Micro accuracy: 0.9660307119590508
Macro accuracy: 0.9660307119590508
