Total images: 3054
Confusion matrix:
[[ 328    0    0]
 [ 225 1083   22]
 [ 296   17 1083]]
Accuracy per class:
 - None: 1.0
 - Red: 0.8142857142857143
 - Green: 0.7757879656160458
Micro accuracy: 0.816633922724296
Macro accuracy: 0.8633578933005867
