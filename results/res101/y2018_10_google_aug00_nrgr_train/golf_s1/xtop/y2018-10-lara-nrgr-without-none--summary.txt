Total images: 1954
Confusion matrix:
[[   0    0    0]
 [ 260 1154    4]
 [ 122   10  404]]
Accuracy per class:
 - None: nan
 - Red: 0.8138222849083215
 - Green: 0.753731343283582
Micro accuracy: 0.797338792221085
Macro accuracy: 0.7837768140959518
