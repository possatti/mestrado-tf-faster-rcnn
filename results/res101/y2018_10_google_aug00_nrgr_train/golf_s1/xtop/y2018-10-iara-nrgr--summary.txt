Total images: 10553
Confusion matrix:
[[2906   31   17]
 [  57 3821   26]
 [ 119   65 3511]]
Accuracy per class:
 - None: 0.983750846310088
 - Red: 0.9787397540983607
 - Green: 0.9502029769959405
Micro accuracy: 0.9701506680564769
Macro accuracy: 0.9708978591347964
