Total images: 3061
Confusion matrix:
[[   0    0    0]
 [   5 2446  277]
 [   0  156  177]]
Accuracy per class:
 - None: nan
 - Red: 0.896627565982
 - Green: 0.531531531532
Micro accuracy: 0.856909506697
Macro accuracy: 0.714079548757
