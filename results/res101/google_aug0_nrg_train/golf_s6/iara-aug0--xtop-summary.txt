Total images: 10344
Confusion matrix:
[[2906   40    8]
 [   5 3690    0]
 [  26   40 3629]]
Accuracy per class:
 - None: 0.983750846310088
 - Red: 0.9986468200270636
 - Green: 0.9821380243572395
Micro accuracy: 0.9884957463263728
Macro accuracy: 0.988178563564797
