Total images: 2935
Confusion matrix:
[[ 328    0    0]
 [ 234  976    1]
 [ 331    5 1060]]
Accuracy per class:
 - None: 1.0
 - Red: 0.805945499587118
 - Green: 0.7593123209169055
Micro accuracy: 0.8054514480408859
Macro accuracy: 0.8550859401680079
