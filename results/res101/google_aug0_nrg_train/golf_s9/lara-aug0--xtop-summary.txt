Total images: 9351
Confusion matrix:
[[2078   15   56]
 [ 751 2850  393]
 [ 512  520 2176]]
Accuracy per class:
 - None: 0.9669613773848301
 - Red: 0.7135703555333
 - Green: 0.6783042394014963
Micro accuracy: 0.7597048444016683
Macro accuracy: 0.7862786574398754
