Total images: 10344
Confusion matrix:
[[2917   21   16]
 [   9 3686    0]
 [  20   34 3641]]
Accuracy per class:
 - None: 0.9874746106973595
 - Red: 0.9975642760487144
 - Green: 0.9853856562922869
Micro accuracy: 0.9903325599381284
Macro accuracy: 0.9901415143461203
