# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Factory method for easily getting imdbs by name."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from functools import partial

import os
import sys
import numpy as np

# sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from model.config import cfg

__sets = {}
from datasets.pascal_voc import pascal_voc
from datasets.google_nrg import google_nrg
from datasets.csv_dataset import CSVDataset
try:
  from datasets.coco import coco # @possatti I made the try/except.
except ImportError:
  print('Warning: Missing COCO-API.', file=sys.stderr)

# Set up voc_<year>_<split>
for year in ['2007', '2012']:
  for split in ['train', 'val', 'trainval', 'test']:
    name = 'voc_{}_{}'.format(year, split)
    __sets[name] = (lambda split=split, year=year: pascal_voc(split, year))

for year in ['2007', '2012']:
  for split in ['train', 'val', 'trainval', 'test']:
    name = 'voc_{}_{}_diff'.format(year, split)
    __sets[name] = (lambda split=split, year=year: pascal_voc(split, year, use_diff=True))

# Set up coco_2014_<split>
for year in ['2014']:
  for split in ['train', 'val', 'minival', 'valminusminival', 'trainval']:
    name = 'coco_{}_{}'.format(year, split)
    __sets[name] = (lambda split=split, year=year: coco(split, year))

# Set up coco_2015_<split>
for year in ['2015']:
  for split in ['test', 'test-dev']:
    name = 'coco_{}_{}'.format(year, split)
    __sets[name] = (lambda split=split, year=year: coco(split, year))

# # Set up google_nrg
# for split in ['train', 'test']:
#   name = 'google_nrg_{}'.format(split)
#   __sets[name] = (lambda split=split: google_nrg(split))

NRG_CLASS_NAMES = ['Red', 'Green']
NRGY_CLASS_NAMES = ['Red', 'Green', 'Yellow']
CSV_DATASETS = {
# Google TL
  'google_nrg_train': {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrg', 'google_tl', 'google_tl_nrg_train.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_BASE,
    'classes': NRG_CLASS_NAMES,
  },
  'google_nrg_test':  {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrg', 'google_tl', 'google_tl_nrg_test.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_BASE,
    'classes': NRG_CLASS_NAMES,
  },
  'google_nrgy_train': {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrgy', 'google_tl', 'google_tl_nrgy_train.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_BASE,
    'classes': NRGY_CLASS_NAMES,
  },
  'google_nrgy_test':  {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrgy', 'google_tl', 'google_tl_nrgy_test.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_BASE,
    'classes': NRGY_CLASS_NAMES,
  },
# Google Aug0.
  'google_aug0_nrg_train': {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrg', 'google_tl', 'google_tl_aug0_nrg_train.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_AUG0_BASE,
    'classes': NRG_CLASS_NAMES,
  },
  'google_aug0_nrg_test':  {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrg', 'google_tl', 'google_tl_aug0_nrg_test.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_AUG0_BASE,
    'classes': NRG_CLASS_NAMES,
  },
  'google_aug0_nrgy_train': {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrgy', 'google_tl', 'google_tl_aug0_nrgy_train.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_AUG0_BASE,
    'classes': NRGY_CLASS_NAMES,
  },
  'google_aug0_nrgy_test':  {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrgy', 'google_tl', 'google_tl_aug0_nrgy_test.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_AUG0_BASE,
    'classes': NRGY_CLASS_NAMES,
  },
  'google_aug0_nrgr_train': {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrgr', 'google_tl', 'google_tl_aug0_nrgr_train.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_AUG0_BASE,
    'classes': NRG_CLASS_NAMES,
  },
  'google_aug0_nrgr_test':  {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'detection_nrgr', 'google_tl', 'google_tl_aug0_nrgr_test.csv'),
    'base_dir': cfg.DATA.GOOGLE_TL_AUG0_BASE,
    'classes': NRG_CLASS_NAMES,
  },
## Y2018_10
  'y2018_10_google_aug00_nrgr_train': {
    'csv_path': os.path.join(cfg.ROOT_DIR, 'data', 'annotations', 'y2018_10', 'detection_nrgr', 'y2018_10_google_aug00_nrgr_train.csv'),
    'base_dir': cfg.DATA.Y2018_10_GOOGLE_AUG00_BASE,
    'classes': NRG_CLASS_NAMES,
  },
}
for name in CSV_DATASETS:
  __sets[name] = partial(CSVDataset, name, **CSV_DATASETS[name])


def get_imdb(name):
  """Get an imdb (image database) by name."""
  if name not in __sets:
    raise KeyError('Unknown dataset: {}'.format(name))
  return __sets[name]()


def list_imdbs():
  """List all registered imdbs."""
  return list(__sets.keys())

if __name__ == '__main__':
  # Test the CSV datasets.
  print("__sets.keys():", __sets.keys(), file=sys.stderr) #!#
  for dsname in ['google_nrg_train', 'google_nrg_test', 'google_nrgy_train', 'google_nrgy_test']:
    dataset = __sets[dsname]()
    print("dataset.name:", dataset.name, file=sys.stderr) #!#
    print("dataset.num_classes:", dataset.num_classes, file=sys.stderr) #!#
    print("dataset.classes:", dataset.classes, file=sys.stderr) #!#
    assert dataset.name == dsname, 'ERROR!'
