from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from model.config import cfg
from datasets.imdb import imdb

# try:
import pandas as pd
#   USE_PANDAS = True
# except ImportError as e:
#   USE_PANDAS = False

import scipy.sparse
import numpy as np
import sys
import os

class CSVDataset(imdb):

  def __init__(self, name, csv_path, base_dir=None, classes=None, ignore_invalid=False):
    """Creates a dataset from a csv file.

    Arguments:
      name: Dataset name.
      csv_path: Path to the CSV file containing the bounding boxes information.

    Keyword Arguments:
      base_dir: Optional base path for the images, in case the paths in the CSV
                file are relative.
      classes: A list of classes (ignoring background). It dictates the order of the classes.
      ignore_invalid: If one entry of the dataset seems wrong, ignore it.

    The CSV file should be in the following format: path, x1, y1, x2, y2, class_name.
    Where x1, y1 are the coordinates for the upper-left corner. And x2, y2, the
    bottom-right corner.
    """

    imdb.__init__(self, name)

    self.config = {
      'min_size': 2,
    }

    if not os.path.isfile(csv_path):
      raise IOError('File `{}` doesn\'t exist.'.format(csv_path))

    # FIXME: Maybe use Python's csv module instead of pandas, so that we don't
    #        have yet one more dependency. I don't think it's a big deal though.
    df = pd.read_csv(csv_path, names=['path', 'x1', 'y1', 'x2', 'y2', 'class_name'])

    if classes is None:
      classes = df['class_name'].unique()
    self._classes = ['__background__'] + classes
    self._class_to_ind = dict(zip(self.classes, range(self.num_classes)))

    df['abspath'] = df['path']
    if base_dir:
      if os.path.isdir(base_dir):
        df['abspath'] = list(map(lambda p: os.path.join(base_dir, p), df['abspath']))
      else:
        raise IOError('The base directory `{}` doesn\'t exist.')

    # Go through each item of the csv.
    im_rois = {}
    print(file=sys.stderr)
    for row in df.itertuples():
      print('\x1b[A\x1b[2K  INFO: Processed {} of {}'.format(row.Index+1, len(df)), file=sys.stderr)
      if not os.path.isfile(row.abspath):
        if ignore_invalid:
          print('WARN: Dataset image doesn\'t exist: `{}`.'.format(row.abspath), file=sys.stderr)
          continue
        else:
          raise ValueError('Found a file in the dataset that does not exist: `{}`.'.format(row.abspath))
      if row.abspath in im_rois:
        annotation = im_rois[row.abspath]
      else:
        annotation = {
          'boxes': [],
          'gt_classes': [],
          'gt_overlaps': [],
          'flipped': False,
          'seg_areas': [],
        }
        im_rois[row.abspath] = annotation

      bbox = np.array([row.x1, row.y1, row.x2, row.y2])
      class_ind = self._class_to_ind[row.class_name]
      overlap = np.zeros((1, self.num_classes), dtype=np.float32)
      overlap[0, class_ind] = 1
      seg_area = (row.x2 - row.x1 + 1) * (row.y2 - row.y1 + 1)

      annotation['boxes'].append(bbox)
      annotation['gt_classes'].append(class_ind)
      annotation['gt_overlaps'].append(overlap)
      annotation['seg_areas'].append(seg_area)

    # Convert lists to `ndarrays`.
    for key in im_rois:
      im_rois[key]['boxes']       = np.array(im_rois[key]['boxes']).reshape((-1, 4))
      im_rois[key]['gt_classes']  = np.array(im_rois[key]['gt_classes']).reshape((-1,))
      im_rois[key]['gt_overlaps'] = np.array(im_rois[key]['gt_overlaps']).reshape((-1, self.num_classes))
      im_rois[key]['seg_areas']   = np.array(im_rois[key]['seg_areas']).reshape((-1,))

      im_rois[key]['gt_overlaps'] = scipy.sparse.csr_matrix(im_rois[key]['gt_overlaps'])

      # print("im_rois[key]['boxes']:\n{}".format(im_rois[key]['boxes'])) #!#
      # print("im_rois[key]['gt_classes']:\n{}".format(im_rois[key]['gt_classes'])) #!#
      # print("im_rois[key]['gt_overlaps']:\n{}".format(im_rois[key]['gt_overlaps'])) #!#
      # print("im_rois[key]['seg_areas']:\n{}".format(im_rois[key]['seg_areas'])) #!#

    self._image_index = list(im_rois.keys())
    self._roidb = list(im_rois.values())

    # We don't need the DataFrame anymore.
    del df

    # Warning: Despite whatever we write in here. `imdb.set_proposal_method(cfg.TRAIN.PROPOSAL_METHOD)`
    # seems to be called on the file `convert_from_depre.py`, overwriting this.
    self.set_proposal_method('gt')
    # self._roidb_handler = self.gt_roidb

  def image_path_at(self, i):
    """Return the absolute path to the `i`th image in the image sequence."""

    image_path = self._image_index[i]
    if not os.path.isfile(image_path):
      raise IOError('File `{}` doesn\'t exist.'.format(image_path))
    return image_path

  def gt_roidb(self):
    """Return the database of ground-truth regions of interest."""

    terrible_debug('gt_roidb-called')

    return self._roidb
