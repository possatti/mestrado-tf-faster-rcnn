"""" Loading MIO-TCD database. """

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
from datasets.imdb import imdb
import datasets.ds_utils as ds_utils
import numpy as np
import scipy.sparse
import scipy.io as sio
import pickle
import subprocess
import uuid
from model.config import cfg

import csv
import sys
import json

GOOGLE_TL_PATH = '/dados/datasets/traffic-lights/google-tl/'
TXT_DIR = os.path.join(GOOGLE_TL_PATH, 'horimoto_split')
ANNO_PATH = os.path.join(GOOGLE_TL_PATH, 'tl-annotations.json')

CLASSES = ['background', 'red', 'green']

class google_nrg(imdb):
  def __init__(self, image_set, devkit_path=None):
    imdb.__init__(self, 'google_nrg_' + image_set)

    self._image_set = image_set
    # @possatti This that I'm trying to load is already the `self._image_index` loaded right ahead.
    # with open(os.path.join(TXT_DIR, image_set), 'r') as f: # @possatti
      # self._image_set_content = f.readlines() # @possatti
    self._devkit_path = self._get_default_path() if devkit_path is None \
      else devkit_path
    print("self._devkit_path:", self._devkit_path, file=sys.stderr) #!#
    self._data_path = GOOGLE_TL_PATH
    self._classes = CLASSES
    self._class_to_ind = dict(list(zip(self.classes, list(range(self.num_classes)))))
    self._image_ext = '.jpg'
    self._image_index = self._load_image_set_index()
    # Default to roidb handler
    self._roidb_handler = self.selective_search_roidb
    self._salt = str(uuid.uuid4())
    self._comp_id = 'comp4'

    # PASCAL specific config options
    # change to MIO-TCD specific config
    # no 'use_diff' is needed
    self.config = {'cleanup': True,
                   'use_salt': True,
                   'matlab_eval': False,
                   'rpn_file': None,
                   'min_size': 2}

    assert os.path.exists(self._devkit_path), \
      'devkit folder path does not exist: {}'.format(self._devkit_path)
    assert os.path.exists(self._data_path), \
      'Path does not exist: {}'.format(self._data_path)

  def image_path_at(self, i):
    """
    Return the absolute path to image i in the image sequence.
    """
    return self.image_path_from_index(self._image_index[i])

  def image_path_from_index(self, index):
    """
    Construct an image path from the image's "index" identifier.
    """
    # TODO: might need to adjust for testing
    # @possatti I think `index` is the relative path without the extension.
    # image_path = os.path.join(self._data_path, index + self._image_ext)
    # @possatti `_load_image_set_index` doesn't strip the extension, so I don't think this should either.
    image_path = os.path.join(self._data_path, index)
    assert os.path.exists(image_path), \
      'Path does not exist: {}'.format(image_path)
    return image_path

  def _load_image_set_index(self):
    """
    Load (and write) the indexes listed in this dataset's image set file.
    """

    cache_file = os.path.join(self.cache_path, self.name + '_image_index.pkl')
    if os.path.exists(cache_file):
      with open(cache_file, 'rb') as fid:
        try:
          image_index = pickle.load(fid)
        except:
          image_index = pickle.load(fid, encoding='bytes')
      print('{} image index loaded from {}'.format(self.name, cache_file))
      print('image_index has length {}'.format(len(image_index)))
      return image_index

    # Example path to image set file:
    # self._devkit_path + /MIO-TCD-Localization/gt_train.csv
    image_set_file = os.path.join(TXT_DIR, self._image_set + '.txt')
    assert os.path.exists(image_set_file), \
      'Path does not exist: {}'.format(image_set_file)
    image_index = []
    counter = 0
    with open(image_set_file) as csvf:
      freader = csv.reader(csvf, delimiter=',')
      for row in freader:
        if row[0].strip() not in image_index:
          image_index.append(row[0].strip())
        # print out process
        counter += 1
        if counter % 100 == 0:
          sys.stdout.write(".")
          if counter % 5000 == 0:
            sys.stdout.write(str(counter/1000)+'k')
            sys.stdout.write("\n")
          sys.stdout.flush()
    sys.stdout.write("\n")
    sys.stdout.flush()

    image_index.sort()
    print('image_index has length {}'.format(len(image_index)))

    with open(cache_file, 'wb') as fid:
      pickle.dump(image_index, fid, pickle.HIGHEST_PROTOCOL)
    print('wrote image index to {}'.format(cache_file))

    return image_index

  def _get_default_path(self):
    """
    Return the default path where MIO-TCD-Localization is expected to be installed.
    """
    return cfg.DATA_DIR

  def gt_roidb(self):
    """
    Return the database of ground-truth regions of interest.

    This function loads/saves from/to a cache file to speed up future calls.
    """

    cache_file = os.path.join(self.cache_path, self.name + '_gt_roidb.pkl')
    if os.path.exists(cache_file):
      with open(cache_file, 'rb') as fid:
        try:
          roidb = pickle.load(fid)
        except:
          roidb = pickle.load(fid, encoding='bytes')
      print('{} gt roidb loaded from {}'.format(self.name, cache_file))
      return roidb

    # TODO: test _load_pascal_annotation
    gt_roidb = self._load_pascal_annotation()
    with open(cache_file, 'wb') as fid:
      pickle.dump(gt_roidb, fid, pickle.HIGHEST_PROTOCOL)
    print('wrote gt roidb to {}'.format(cache_file))

    return gt_roidb

  def selective_search_roidb(self):
    """
    Return the database of selective search regions of interest.
    Ground-truth ROIs are also included.

    This function loads/saves from/to a cache file to speed up future calls.
    """
    cache_file = os.path.join(self.cache_path,
                              self.name + '_selective_search_roidb.pkl')

    if os.path.exists(cache_file):
      with open(cache_file, 'rb') as fid:
        roidb = pickle.load(fid)
      print('{} ss roidb loaded from {}'.format(self.name, cache_file))
      return roidb

    # TODO: may need to change operation for test
    # if int(self._year) == 2007 or self._image_set != 'test':
    gt_roidb = self.gt_roidb()
    # TODO: change _load_selective_search_roidb
    ss_roidb = self._load_selective_search_roidb(gt_roidb)
    # TODO: change merge_roidbs
    roidb = imdb.merge_roidbs(gt_roidb, ss_roidb)
    # else:
    #   roidb = self._load_selective_search_roidb(None)
    with open(cache_file, 'wb') as fid:
      pickle.dump(roidb, fid, pickle.HIGHEST_PROTOCOL)
    print('wrote ss roidb to {}'.format(cache_file))

    return roidb

  def rpn_roidb(self):
    # TODO: may need update for test
    # if int(self._year) == 2007 or self._image_set != 'test':
    gt_roidb = self.gt_roidb()
    rpn_roidb = self._load_rpn_roidb(gt_roidb)
    roidb = imdb.merge_roidbs(gt_roidb, rpn_roidb)
    # else:
    #   roidb = self._load_rpn_roidb(None)

    return roidb

  def _load_rpn_roidb(self, gt_roidb):
    filename = self.config['rpn_file']
    print('loading {}'.format(filename))
    assert os.path.exists(filename), \
      'rpn data not found at: {}'.format(filename)
    with open(filename, 'rb') as f:
      box_list = pickle.load(f)
    # TODO: change create_roidb_from_box_list
    return self.create_roidb_from_box_list(box_list, gt_roidb)

  # TODO: change this function, but need to find the mat file
  def _load_selective_search_roidb(self, gt_roidb):
    filename = os.path.abspath(os.path.join(cfg.DATA_DIR,
                                            'selective_search_data',
                                            self.name + '.mat'))
    assert os.path.exists(filename), \
      'Selective search data not found at: {}'.format(filename)
    raw_data = sio.loadmat(filename)['boxes'].ravel()

    box_list = []
    for i in range(raw_data.shape[0]):
      boxes = raw_data[i][:, (1, 0, 3, 2)] - 1
      keep = ds_utils.unique_boxes(boxes)
      boxes = boxes[keep, :]
      keep = ds_utils.filter_small_boxes(boxes, self.config['min_size'])
      boxes = boxes[keep, :]
      box_list.append(boxes)

    return self.create_roidb_from_box_list(box_list, gt_roidb)

  def _load_pascal_annotation(self):
    with open(ANNO_PATH, 'r') as f:
      annotations_data = json.load(f)

    annotations = []
    for i, item in enumerate(annotations_data):
      print('\x1b[A\x1b[2K  INFO: Processed {} of {}'.format(i+1, len(annotations_data)), file=sys.stderr)
      img_path = item['filepath']
      # @possatti `_load_image_set_index` doesn't strip the extension, so I don't think this should either.
      # if os.path.splitext(img_path)[0] in self._image_index:
      if img_path in self._image_index:
        boxes = np.zeros((0, 4), dtype=np.uint16)
        overlaps = np.zeros((0, self.num_classes), dtype=np.float32)
        gt_classes_L = []
        seg_areas_L = []

        for tl in item['traffic_lights']:
          x1 = tl['bbox'][0][0]
          y1 = tl['bbox'][0][1]
          x2 = tl['bbox'][1][0]
          y2 = tl['bbox'][1][1]
          label = tl['status']

          # Load only Red (1) and Green (2) traffic lights.
          if label in [1, 2]:
            boxes = np.vstack((boxes, np.array([x1, y1, x2, y2])))
            gt_classes_L.append(label)
            overlap =  np.zeros((1, self.num_classes), dtype=np.float32)
            overlap[0, label] = 1.0
            overlaps = np.vstack((overlaps, overlap))
            seg_areas_L.append((x2 - x1 + 1) * (y2 - y1 + 1))

        gt_classes = np.array(gt_classes_L, dtype=np.int32)
        seg_areas = np.array(seg_areas_L, dtype=np.int32)
        overlaps = scipy.sparse.csr_matrix(overlaps)
        annotations.append((img_path,{'boxes': boxes,
                      'gt_classes': gt_classes,
                      'gt_overlaps': overlaps,
                      'flipped': False,
                      'seg_areas': seg_areas}))
    # @possatti The sort is important because `_load_image_set_index` also sort
    # the paths. So we need to sort, so that will be a one to one correspondence.
    annotations = sorted(annotations, key=lambda x: x[0])
    annotations = [x[1] for x in annotations]
    return annotations
