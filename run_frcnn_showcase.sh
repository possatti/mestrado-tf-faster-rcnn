#!/usr/bin/env bash

rm -f frcnn-showcase-detections.zip
# python3 possatti/viz_natural_predictions.py frcnn-showcase-detections.txt --save-imgs frcnn-showcase-detections
# mogrify -verbose -crop '640x480+36+4' frcnn-showcase-detections/*

# python3 possatti/viz_natural_predictions.py frcnn-showcase-detections.txt \
# 	-l 0431 -l 0469 -l 1598 -l 0656 -l frame_006613 -l 0131 \
# 	--save-imgs frcnn-showcase-detections

# python3 possatti/viz_natural_predictions.py frcnn-showcase-detections.txt -a \
# 	-l 0431 -l 0469 -l 1598 -l 0656 -l frame_006613 -l 0131 \
# 	--save-imgs frcnn-showcase-detections-badges


python3 possatti/viz_natural_predictions.py frcnn-showcase-detections.txt -a \
	-l 0431 -l 0469 -l 1598 -l 0656 -l frame_006613 -l 0131


# zip frcnn-showcase-detections.zip -rj frcnn-showcase-detections
