#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import numpy as np
import argparse
import random
import sys
import os
import re

import flextop

print(sys.version, file=sys.stderr)

random.seed(7)

# all_lisa_day_test_predictions = [ '/home/lucaspossatti/projects/traffic-lights/darknet/results_yolo/y2018_10_google_aug00_nrgr_train_s{}/yolov3-nrgr-5000-test/yolov3-nrgr-5000_final/y2018-10-lisa-day-test-nrgr--detections.txt'.format(i) for i in range(10) ]

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('natural_predictions')
	args = parser.parse_args(['/home/lucaspossatti/projects/traffic-lights/darknet/results_yolo/y2018_10_google_aug00_nrgr_train_s9/yolov3-nrgr-5000-test/yolov3-nrgr-5000_final/y2018-10-lisa-day-test-nrgr--detections.txt'])
	return args

def main():
	args = parse_args()

	predictions = flextop.parse_natural_predictions(args.natural_predictions)
	total_images = len(predictions)
	print('Total images:', total_images)
	obj_predictions = { k:v for k,v in predictions.items() if len(v) > 0 }
	images_with_objects = len(obj_predictions)
	print('Images with objects:', images_with_objects)

	discordants = []
	concordants = []
	discordants_counts = []
	for image_path, preds in predictions.items():
		if len(preds) > 0:
			statuses = [ p['class_num'] for p in preds ]
			different_statuses = set(statuses)
			if len(different_statuses) > 1:
				discordants.append(image_path)
				discordants_counts.append(len(preds))
			else:
				concordants.append(image_path)

	print('Images with concordant TLs:', len(concordants))
	print('Images with discordant TLs:', len(discordants))

	unique_discordants_counts, count_unique_discordants_counts = np.unique(discordants_counts, return_counts=True)
	print('Discordant TL count:')
	for n, c in zip(unique_discordants_counts, count_unique_discordants_counts):
		print(' - {} TLs: {} images'.format(n, c))

	# print('Discordant examples:')
	# for image_path in random.sample(discordants, k=5):
	# 	print(' - {}'.format(image_path))

	# print('All discordant images:')
	# for image_path in discordants:
	# 	print(' - {}'.format(image_path))


if __name__ == '__main__':
	main()
