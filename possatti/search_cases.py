#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import glob
import sys
import os
import re

print(sys.version, file=sys.stderr)


def parse_args():
	parser = argparse.ArgumentParser(description='Search on results for specific cases.')
	# parser.add_argument('arg')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()
	rules = ['xtop', 'fat_tl', 'fat_twins']
	datasets = {
		'iara': 'y2018-10-iara-nrgr--predictions.csv',
		'lara': 'y2018-10-lara-nrgr--predictions.csv',
		'lisa-day-train': 'y2018-10-lisa-day-train-nrgr--predictions.csv',
		'vitoria': 'y2018-10-vitoria--predictions.csv',
	}

	df = None
	for ds in datasets:
		for rule in rules:
			x = pd.read_csv('results/res101/y2018_10_google_aug00_nrgr_train/golf_s0/{}/{}'.format(rule, datasets[ds]))
			x['dataset'] = ds
			x['rule'] = rule
			if df is None:
				df = x
			else:
				df = pd.concat([df, x], ignore_index=True)

	pd.set_option('display.max_colwidth', -1)

	# sub = df.loc[(df['rule']=='xtop')&(df['dataset']=='vitoria'), 'n_detections']
	# sub = df.loc[(df['rule']=='xtop')&(df['dataset']!='lara')].sort_values(by='n_detections')

	# sub = df.loc[(df['rule']=='xtop')&(df['n_detections']>4)]
	# print("sub.sample(n=30):\n{}".format(sub.sample(n=50)), file=sys.stderr) #!#

	## Wrong ones:
	# sub = df.loc[(df['label']!=df['prediction'])]
	# print("sub.sample(n=50):\n{}".format(sub.sample(n=50)), file=sys.stderr) #!#
	# sub = df.loc[(df['label']==0)&(df['prediction']!=0)]
	# print("sub.sample(n=50):\n{}".format(sub.sample(n=50)), file=sys.stderr) #!#

	for path in df['path'].unique():
		# sub = df.loc[(df['path']==path)&(df['rule']!='fat_twins')]
		sub = df.loc[(df['path']==path)]
		x = (sub['label']==sub['prediction']).sum()
		if x == 1:
			print("sub:\n{}".format(sub), file=sys.stderr) #!#




if __name__ == '__main__':
	main()
