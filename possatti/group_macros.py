#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

import sss

print(sys.version, file=sys.stderr)

RULES = ['xtop', 'fat_tl', 'fat_twins', 'random_select']
MERGE_GROUPS = {
    'all_hard': [
        'IARA_hard',
        'LARA_hard',
        'LISA_hard',
        'vitoria(2018_09)',
    ],
    'LISA_day': [
        'LISA_day_train',
        'LISA_day_test',
    ],
    'LISA_day_non_trivial': [
        'LISA_day_train_non_trivial',
        'LISA_day_test_non_trivial',
    ]
}

DATASETS = {
    'IARA_hard':        'y2018-10-iara-nrgr-difficult--summary.txt',
    'LARA_hard':        'y2018-10-lara-nrgr-difficult--summary.txt',
    'LISA_hard':        'y2018-10-lisa-day-train-nrgr-difficult--summary.txt',
    'vitoria(2018_09)': 'y2018-10-vitoria--summary.txt',
    # 'GOOGLE':           'y2018-10-google-aug00-nrgr--summary.txt',
    'IARA':             'y2018-10-iara-nrgr--summary.txt',
    'LARA':             'y2018-10-lara-nrgr--summary.txt',
    'LISA_day_test':    'y2018-10-lisa-day-test-nrgr--summary.txt',
    'LISA_day_train':   'y2018-10-lisa-day-train-nrgr--summary.txt',
    'LISA_day_test_non_trivial':    'y2018-10-lisa-day-test-nrgr--summary--non-trivial.txt',
    'LISA_day_train_non_trivial':   'y2018-10-lisa-day-train-nrgr--summary--non-trivial.txt',

    'IARA_only_none': 'y2018-10-iara-nrgr-only-none--summary.txt',
    'LARA_only_none': 'y2018-10-lara-nrgr-only-none--summary.txt',
    'LISA_day_test_only_none': 'y2018-10-lisa-day-test-nrgr-only-none--summary.txt',
    'LISA_day_train_only_none': 'y2018-10-lisa-day-train-nrgr-only-none--summary.txt',
    'vitoria(2018_09)_only_none': 'y2018-10-vitoria-only-none--summary.txt',

    'IARA_without_none': 'y2018-10-iara-nrgr-without-none--summary.txt',
    'LARA_without_none': 'y2018-10-lara-nrgr-without-none--summary.txt',
    'LISA_day_test_without_none': 'y2018-10-lisa-day-test-nrgr-without-none--summary.txt',
    'LISA_day_train_without_none': 'y2018-10-lisa-day-train-nrgr-without-none--summary.txt',
    'vitoria(2018_09)_without_none': 'y2018-10-vitoria-without-none--summary.txt',
}

FRCNN_PATH_TEMPLATE = os.path.join(os.path.dirname(__file__), '..', 'results/res101/y2018_10_google_aug00_nrgr_train/golf_s{i}/{rule}/{txt}')
FRCNN_PATH_TEMPLATE = os.path.realpath(FRCNN_PATH_TEMPLATE)

YOLOV3_DEFAULT_PATH_TEMPLATE =  os.path.join('/home/lucaspossatti/projects/traffic-lights/darknet', 'results_yolo/y2018_10_google_aug00_nrgr_train_s{i}/yolov3-nrgr-5000-test/yolov3-nrgr-5000_final/{rule}/{txt}')

CLASSES = ['None', 'Red', 'Green']

def float_or_nan(s):
    if s.lower().strip() == 'nan':
        return np.nan
    else:
        return np.float(s)

def parse_txt(txt_path):
    with open(txt_path, 'r') as f:
        txt_content = f.read()
    d = {
        'conf_mat':  np.array([ l.split() for l in re.findall(r'^\s*\[+([\s\d]+)]+', txt_content, flags=re.M) ], dtype=int),
        'acc_none':  float_or_nan(re.search(r'^ - None: (nan|[\d.]+)',  txt_content, flags=re.MULTILINE).group(1)),
        'acc_red':   float_or_nan(re.search(r'^ - Red: (nan|[\d.]+)',   txt_content, flags=re.MULTILINE).group(1)),
        'acc_green': float_or_nan(re.search(r'^ - Green: (nan|[\d.]+)', txt_content, flags=re.MULTILINE).group(1)),
        'micro': float_or_nan(re.search(r'^Micro accuracy: (nan|[\d.]+)', txt_content, flags=re.MULTILINE).group(1)),
        'macro': float_or_nan(re.search(r'^Macro accuracy: (nan|[\d.]+)', txt_content, flags=re.MULTILINE).group(1)),
    }
    return d

def get_ds_name(txt_path):
    # print("txt_path:", txt_path, file=sys.stderr) #!#
    return re.search(r'y2018-10-([\w-]+).*--summary\.txt', txt_path).group(1).upper()

def read_txts(path_template, datasets):
    cols = ['shuffle', 'rule', 'dataset', 'macro', 'conf_mat']
    data = { c: [] for c in cols }
    for ds_name in DATASETS:
        ds_txt_name = DATASETS[ds_name]
        for rule in RULES:
            for i in range(10):
                txt_path = path_template.format(i=i, rule=rule, txt=ds_txt_name)
                txt_d = parse_txt(txt_path)
                data['shuffle'].append(i)
                data['rule'].append(rule)
                data['dataset'].append(ds_name)
                data['macro'].append(txt_d['macro'])
                data['conf_mat'].append(txt_d['conf_mat'])
    return data

def calc_accs(conf_mat):
    acc_micro = conf_mat.diagonal().sum() / conf_mat.sum()
    np.seterr(invalid="ignore")
    acc_per_class = conf_mat.diagonal() / conf_mat.sum(axis=1)
    np.seterr(divide="warn")
    acc_per_class_no_nan = acc_per_class[~np.isnan(acc_per_class)]
    acc_macro = acc_per_class_no_nan.mean()
    return {
        'acc_per_class' : acc_per_class,
        'macro' : acc_macro,
        'micro' : acc_micro,
    }

def group(df, group_name, ds_names):
    group_df = df.loc[df['dataset'].isin(ds_names), :]
    if len(group_df) == 0:
        raise Exception('Couldn\'t find anything to group.')
    merged_rows = { c: [] for c in df.columns }
    for shuffle in group_df['shuffle'].unique():
        for rule in group_df['rule'].unique():
            rows = (group_df['shuffle']==shuffle) & (group_df['rule']==rule)
            selected_lines = group_df.loc[rows, :]
            merged_conf_mat = selected_lines['conf_mat'].sum()
            accs = calc_accs(merged_conf_mat)
            merged_rows['shuffle'].append(shuffle)
            merged_rows['rule'].append(rule)
            merged_rows['dataset'].append(group_name)
            merged_rows['macro'].append(accs['macro'])
            merged_rows['conf_mat'].append(merged_conf_mat)
    return merged_rows

def thiago_print(df, framework_name):
    for row in df.itertuples():
        print('{i} {framework}-{rule} {ds} {macro}'.format(i=row.shuffle, framework=framework_name, rule=row.rule, ds=row.dataset, macro=row.macro))

def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('merge_groups', nargs=argparse.REMAINDER, choices=MERGE_GROUPS)
    args = parser.parse_args()
    return args

def main():
    args = parse_args()

    # For Faster R-CNN.
    data = read_txts(FRCNN_PATH_TEMPLATE, DATASETS)
    df = pd.DataFrame(data)
    for merge_group in args.merge_groups:
        print('INFO: Merging {}.'.format(merge_group), file=sys.stderr)
        g = group(df, merge_group, MERGE_GROUPS[merge_group])
        df = pd.concat([df, pd.DataFrame(g)])
    thiago_print(df, 'FRCNN')
    # del data, df, g

    # # For YOLO.
    # data = read_txts(YOLOV3_DEFAULT_PATH_TEMPLATE, DATASETS)
    # df = pd.DataFrame(data)
    # for merge_group in args.merge_groups:
    #     g = group(df, merge_group, MERGE_GROUPS[merge_group])
    #     df = pd.concat([df, pd.DataFrame(g)])
    # thiago_print(df, 'YOLOv3-default')


if __name__ == '__main__':
    main()
