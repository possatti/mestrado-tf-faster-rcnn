#!/usr/bin/env python

# --------------------------------------------------------
# Tensorflow Faster R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Xinlei Chen, based on code from Ross Girshick
# --------------------------------------------------------

"""
Demo script showing detections in sample images.

See README.md for installation instructions before running.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import _init_paths
from model.config import cfg
from model.test import im_detect
from model.nms_wrapper import nms

from utils.timer import Timer

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import argparse
import random
import glob
import cv2
import sys
import os

from nets.vgg16 import vgg16
from nets.resnet_v1 import resnetv1

import testing

CLASSES = ('__background__', 'red', 'green')
CLASS_COLORS = ('black', 'red', 'green')
N_CLASSES = len(CLASSES)

NETS = ('vgg16', 'res101')
DATASETS = {
    'google_nrg': ('google_nrg_train',)
}

IMAGE_EXTENSIONS = ['.jpg', '.jpeg', '.png']


def demo(sess, net, image_path, nms_thresh=0.3, conf_thresh=0.8):
    """Detect object classes in an image using pre-computed object proposals."""

    # Load the image.
    im = cv2.imread(image_path)

    # Run the image through the network.
    dets_d = testing.run_test(sess, net, im, N_CLASSES, nms_thresh=nms_thresh, conf_thresh=conf_thresh)

    # Transform from BGR to RGB.
    im = im[:, :, (2, 1, 0)]

    fig, ax = plt.subplots(1, 1)
    ax.imshow(im, aspect='equal')
    plt.axis('off')
    plt.tight_layout()

    for cls_ind, class_name in enumerate(CLASSES[1:]):
        cls_ind += 1 # because we skipped background
        dets = dets_d[cls_ind]
        testing.draw_dets(ax, class_name, dets, color=CLASS_COLORS[cls_ind])
    plt.show()

def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Tensorflow Faster R-CNN demo')
    parser.add_argument('ckpt',
        help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')
    parser.add_argument('images', nargs='+', help='A directory with images, or an image file.')
    parser.add_argument('--net', help='Network to use [{}]'.format(','.join(NETS)),
                        choices=NETS, default='res101')
    parser.add_argument('--tag', default='default')
    parser.add_argument('-k', '--k-images', type=int, help='Sample `k` images from all the images.', default=None)
    parser.add_argument('-s', '--seed', type=int, help='Seed for the sampler.', default=7)
    args = parser.parse_args()

    if args.k_images == None:
        args.k_images = len(args.images)

    return args

if __name__ == '__main__':
    cfg.TEST.HAS_RPN = True  # Use RPN for proposals

    args = parse_args()

    random.seed(args.seed)

    sess, net = testing.load_network(args.ckpt, N_CLASSES, args.net, anchor_scales=[4,8,16,32], anchor_ratios=[0.5,1,2])

    # Expand testing directories.
    image_paths = []
    for path in args.images:
        file_basename, file_extension = os.path.splitext(path)
        if os.path.isfile(path) and file_extension.lower() in IMAGE_EXTENSIONS:
            image_paths.append(path)
        elif os.path.isdir(path):
            for root, dirs, files in os.walk(path):
                for filename in files:
                    file_basename, file_extension = os.path.splitext(filename)
                    if file_extension.lower() in IMAGE_EXTENSIONS:
                        image_path = os.path.join(root, filename)
                        image_paths.append(image_path)
        else:
            print('WARN: `{}` is neither an image nor a valid directory. Ignoring it.'.format(path), file=sys.stderr)

    # Sample some images.
    if args.k_images < len(image_paths):
        print('WARN: Showing only {} images out of {}.'.format(args.k_images, len(image_paths)), file=sys.stderr)
    image_paths = random.sample(image_paths, args.k_images)

    for im_path in image_paths:
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        print('Demo for `{}`'.format(im_path))
        demo(sess, net, im_path)
