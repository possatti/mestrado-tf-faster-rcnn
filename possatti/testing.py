#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import time
import sys
import os
import re

import _init_paths
from model.test import im_detect
from model.config import cfg
from model.nms_wrapper import nms
from utils.timer import Timer

from nets.vgg16 import vgg16
from nets.resnet_v1 import resnetv1


def run_test(sess, net, im, n_classes, conf_thresh=0.8, nms_thresh=0.3, return_inference_duration=False):
    """Run the image through the network.

    Returns a dictionary, with classe's numbers as keys, and `dets` as values.

    Discard bounding boxes with score below `conf_thresh`.

    `n_classes` should count background too.

    When `return_inference_duration` is true, the function will return a second value.
    The inference duration, in seconds.
    """

    inference_start_time = time.time()
    scores, boxes = im_detect(sess, net, im)
    inference_end_time = time.time()

    dets_per_class = dict()

    # I don't know why we have to do this... I wish we didn't.
    for cls_ind in range(1, n_classes):
        cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
        cls_scores = scores[:, cls_ind]
        dets = np.hstack([cls_boxes, cls_scores[:, np.newaxis]]).astype(np.float32)

        keep = nms(dets, nms_thresh)
        dets = dets[keep, :]

        conf_inds = np.where(dets[:, -1] >= conf_thresh)[0]
        dets = dets[conf_inds, :]

        dets_per_class[cls_ind] = dets

    if return_inference_duration:
        return dets_per_class, inference_end_time - inference_start_time
    else:
        return dets_per_class

def draw_dets(ax, class_name, dets, color='white'):
    """Draw detected bounding boxes."""

    for i in range(len(dets)):
        bbox = dets[i, :4]
        score = dets[i, -1]

        ax.add_patch(
            plt.Rectangle(
                (bbox[0], bbox[1]),
                bbox[2] - bbox[0],
                bbox[3] - bbox[1], fill=False,
                edgecolor=color, linewidth=3.5
            )
        )
        ax.text(bbox[0], bbox[1] - 2,
                '{:s} {:.3f}'.format(class_name, score),
                bbox=dict(facecolor=color, alpha=0.5),
                fontsize=14, color='white')

def load_network(checkpoint_path, n_classes, net_name, anchor_scales=None, anchor_ratios=None, tag='default'):
    if anchor_scales is None:
        anchor_scales = cfg.ANCHOR_SCALES
    if anchor_ratios is None:
        anchor_ratios = cfg.ANCHOR_RATIOS

    # model path
    match = re.search(r'(.*\.ckpt)(|\.meta|\.index)', checkpoint_path)
    if not match:
        raise ValueError('Checkpoint is different from the expected format. Value received: `{}`.'.format(checkpoint_path))
    tfmodel = match.group(1)
    meta_filepath = tfmodel + '.meta'
    if not os.path.isfile(meta_filepath):
        raise IOError('{:s} not found.'.format(meta_filepath))

    # set config
    tfconfig = tf.ConfigProto(allow_soft_placement=True)
    tfconfig.gpu_options.allow_growth=True

    # init session
    sess = tf.Session(config=tfconfig)

    # load network
    if net_name == 'vgg16':
        net = vgg16()
    elif net_name == 'res101':
        net = resnetv1(num_layers=101)
    else:
        raise NotImplementedError()
    net.create_architecture("TEST", n_classes, tag=tag, anchor_scales=anchor_scales, anchor_ratios=anchor_ratios)

    # restore training
    saver = tf.train.Saver()
    saver.restore(sess, tfmodel)

    return sess, net
