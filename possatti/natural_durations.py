#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import matplotlib.pyplot as plt
import numpy as np
import argparse
import sys
import os
import re

print(sys.version, file=sys.stderr)


def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('natural_detections')
	parser.add_argument('--hist', action='store_true')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	with open(args.natural_detections, 'r') as f:
		content = f.read()
	durations = re.findall(r'Predicted in ([\d.]+) seconds\.', content)
	durations = np.array(durations, dtype=float)
	# print("durations:\n{}".format(durations), file=sys.stderr) #!#

	mean = durations.mean()
	print('mean: {} seconds ({} FPS)'.format(mean, 1/mean))
	print('std:  {}'.format(durations.std()))
	print('min:  {}'.format(durations.min()))
	print('max:  {}'.format(durations.max()))

	if args.hist:
		plt.hist(durations, bins=1000, range=(0, 0.5))
		plt.show()



if __name__ == '__main__':
	main()
