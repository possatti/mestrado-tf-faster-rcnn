#!/usr/bin/env python

"""
Demo script showing detections in sample images.

See README.md for installation instructions before running.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from sklearn.metrics import confusion_matrix

import _init_paths
from model.config import cfg, cfg_from_list, cfg_from_file
from model.test import im_detect
from model.nms_wrapper import nms
from utils.timer import Timer

import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import numpy as np
import argparse
import glob
import cv2
import sys
import os
import re

from nets.vgg16 import vgg16
from nets.resnet_v1 import resnetv1

import testing

# Use `raw_input` as `input` for Python 2.
if re.search(r'^2\.', sys.version):
    input = raw_input

CLASSES = {
    'NRG': ['None', 'Red', 'Green'],
    'NRGY': ['None', 'Red', 'Green', 'Yellow'],
}
CLASS_COLORS = {
    'NRG': ['black', 'red', 'green'],
    'NRGY': ['black', 'red', 'green', 'yellow'],
}

# N_CLASSES = len(CLASSES)

NETS = ['res101']

PREDICTIONS_COLNAMES = ['path', 'label', 'prediction', 'n_detections']
PREDICTIONS_COLTYPES = [str, int, int, int]

ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))

def arg_comma_separated_float_list(s):
    """Parse a string like '1., -2.1, 3, whatever' into a python list."""
    if type(s) != str:
        raise TypeError('Expected a string, got type `{}`.'.format(type(s)))
    pieces = re.split(r', *', s)
    pieces = [ float(p) for p in pieces ]
    return pieces

def arg_comma_separated_list(s):
    """Parse a string like '1., -2.1, 3, whatever' into a python list."""
    if type(s) != str:
        raise TypeError('Expected a string, got type `{}`.'.format(type(s)))
    pieces = re.split(r', *', s)
    return pieces

def xtop_predict(sess, net, image, class_names=CLASSES['NRG'], conf_thresh=0.8, nms_thresh=0.3, plt_debug=False):
    if type(image) is str:
        if not os.path.isfile(image):
            raise ValueError('Couldn\'t load image from `{}`. File doesn\'t exist.')
        im = cv2.imread(image)
    elif type(image) is np.ndarray:
        im = image
    else:
        raise TypeError('Expected a numpy ndarray, or an image path. Got `{}`.'.format(type(image)))

    top_center_coord = np.array((im.shape[1]/2, 0))

    dets_d = testing.run_test(sess, net, im, n_classes, conf_thresh=conf_thresh, nms_thresh=nms_thresh)

    all_centers = []

    for cls_ind, class_name in enumerate(class_names):
        if cls_ind != 0:
            # centers: [(x,y,class),...]
            dets = dets_d[cls_ind]
            centers = np.empty(shape=(len(dets), 3), dtype=dets.dtype)
            centers[:,0] = (dets[:,0] + dets[:,2])/2
            centers[:,1] = (dets[:,1] + dets[:,3])/2
            centers[:,2] = cls_ind
            all_centers.append(centers)

    all_centers = np.concatenate(all_centers, axis=0)

    if len(all_centers) == 0:
        prediction = 0
    else:
        # Find the traffic light which is closest to the top-center, and get its prediction.
        vecs = all_centers[:,:2] - top_center_coord
        distances = np.linalg.norm(vecs, axis=1)
        min_distance_ind = np.argmin(distances)
        prediction = all_centers[min_distance_ind, 2]

    return {
        'n_detections': len(all_centers),
        'prediction': prediction,
        'all_centers': all_centers,
        'dets_d': dets_d,
        'top_center': top_center_coord,
    }

def plot_xtop_result(im, xtop_result, class_names, colors):
    """Plot image and detections."""

    n_classes = len(class_names)
    n_colors = len(class_names)
    if n_colors != n_classes:
        raise ValueError('n_classes != n_colors ({} != {})'.format(n_classes, n_colors))

    im = im[:,:,(2,1,0)]
    fig, ax = plt.subplots(1, 1)
    plt.axis('off')
    ax.imshow(im)
    for i, class_name, color in zip(range(n_classes), class_names, colors):
        if i != 0:
            dets = xtop_result['dets_d'][i]
            testing.draw_dets(ax, class_name, dets, color=color)
    ax.plot(xtop_result['top_center'][0], xtop_result['top_center'][1], 'bo')
    all_centers = xtop_result['all_centers']
    ax.scatter(all_centers[:,0], all_centers[:,1], marker='x', color=list(map(lambda c: CLASS_COLORS[int(c)], all_centers[:,2])))
    plt.show()

def predict_one(args):
    class_names = CLASSES[args.classes]
    class_colors = CLASS_COLORS[args.classes]
    n_classes = len(class_names)
    sess, net = testing.load_network(args.ckpt, n_classes, args.net, args.anchor_scales, args.anchor_ratios, tag=args.tag)

    image_path = args.image_path
    if args.base_dir is not None:
        image_path = os.path.join(args.base_dir, args.image_path)

    if not os.path.isfile(image_path):
        raise IOError('File `{}` not found. Maybe you forgot to inform `--base-dir`?'.format(image_path))

    im = cv2.imread(image_path)

    result = xtop_predict(sess, net, im, nms_thresh=args.nms_thresh, conf_thresh=args.conf_thresh)

    print('Image path:', image_path)
    print('Final prediction:', int(result['prediction']))
    print('Number of object detections:', int(result['n_detections']))

    plot_xtop_result(im, result, class_names, class_colors)

def predict_mult(args):
    class_names = CLASSES[args.classes]
    class_colors = CLASS_COLORS[args.classes]
    n_classes = len(class_names)
    sess, net = testing.load_network(args.ckpt, n_classes, args.net, args.anchor_scales, args.anchor_ratios, tag=args.tag)

    while True:
        print('Image path: ', end='', file=sys.stderr)
        sys.stderr.flush()
        image_path = sys.stdin.readline()
        image_path = image_path.strip()

        if re.search(r'^\s*$', image_path):
            break

        if args.base_dir is not None:
            image_path = os.path.join(args.base_dir, args.image_path)

        if not os.path.isfile(image_path):
            print('File `{}` not found. Maybe you forgot to inform `--base-dir`?'.format(image_path), file=sys.stderr)
            continue

        im = cv2.imread(image_path)

        result = xtop_predict(sess, net, im, nms_thresh=args.nms_thresh, conf_thresh=args.conf_thresh)

        print('Image path:', image_path)
        print('Final prediction:', int(result['prediction']))
        print('Number of object detections:', int(result['n_detections']))

        plot_xtop_result(im, result, class_names, class_colors)

def predict_csv_list(args):
    class_names = CLASSES[args.classes]
    n_classes = len(class_names)
    sess, net = testing.load_network(args.ckpt, n_classes, args.net, args.anchor_scales, args.anchor_ratios, tag=args.tag)

    # Load classification list
    file_list_df = pd.read_csv(args.classification_csv, names=['path', 'label'])
    file_list_df['abspath'] = file_list_df['path']
    if args.base_dir:
        file_list_df['abspath'] = list(map(lambda p: os.path.join(args.base_dir, p), file_list_df['abspath']))

    # Load cache file.
    if os.path.isfile(args.predictions_csv):
        predictions_df = pd.read_csv(args.predictions_csv)
    else:
        predictions_df = pd.DataFrame({ colname: pd.Series([], dtype=coltype) for colname, coltype in zip(PREDICTIONS_COLNAMES, PREDICTIONS_COLTYPES) })

    print(file=sys.stderr)
    for tup in file_list_df.itertuples():
        print('\x1b[A\x1b[2KINFO: {} of {}'.format(tup.Index+1, len(file_list_df)), file=sys.stderr)
        if not predictions_df['path'].isin([tup.path]).any():
            if not os.path.isfile(tup.abspath):
                raise IOError('File `{}` not found. Maybe you forgot to inform `--base-dir`?'.format(tup.abspath))
            result = xtop_predict(sess, net, tup.abspath, nms_thresh=args.nms_thresh, conf_thresh=args.conf_thresh)
            # Cache the prediction.
            predictions_df = predictions_df.append({
                'path': tup.path,
                'label': int(tup.label),
                'prediction': int(result['prediction']),
                'n_detections': int(result['n_detections']),
            }, ignore_index=True)
            predictions_df.to_csv(args.predictions_csv, columns=PREDICTIONS_COLNAMES, index=False)

    calculate_metrics(predictions_df, class_names=class_names)

def calculate_metrics(predictions_df, class_names=None):
    confmat = confusion_matrix(predictions_df['label'], predictions_df['prediction'], labels=range(len(class_names)))
    acc_micro = confmat.diagonal().sum() / confmat.sum()
    acc_per_class = confmat.diagonal() / confmat.sum(axis=1)
    acc_per_class_no_nan = acc_per_class[~np.isnan(acc_per_class)]
    acc_macro = acc_per_class_no_nan.mean()

    if class_names is None:
        class_names = range(len(confmat))

    print('Total images: {}'.format(len(predictions_df)))
    print('Confusion matrix:')
    print(confmat)
    print('Accuracy per class:')
    for label, label_name in enumerate(class_names):
        print(' - {}: {}'.format(label_name, acc_per_class[label]))
    print('Micro accuracy: {}'.format(acc_micro))
    print('Macro accuracy: {}'.format(acc_macro))

def metrics(args):
    # Load predictions file.
    predictions_df = pd.read_csv(args.predictions_csv)
    calculate_metrics(predictions_df, class_names=CLASSES)


def parse_args():
    parser = argparse.ArgumentParser(description='Tensorflow Faster R-CNN demo')

    common_parser = argparse.ArgumentParser(add_help=False)
    common_parser.add_argument('--net', help='Network to use', choices=NETS, default='res101')
    common_parser.add_argument('-s', '--anchor-scales', type=arg_comma_separated_float_list, required=True)
    common_parser.add_argument('-r', '--anchor-ratios', type=arg_comma_separated_float_list, required=True)
    common_parser.add_argument('--nms-thresh', type=float, default=0.3)
    common_parser.add_argument('--conf-thresh', type=float, default=0.8)
    common_parser.add_argument('--classes', choices=CLASSES, default=CLASSES[0], help='Which set of classes should be used.')
    common_parser.add_argument('--tag', default='default') # I don't think this is useful for anything.
    common_parser.add_argument('--cfg', dest='cfgs', action='append', help='''
        YAML file containing configuration. This option can be used multiple times.
        The configuration files will be applied in the order they were received
        from the command line.
        ''')
    common_parser.add_argument('--set', help='Space separated list o configuration keys and configuration values.')
    common_parser.add_argument('--base-dir', help='Base directory for the images.')

    subparsers = parser.add_subparsers(help='Available commands.', dest='command')
    subparsers.required = True

    predict_one_parser = subparsers.add_parser('predict_one', parents=[common_parser], help='Predict on a single image.')
    predict_one_parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')
    predict_one_parser.add_argument('image_path', help='CSV with (image path, and gt class).')

    predict_mult_parser = subparsers.add_parser('predict_mult', parents=[common_parser], help='Predict on multiple files from stdin.')
    predict_mult_parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')

    predict_csv_parser = subparsers.add_parser('predict_csv', parents=[common_parser], help='Predict all images from a classification csv file.')
    predict_csv_parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')
    predict_csv_parser.add_argument('classification_csv', help='CSV with (image path, and gt class).')
    predict_csv_parser.add_argument('predictions_csv', help='Where the predictions should be saved to.')

    # predict_dataset_parser = subparsers.add_parser('predict_dataset', parents=[common_parser], help='Predict all images from a given dataset.')
    # predict_dataset_parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')
    # predict_dataset_parser.add_argument('dataset', choices=..., help='Which dataset should it test on.')

    metrics_parser = subparsers.add_parser('metrics', help='Calculate metrics for a csv file of "xtop predictions".')
    metrics_parser.add_argument('predictions_csv', help='"xtop predictions" csv file.')

    args = parser.parse_args()

    return args

if __name__ == '__main__':
    args = parse_args()

    if args.cfgs is not None:
        for cfg_file in cfgs:
            cfg_from_file(cfg_file)
    if args.set is not None:
        cfg_from_list(args.set.split(' '))

    if args.command == 'predict_one':
        predict_one(args)
    elif args.command == 'predict_mult':
        predict_mult(args)
    elif args.command == 'predict_csv':
        predict_csv_list(args)
    elif args.command == 'metrics':
        metrics(args)
    else:
        print('ERR: Unknown command {}.'.format(args.command), file=sys.stderr)
        exit(1)
