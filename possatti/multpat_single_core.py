#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import argparse
import sys
import re
import os

def main():
	# Arguments and options.
	parser = argparse.ArgumentParser(description='Select lines from `--file-in` or `STDIN` that contain any line present in `patterns`.')
	parser.add_argument('patterns')
	parser.add_argument('-f', '--file-in')
	args = parser.parse_args()

	with open(args.patterns, 'r') as f:
		patterns = [ x.strip() for x in f.readlines() ]

	# print("patterns:\n", patterns, file=sys.stderr) #!#

	if args.file_in is None:
		input_file = sys.stdin
	else:
		input_file = open(args.file_in, 'r')
	lines = [ x.strip() for x in input_file.readlines() ]

	# print("lines:\n", lines, file=sys.stderr) #!#

	for line in lines:
		for pat in patterns:
			if pat in line:
				print(line)

if __name__ == '__main__':
	main()
