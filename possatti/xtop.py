#!/usr/bin/env python

"""
Demo script showing detections in sample images.

See README.md for installation instructions before running.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from sklearn.metrics import confusion_matrix

import _init_paths
from model.config import cfg, cfg_from_list, cfg_from_file
from model.test import im_detect
from model.nms_wrapper import nms
from utils.timer import Timer

import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import numpy as np
import argparse
import time
import glob
import cv2
import sys
import os
import re

from nets.vgg16 import vgg16
from nets.resnet_v1 import resnetv1

import flextop
import testing

# Use `raw_input` as `input` for Python 2.
if re.search(r'^2\.', sys.version):
    input = raw_input

CLASSES = {
    'NRG': ['None', 'Red', 'Green'],
    'NRGY': ['None', 'Red', 'Green', 'Yellow'],
}
CLASS_COLORS = {
    'NRG': ['black', 'red', 'green'],
    'NRGY': ['black', 'red', 'green', 'yellow'],
}

NETS = ['res101']

PREDICTIONS_COLNAMES = ['path', 'label', 'prediction', 'n_detections']
PREDICTIONS_COLTYPES = [str, int, int, int]

ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))

def arg_comma_separated_float_list(s):
    """Parse a string like '1., -2.1, 3, whatever' into a python list."""
    if type(s) != str:
        raise TypeError('Expected a string, got type `{}`.'.format(type(s)))
    pieces = re.split(r', *', s)
    pieces = [ float(p) for p in pieces ]
    return pieces

def arg_comma_separated_list(s):
    """Parse a string like '1., -2.1, 3, whatever' into a python list."""
    if type(s) != str:
        raise TypeError('Expected a string, got type `{}`.'.format(type(s)))
    pieces = re.split(r', *', s)
    return pieces

def flex_predict(sess, net, image, n_classes, conf_thresh=0.8, nms_thresh=0.3, plot=False, rule='xtop', rule_options={}):
    xtop_start = time.time()

    # Load image.
    if type(image) is str:
        if not os.path.isfile(image):
            raise ValueError('Couldn\'t load image from `{}`. File doesn\'t exist.')
        im = cv2.imread(image)
    elif type(image) is np.ndarray:
        im = image
    else:
        raise TypeError('Expected a numpy ndarray, or an image path. Got `{}`.'.format(type(image)))
    im_height, im_width, _ = im.shape

    # Run through the network.
    dets_d = testing.run_test(sess, net, im, n_classes, conf_thresh=conf_thresh, nms_thresh=nms_thresh)
    predictions = []
    for cls_ind in range(1, n_classes):
        for det in dets_d[cls_ind]:
            predictions.append({'x1': det[0], 'y1': det[1], 'x2': det[2], 'y2': det[3],
                'class_num': cls_ind, 'score': det[4]})

    fig = None
    if plot:
        fig, ax = plt.subplots(1, 1)
        ax.axis('off')
        ax.imshow(im[:,:,(2,1,0)])
        rule_options['plot_ax'] = ax

    # Apply rule.
    prediction = flextop.rules[rule](predictions, im_width, im_height, **rule_options)

    xtop_end = time.time()

    return {
        'n_detections': sum([ len(dets_d[c]) for c in dets_d ]),
        'prediction': prediction,
        'dets_d': dets_d,
        'figure': fig,
        'duration': xtop_end - xtop_start,
    }

def predict_one(args):
    class_names = CLASSES[args.labels]
    class_colors = CLASS_COLORS[args.labels]
    n_classes = len(class_names)
    sess, net = testing.load_network(args.ckpt, n_classes, args.net, args.anchor_scales, args.anchor_ratios, tag=args.tag)

    image_path = args.image_path
    if args.base_dir is not None:
        image_path = os.path.join(args.base_dir, args.image_path)

    if not os.path.isfile(image_path):
        raise IOError('File `{}` not found. Maybe you forgot to inform `--base-dir`?'.format(image_path))

    im = cv2.imread(image_path)

    result = flex_predict(sess, net, im, n_classes, rule=args.rule, plot=True, nms_thresh=args.nms_thresh, conf_thresh=args.conf_thresh)

    print('Image path:', image_path)
    print('Final prediction:', int(result['prediction']))
    print('Number of object detections:', int(result['n_detections']))
    print('Duration in seconds:', result['duration'])

    plt.show() # Show `result['figure']`

def predict_mult(args):
    class_names = CLASSES[args.labels]
    class_colors = CLASS_COLORS[args.labels]
    n_classes = len(class_names)
    sess, net = testing.load_network(args.ckpt, n_classes, args.net, args.anchor_scales, args.anchor_ratios, tag=args.tag)

    while True:
        print('Image path: ', end='', file=sys.stderr)
        sys.stderr.flush()
        image_path = sys.stdin.readline()
        image_path = image_path.strip()

        if re.search(r'^\s*$', image_path):
            break

        if args.base_dir is not None:
            image_path = os.path.join(args.base_dir, image_path)

        if not os.path.isfile(image_path):
            print('File `{}` not found. Maybe you forgot to inform `--base-dir`?'.format(image_path), file=sys.stderr)
            continue

        im = cv2.imread(image_path)

        result = flex_predict(sess, net, im, n_classes, rule=args.rule, plot=True, nms_thresh=args.nms_thresh, conf_thresh=args.conf_thresh)

        print('Image path:', image_path)
        print('Final prediction:', int(result['prediction']))
        print('Number of object detections:', int(result['n_detections']))
        print('Duration in seconds:', result['duration'])

        plt.show() # Show `result['figure']`

def predict_csv(args):
    class_names = CLASSES[args.labels]
    n_classes = len(class_names)
    sess, net = testing.load_network(args.ckpt, n_classes, args.net, args.anchor_scales, args.anchor_ratios, tag=args.tag)

    # Load classification list
    if re.match(r'.+\.csv$', args.classification_file):
        file_list_df = pd.read_csv(args.classification_file, names=['path', 'label'])
    elif re.match(r'.+\.txt$', args.classification_file):
        file_list_df = pd.read_csv(args.classification_file, sep=' ', names=['path', 'label'])
    else:
        raise ValueError('Unknown extension for classification file.')
    file_list_df['abspath'] = file_list_df['path']
    if args.base_dir:
        file_list_df['abspath'] = list(map(lambda p: os.path.join(args.base_dir, p), file_list_df['abspath']))

    # Load cache file.
    if os.path.isfile(args.predictions_csv):
        predictions_df = pd.read_csv(args.predictions_csv)
    else:
        predictions_df = pd.DataFrame({ colname: pd.Series([], dtype=coltype) for colname, coltype in zip(PREDICTIONS_COLNAMES, PREDICTIONS_COLTYPES) })

    print(file=sys.stderr)
    for tup in file_list_df.itertuples():
        print('\x1b[A\x1b[2KINFO: {} of {}'.format(tup.Index+1, len(file_list_df)), file=sys.stderr)
        if not predictions_df['path'].isin([tup.path]).any():
            if not os.path.isfile(tup.abspath):
                raise IOError('File `{}` not found. Maybe you forgot to inform `--base-dir`?'.format(tup.abspath))
            result = flex_predict(sess, net, tup.abspath, n_classes, rule=args.rule, nms_thresh=args.nms_thresh, conf_thresh=args.conf_thresh)
            # Cache the prediction.
            predictions_df = predictions_df.append({
                'path': tup.path,
                'label': int(tup.label),
                'prediction': int(result['prediction']),
                'n_detections': int(result['n_detections']),
            }, ignore_index=True)
            predictions_df.to_csv(args.predictions_csv, columns=PREDICTIONS_COLNAMES, index=False)

    calculate_metrics(predictions_df, class_names=class_names)

def calculate_metrics(predictions_df, class_names=None):
    confmat = confusion_matrix(predictions_df['label'], predictions_df['prediction'], labels=range(len(class_names)))
    acc_micro = confmat.diagonal().sum() / confmat.sum()
    acc_per_class = confmat.diagonal() / confmat.sum(axis=1)
    acc_per_class_no_nan = acc_per_class[~np.isnan(acc_per_class)]
    acc_macro = acc_per_class_no_nan.mean()

    if class_names is None:
        class_names = range(len(confmat))

    print('Total images: {}'.format(len(predictions_df)))
    print('Confusion matrix:')
    print(confmat)
    print('Accuracy per class:')
    for label, label_name in enumerate(class_names):
        print(' - {}: {}'.format(label_name, acc_per_class[label]))
    print('Micro accuracy: {}'.format(acc_micro))
    print('Macro accuracy: {}'.format(acc_macro))

def metrics(args):
    # Load predictions file.
    predictions_df = pd.read_csv(args.predictions_csv)
    calculate_metrics(predictions_df, class_names=CLASSES[args.labels])


def parse_args():
    parser = argparse.ArgumentParser(description='Tensorflow Faster R-CNN demo')

    common_parser = argparse.ArgumentParser(add_help=False)
    common_parser.add_argument('--net', help='Network to use', choices=NETS, default='res101')
    common_parser.add_argument('-s', '--anchor-scales', type=arg_comma_separated_float_list)
    common_parser.add_argument('-r', '--anchor-ratios', type=arg_comma_separated_float_list)
    common_parser.add_argument('--labels', choices=CLASSES, default='NRG', help='Which set of labels should be used.')
    common_parser.add_argument('--nms-thresh', type=float, default=0.3)
    common_parser.add_argument('--conf-thresh', type=float, default=0.8)
    common_parser.add_argument('--rule', choices=flextop.rules, default='xtop')
    common_parser.add_argument('--tag', default='default') # I don't think this is useful for anything.
    common_parser.add_argument('--cfg', dest='cfgs', action='append', help='''
        YAML file containing configuration. This option can be used multiple times.
        The configuration files will be applied in the order they were received
        from the command line.
        ''')
    common_parser.add_argument('--set', help='Space separated list o configuration keys and configuration values.')
    common_parser.add_argument('--base-dir', help='Base directory for the images.')

    subparsers = parser.add_subparsers(help='Available commands.', dest='command')
    subparsers.required = True

    predict_one_parser = subparsers.add_parser('predict_one', parents=[common_parser], help='Predict on a single image.')
    predict_one_parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')
    predict_one_parser.add_argument('image_path', help='CSV with (image path, and gt class).')

    predict_mult_parser = subparsers.add_parser('predict_mult', parents=[common_parser], help='Predict on multiple files from stdin.')
    predict_mult_parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')

    predict_csv_parser = subparsers.add_parser('predict_csv', parents=[common_parser], help='Predict all images from a classification csv file.')
    predict_csv_parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')
    predict_csv_parser.add_argument('classification_file', help='CSV with (image path, and gt class).')
    predict_csv_parser.add_argument('predictions_csv', help='Where the predictions should be saved to.')

    # predict_dataset_parser = subparsers.add_parser('predict_dataset', parents=[common_parser], help='Predict all images from a given dataset.')
    # predict_dataset_parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')
    # predict_dataset_parser.add_argument('dataset', choices=..., help='Which dataset should it test on.')

    metrics_parser = subparsers.add_parser('metrics', parents=[common_parser], help='Calculate metrics for a csv file of "xtop" predictions.')
    metrics_parser.add_argument('predictions_csv', help='"xtop predictions" csv file.')

    args = parser.parse_args()

    return args

if __name__ == '__main__':
    args = parse_args()

    if args.cfgs is not None:
        for cfg_file in args.cfgs:
            cfg_from_file(cfg_file)
    if args.set is not None:
        cfg_from_list(args.set.split(' '))

    if args.command == 'predict_one':
        predict_one(args)
    elif args.command == 'predict_mult':
        predict_mult(args)
    elif args.command == 'predict_csv':
        predict_csv(args)
    elif args.command == 'metrics':
        metrics(args)
    else:
        print('ERR: Unknown command {}.'.format(args.command), file=sys.stderr)
        exit(1)
