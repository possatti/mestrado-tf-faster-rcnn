#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

import flextop

print(sys.version, file=sys.stderr)


DATASET_NAMES = [
    'IARA',
    'LARA',
    'LISA-day-train',
    'LISA-day-test',
]
DATASET_STUBS = {
    'IARA': 'iara',
    'LARA': 'lara',
    'LISA-day-train': 'lisa-day-train',
    'LISA-day-test': 'lisa-day-test',
}

# DATASET_NAMES =

RULES = [
    'xtop',
    'fat_tl',
    'fat_twins',
    'random_select',
]
HEURISTIC_NAME = {
    'xtop': '1H',
    'fat_tl': '1L',
    'fat_twins': '1H-2L',
    'random_select': 'R',
}

def parse_args():
    parser = argparse.ArgumentParser(description='')
    # parser.add_argument('arg')
    # parser.add_argument('-o', '--opt')
    args = parser.parse_args()
    return args

def assign(obj, **kwargs):
    for key in kwargs:
        obj[key] = kwargs[key]

def main():
    args = parse_args()

    frcnn_result_template = '/home/lucaspossatti/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s{i}/{rule}/y2018-10-{ds_stub}-nrgr--predictions.csv'
    n_min = 0
    n_max = 5

    print('INFO: Collecting files...', file=sys.stderr)
    accs = []
    for d, ds_name in enumerate(DATASET_NAMES):
        ds_stub = DATASET_STUBS[ds_name]
        for r, rule in enumerate(RULES):
            for i in range(10):
                predictions_csv_path = frcnn_result_template.format(i=i, rule=rule, ds_stub=ds_stub)
                predictions_df = pd.read_csv(predictions_csv_path)
                predictions_df.assign(ds_name=ds_name, rule=rule, i=i)

                for n in range(n_min, n_max):
                    # sub_mask = (predictions_df['label']!=0) & (predictions_df['prediction']!=0)
                    sub_mask = (predictions_df['label']!=0) & (predictions_df['n_detections']>=n)
                    sub = predictions_df.loc[sub_mask]


                    d_metrics = flextop.calc_metrics(sub['label'].values, sub['prediction'].values)

                    del d_metrics['confmat'], d_metrics['acc_per_class']
                    assign(d_metrics, ds_name=ds_name, rule=rule, i=i, n=n,
                        n_images=len(sub.index),
                    )
                    accs.append(d_metrics)

            print('INFO: {} of {}.\r'.format(i+r*10+d*len(RULES)*10 + 1, 10*len(RULES)*len(DATASET_NAMES)), end='', file=sys.stderr)
    print(file=sys.stderr)
    accs = pd.DataFrame(accs)
    print("accs:\n{}".format(accs), file=sys.stderr) #!#

    for d, ds_name in enumerate(DATASET_NAMES):
        for r, rule in enumerate(RULES):
            for n in range(n_min, n_max):
                heuristic = HEURISTIC_NAME[rule]
                sub_mask = (accs['ds_name']==ds_name) & (accs['rule']==rule) & (accs['n']==n)
                sub = accs.loc[sub_mask]
                accs.loc[sub_mask, 'macro_mean_i'] = sub['acc_macro'].mean()

    image_count = accs.drop_duplicates(subset=['ds_name', 'n'], keep='first')
    print("\nJust checking the image count:\n{}".format(image_count), file=sys.stderr) #!#

    print("\nJust checking the mean difference on n=0:", file=sys.stderr) #!#
    the_relevant_xtop = accs.query('i==0 and n==0 and rule=="xtop"')
    the_relevant_random = accs.query('i==0 and n==0 and rule=="random_select"')
    the_difference = the_relevant_xtop['macro_mean_i'].values - the_relevant_random['macro_mean_i'].values
    print(the_relevant_xtop, '\n')
    print(the_relevant_random, '\n')
    print(the_difference, '\n')

    print("\nJust checking the mean difference on n=3:", file=sys.stderr) #!#
    the_relevant_xtop = accs.query('i==0 and n==3 and rule=="xtop"')
    the_relevant_random = accs.query('i==0 and n==3 and rule=="random_select"')
    the_difference = the_relevant_xtop['macro_mean_i'].values - the_relevant_random['macro_mean_i'].values
    print(the_relevant_xtop, '\n')
    print(the_relevant_random, '\n')
    print(the_difference, '\n')

    sns.set(
        context='paper', style='darkgrid', font_scale=1.1,
    )
    sns.set_palette('muted')

    sns.catplot(
        x='n', y='acc_macro', hue='rule',
        data=accs,
        kind='box',
        col='ds_name',
    )
    plt.show()




if __name__ == '__main__':
    main()
