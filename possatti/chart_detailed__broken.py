#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

import flextop

print(sys.version, file=sys.stderr)


DATASET_NAMES = [
    # 'IARA',
    # 'LARA',
    'LISA-day-train',
    'LISA-day-test',
]
DATASET_STUBS = {
    'IARA': 'iara',
    'LARA': 'lara',
    'LISA-day-train': 'lisa-day-train',
    'LISA-day-test': 'lisa-day-test',
}

RULES = [
    'xtop',
    # 'fat_tl',
    # 'fat_twins',
    'random_select',
]
HEURISTIC_NAME = {
    'xtop': '1H',
    'fat_tl': '1L',
    'fat_twins': '1H-2L',
    'random_select': 'R',
}

def parse_args():
    parser = argparse.ArgumentParser(description='')
    # parser.add_argument('arg')
    # parser.add_argument('-o', '--opt')
    args = parser.parse_args()
    return args

def chart_ignore_detector_performance(df):
    df = df.loc[(df['label']!=0) & df['prediction']!=0]

    accs = []
    for d, ds_name in enumerate(DATASET_NAMES):
        for r, rule in enumerate(RULES):
            for i in range(10):
                print("ds_name:\n{}".format(ds_name), file=sys.stderr) #!#
                print("rule:\n{}".format(rule), file=sys.stderr) #!#
                print("i:\n{}".format(i), file=sys.stderr) #!#
                sub = df.loc[(df['ds_name']==ds_name)&(df['rule']==rule)&(df['i']==i)]
                # dt = flextop.calc_metrics(sub['label'].values, sub['prediction'].values, range(3))
                # with pd.option_context('display.max_rows', None):
                #     print("sub:\n{}".format(sub.drop(columns=['path'])), file=sys.stderr) #!#
                unq = np.unique(sub['labels'])
                dt = flextop.calc_metrics(sub['label'].values, sub['prediction'].values)
                print("dt:\n{}".format(dt), file=sys.stderr) #!#
                del dt['confmat'], dt['acc_per_class']
                dt['ds_name'] = ds_name
                dt['rule'] = rule
                dt['i'] = i
                accs.append(dt)
                break
            break
        break
    accs = pd.DataFrame(accs)
    print("accs:\n{}".format(accs), file=sys.stderr) #!#
    # print("df:\n{}".format(df), file=sys.stderr) #!#


def main():
    args = parse_args()

    frcnn_result_template = '/home/lucaspossatti/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s{i}/{rule}/y2018-10-{ds_stub}-nrgr--predictions.csv'

    print('INFO: Collecting files...', file=sys.stderr)
    dfs = []
    for d, ds_name in enumerate(DATASET_NAMES):
        ds_stub = DATASET_STUBS[ds_name]
        for r, rule in enumerate(RULES):
            for i in range(10):
                predictions_csv_path = frcnn_result_template.format(i=i, rule=rule, ds_stub=ds_stub)
                predictions_df = pd.read_csv(predictions_csv_path)
                predictions_df['ds_name'] = ds_name
                predictions_df['rule'] = rule
                predictions_df['i'] = i
                dfs.append(predictions_df)
                print("ds_name:\n{}".format(ds_name), file=sys.stderr) #!#
                print("rule:\n{}".format(rule), file=sys.stderr) #!#
                print("i:\n{}".format(i), file=sys.stderr) #!#
                count_label = np.unique(predictions_df['label'].values)
                count_prediction = np.unique(predictions_df['prediction'].values)
                print("count_label:\n{}".format(count_label), file=sys.stderr) #!#
                print("count_prediction:\n{}".format(count_prediction), file=sys.stderr) #!#
                exit(3)
            print('INFO: {} of {}.\r'.format(i+r*10+d*len(RULES)*10 + 1, 10*len(RULES)*len(DATASET_NAMES)), end='', file=sys.stderr)
    print(file=sys.stderr)
    df = pd.concat(dfs, ignore_index=True)

    chart_ignore_detector_performance(df)


if __name__ == '__main__':
    main()
