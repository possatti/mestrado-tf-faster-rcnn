#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

print(sys.version, file=sys.stderr)



def parse_args():
    parser = argparse.ArgumentParser(description='')
    # parser.add_argument('arg')
    # parser.add_argument('-o', '--opt')
    args = parser.parse_args()
    return args

def main():
    args = parse_args()

    frcnn_result_template = '/home/lucaspossatti/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s{i}/{rule}/y2018-10-{ds_stub}-nrgr--predictions.csv'
    dataset_names = [
        'iara',
        'lara',
        'lisa-day-train',
        'lisa-day-test',
    ]

    rules = ['xtop', 'fat_tl', 'fat_twins', 'random_select']

    print('INFO: Collecting files...', file=sys.stderr)
    counts = []
    for ds_stub in dataset_names:
        for rule in rules:
            for i in range(10):
                predictions_csv_path = frcnn_result_template.format(i=i, rule=rule, ds_stub=ds_stub)
                df = pd.read_csv(predictions_csv_path)
                item = {'ds_name': ds_stub, 'rule': rule, 'i': i}
                item['n_misses'] = ((df['label']!=0) & (df['prediction']==0)).sum()
                item['n_confusions_pred1'] = ((df['label']==2) & (df['prediction']==1)).sum()
                item['n_confusions_pred2'] = ((df['label']==1) & (df['prediction']==2)).sum()
                item['n_confusions'] = item['n_confusions_pred1'] + item['n_confusions_pred2']
                item['n_errors'] = item['n_confusions'] + item['n_misses']
                item['n_images'] = len(df)
                counts.append(item)

    print('INFO: Putting into a Data Frame...', file=sys.stderr)
    counts = pd.DataFrame(counts)
    counts.astype({'rule': 'category'})
    error_types = ['misses', 'confusions_pred1', 'confusions_pred2', 'confusions']
    for x in error_types:
        counts['p_'+x] = counts['n_'+x] / counts['n_images']
    print("counts:\n{}".format(counts), file=sys.stderr) #!#

    print('INFO: Plotting...', file=sys.stderr)
    sns.set(
        context='paper', style='darkgrid', font_scale=1.1,
    )
    sns.set_palette('muted')

    # sns.catplot(
    #     x='rule', y='p_misses', data=counts,
    #     kind='box',
    #     # hue='Model', hue_order=['Faster R-CNN', 'YOLOv3'],
    #     col='ds_name', col_order=dataset_names,
    #     height=3, aspect=1, # width / height
    #     margin_titles=True, fliersize=1, width=0.8, linewidth=1.0,
    #     legend=False,
    # )

    # error_types = ['misses', 'confusions']
    fig, axes = plt.subplots(len(error_types), len(dataset_names), sharey=True)
    for d, ds_name in enumerate(dataset_names):
        sub = counts.loc[(counts['ds_name']==ds_name)]
        for e, error_type in enumerate(error_types):
            ax = axes[e,d]
            sns.catplot(x='rule', y='p_'+error_type, data=sub, kind='box', ax=ax)
            if e == 0:
                ax.set_title(ds_name)
                # ax.set_title('{} {}'.format(ds_name, error_type))


    import tkinter as tk
    fig.show()
    tk.mainloop()
    input() # Prevents main thread from terminating. https://stackoverflow.com/q/39840815/4630320

if __name__ == '__main__':
    main()
