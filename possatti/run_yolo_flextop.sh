#!/usr/bin/env bash

## Set some bash options.
##  - http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit  # make script exit when a command fails.
# set -o nounset  # exit when script tries to use undeclared variables.
# set -o xtrace   # trace what gets executed.

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

usage() {
	echo "usage: $0 DATA_CFG NET_CFG WEIGHTS DATASET_GROUP --rule [xtop,fat_tl,fat_twins]" 1>&2
}

ARGS=()
RULE="xtop"
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		-h|--help)
			usage
			exit
			;;
		--rule)
			RULE="$2"
			shift 2
			;;
		*)
			ARGS+=("$1")
			shift
			;;
	esac
done

echo "POSITIONAL ARGS: ${ARGS[@]}" 1>&2

# Check number of arguments.
ARGS_N=4
if [[ "${#ARGS[@]}" -ne "$ARGS_N" ]]; then
	echo "ERR: Expected $ARGS_N positional arguments. Got ${#ARGS[@]}." 1>&2
	usage
	exit 1
fi

# Unpack positional args.
DATA_CFG="${ARGS[0]}"
NET_CFG="${ARGS[1]}"
WEIGHTS="${ARGS[2]}"
DATASET_GROUP="${ARGS[3]}"

get_name() {
	echo -n $(echo -n "$1" | perl -ne 'm:/([^/]+)\.\w+$:; print $1')
}

if [ -f "$DATA_CFG" ]; then
	DATA_CFG_NAME=$(get_name "$DATA_CFG")
else
	echo "DATA_CFG doesn't exist. Path: '${DATA_CFG}'." 1>&2
	usage 1>&2
	exit 1
fi
if [ -f "$NET_CFG" ]; then
	NET_CFG_NAME=$(get_name "$NET_CFG")
else
	echo "NET_CFG doesn't exist. Path: '${NET_CFG}'." 1>&2
	usage 1>&2
	exit 1
fi
if [ -f "$WEIGHTS" ]; then
	WEIGHTS_NAME=$(get_name "$WEIGHTS")
else
	echo "WEIGHTS doesn't exist. Path: '${WEIGHTS}'." 1>&2
	usage 1>&2
	exit 1
fi

case "$DATASET_GROUP" in
	# ALL_NRG)
	# 	TEST_NAMES=(GOOGLE_TL_TEST IARA LARA LISA_DAY_TRAIN LISA_DAY_TEST)
	# 	;;
	AUG0_NRG)
		TEST_NAMES=("GOOGLE_AUG0_VAL_BBOX" "IARA_AUG0" "LARA_AUG0" "LISA_DAY_TRAIN_AUG0" "LISA_DAY_TEST_AUG0")
		LABELS="NRG"
		;;
	AUG0_NRGY)
		TEST_NAMES=("GOOGLE_AUG0_NRGY" "IARA_AUG0_NRGY")
		LABELS="NRGY"
		;;
	AUG0_NRGR)
		TEST_NAMES=("GOOGLE_AUG0_NRGR" "IARA_AUG0_NRGR" "VITORIA_TL_NRGR")
		LABELS="NRG"
		;;
	TL640_NRGR)
		TEST_NAMES=("TL640_GOOGLE_NRGR" "TL640_IARA_NRGR" "TL640_LARA_NRGR" "TL640_LISA_DAY_TEST_NRGR" "TL640_LISA_DAY_TRAIN_NRGR")
		LABELS="NRG"
		;;
	AUG0_AND_TL640_NRGR)
		TEST_NAMES=("GOOGLE_AUG0_NRGR" "IARA_AUG0_NRGR" "TL640_LARA_NRGR" "TL640_LISA_DAY_TEST_NRGR" "TL640_LISA_DAY_TRAIN_NRGR" "VITORIA_TL_NRGR")
		LABELS="NRG"
		;;
	Y2018_10_NRGR)
		TEST_NAMES=("Y2018_10_GOOGLE_AUG00_NRGR" "Y2018_10_IARA_NRGR" "Y2018_10_LARA_NRGR" "Y2018_10_LISA_DAY_TEST_NRGR" "Y2018_10_LISA_DAY_TRAIN_NRGR" "Y2018_10_VITORIA")
		LABELS="NRG"
		;;
	*)
		echo "ERROR: Unrecognized value for DATASET_GROUP: '${DATASET_GROUP}'."
		usage 1>&2
		exit 1
		;;
esac

RULE_RESULTS_DIR="results_yolo/${DATA_CFG_NAME}/${NET_CFG_NAME}/${WEIGHTS_NAME}/${RULE}"
RESULTS_DIR="results_yolo/${DATA_CFG_NAME}/${NET_CFG_NAME}/${WEIGHTS_NAME}"

echo "DATA_CFG: ${DATA_CFG}" #!#
echo "NET_CFG: ${NET_CFG}" #!#
echo "WEIGHTS: ${WEIGHTS}" #!#
echo "DATASET_GROUP: ${DATASET_GROUP}" #!#
echo "RULE_RESULTS_DIR: ${RULE_RESULTS_DIR}" #!#
echo "RESULTS_DIR: ${RESULTS_DIR}" #!#
echo

mkdir -p "$RULE_RESULTS_DIR"
mkdir -p "$RESULTS_DIR"

# Gather test data in an associative array.
declare -A TEST_DATA

# Original images.
#TEST_DATA[GOOGLE_TL_TEST,CSV_PATH]="data/annotations/classification_nrg/google_tl/google_tl_classification_nrg_possatti_yellow_fix_test.csv"
#TEST_DATA[GOOGLE_TL_TEST,PREDICTIONS_CSV_NAME]="google-tl-test-xtop-predictions.csv"
#TEST_DATA[GOOGLE_TL_TEST,BASE_DIR]="/dados/datasets/traffic-lights/google-tl"
#TEST_DATA[GOOGLE_TL_TEST,SUMMARY_NAME]="google-tl-test-xtop-predictions-summary.txt"
#TEST_DATA[IARA,CSV_PATH]="data/annotations/classification_nrg/iara/horimoto_IARA_classification_nrg_no_small.csv"
#TEST_DATA[IARA,PREDICTIONS_CSV_NAME]="iara-xtop-predictions.csv"
#TEST_DATA[IARA,BASE_DIR]="/dados/datasets/traffic-lights-franco/BASES"
#TEST_DATA[IARA,SUMMARY_NAME]="iara-xtop-predictions-summary.txt"
#TEST_DATA[LARA,CSV_PATH]="data/annotations/classification_nrg/lara/horimoto_LARA_classification_nrg_no_small.csv"
#TEST_DATA[LARA,PREDICTIONS_CSV_NAME]="lara-xtop-predictions.csv"
#TEST_DATA[LARA,BASE_DIR]="/dados/datasets/traffic-lights-franco/BASES"
#TEST_DATA[LARA,SUMMARY_NAME]="lara-xtop-predictions-summary.txt"
#TEST_DATA[LISA_DAY_TRAIN,CSV_PATH]="data/annotations/classification_nrg/lisa/horimoto_LISA_classification_nrg_no_small_day_train.csv"
#TEST_DATA[LISA_DAY_TRAIN,PREDICTIONS_CSV_NAME]="lisa-day-train-xtop-predictions.csv"
#TEST_DATA[LISA_DAY_TRAIN,BASE_DIR]="/dados/datasets/traffic-lights-franco/BASES"
#TEST_DATA[LISA_DAY_TRAIN,SUMMARY_NAME]="lisa-day-train-xtop-predictions-summary.txt"
#TEST_DATA[LISA_DAY_TEST,CSV_PATH]="data/annotations/classification_nrg/lisa/horimoto_LISA_classification_nrg_no_small_day_test.csv"
#TEST_DATA[LISA_DAY_TEST,PREDICTIONS_CSV_NAME]="lisa-day-test-xtop-predictions.csv"
#TEST_DATA[LISA_DAY_TEST,BASE_DIR]="/dados/datasets/traffic-lights-franco/BASES"
#TEST_DATA[LISA_DAY_TEST,SUMMARY_NAME]="lisa-day-test-xtop-predictions-summary.txt"

# NRG "aug0" images.
TEST_DATA[GOOGLE_AUG0_VAL_BBOX,CSV_PATH]="data/annotations/classification_aug0_nrg/bal_google_val-bbox.csv"
TEST_DATA[GOOGLE_AUG0_VAL_BBOX,PREDICTIONS_CSV_NAME]="google-aug0-val-bbox--predictions.csv"
TEST_DATA[GOOGLE_AUG0_VAL_BBOX,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[GOOGLE_AUG0_VAL_BBOX,SUMMARY_NAME]="google-aug0-val-bbox--summary.txt"
TEST_DATA[IARA_AUG0,CSV_PATH]="data/annotations/classification_aug0_nrg/bal_IARA.csv"
TEST_DATA[IARA_AUG0,PREDICTIONS_CSV_NAME]="iara-aug0--predictions.csv"
TEST_DATA[IARA_AUG0,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[IARA_AUG0,SUMMARY_NAME]="iara-aug0--summary.txt"
TEST_DATA[LARA_AUG0,CSV_PATH]="data/annotations/classification_aug0_nrg/bal_LARA.csv"
TEST_DATA[LARA_AUG0,PREDICTIONS_CSV_NAME]="lara-aug0--predictions.csv"
TEST_DATA[LARA_AUG0,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[LARA_AUG0,SUMMARY_NAME]="lara-aug0--summary.txt"
TEST_DATA[LISA_DAY_TRAIN_AUG0,CSV_PATH]="data/annotations/classification_aug0_nrg/bal_LISA_day_train.csv"
TEST_DATA[LISA_DAY_TRAIN_AUG0,PREDICTIONS_CSV_NAME]="lisa-day-train-aug0--predictions.csv"
TEST_DATA[LISA_DAY_TRAIN_AUG0,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[LISA_DAY_TRAIN_AUG0,SUMMARY_NAME]="lisa-day-train-aug0--summary.txt"
TEST_DATA[LISA_DAY_TEST_AUG0,CSV_PATH]="data/annotations/classification_aug0_nrg/bal_LISA_day_test.csv"
TEST_DATA[LISA_DAY_TEST_AUG0,PREDICTIONS_CSV_NAME]="lisa-day-test-aug0--predictions.csv"
TEST_DATA[LISA_DAY_TEST_AUG0,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[LISA_DAY_TEST_AUG0,SUMMARY_NAME]="lisa-day-test-aug0--summary.txt"

# NRGY "aug0" images.
TEST_DATA[GOOGLE_AUG0_NRGY,CSV_PATH]="data/annotations/classification_aug0_nrgy/google_aug0_nrgy_test.csv"
TEST_DATA[GOOGLE_AUG0_NRGY,PREDICTIONS_CSV_NAME]="google-aug0-nrgy-test--predictions.csv"
TEST_DATA[GOOGLE_AUG0_NRGY,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[GOOGLE_AUG0_NRGY,SUMMARY_NAME]="google-aug0-nrgy-test--summary.txt"
TEST_DATA[IARA_AUG0_NRGY,CSV_PATH]="data/annotations/classification_aug0_nrgy/IARA_aug0_nrgy.csv"
TEST_DATA[IARA_AUG0_NRGY,PREDICTIONS_CSV_NAME]="iara-aug0-nrgy--predictions.csv"
TEST_DATA[IARA_AUG0_NRGY,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[IARA_AUG0_NRGY,SUMMARY_NAME]="iara-aug0-nrgy--summary.txt"
# TEST_DATA[LARA_AUG0_NRGY,CSV_PATH]="data/annotations/classification_aug0_nrg/bal_LARA.csv"
# TEST_DATA[LARA_AUG0_NRGY,PREDICTIONS_CSV_NAME]="lara-aug0--predictions.csv"
# TEST_DATA[LARA_AUG0_NRGY,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
# TEST_DATA[LARA_AUG0_NRGY,SUMMARY_NAME]="lara-aug0--summary.txt"
# TEST_DATA[LISA_DAY_TRAIN_AUG0_NRGY,CSV_PATH]="data/annotations/classification_aug0_nrg/bal_LISA_day_train.csv"
# TEST_DATA[LISA_DAY_TRAIN_AUG0_NRGY,PREDICTIONS_CSV_NAME]="lisa-day-train-aug0--predictions.csv"
# TEST_DATA[LISA_DAY_TRAIN_AUG0_NRGY,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
# TEST_DATA[LISA_DAY_TRAIN_AUG0_NRGY,SUMMARY_NAME]="lisa-day-train-aug0--summary.txt"
# TEST_DATA[LISA_DAY_TEST_AUG0_NRGY,CSV_PATH]="data/annotations/classification_aug0_nrg/bal_LISA_day_test.csv"
# TEST_DATA[LISA_DAY_TEST_AUG0_NRGY,PREDICTIONS_CSV_NAME]="lisa-day-test-aug0--predictions.csv"
# TEST_DATA[LISA_DAY_TEST_AUG0_NRGY,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
# TEST_DATA[LISA_DAY_TEST_AUG0_NRGY,SUMMARY_NAME]="lisa-day-test-aug0--summary.txt"

# NRGR "aug0" images.
TEST_DATA[GOOGLE_AUG0_NRGR,CSV_PATH]="data/annotations/classification_aug0_nrgr/google_aug0_nrgr_test.csv"
TEST_DATA[GOOGLE_AUG0_NRGR,PREDICTIONS_CSV_NAME]="google-aug0-nrgr-test--predictions.csv"
TEST_DATA[GOOGLE_AUG0_NRGR,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[GOOGLE_AUG0_NRGR,SUMMARY_NAME]="google-aug0-nrgr-test--summary.txt"
TEST_DATA[IARA_AUG0_NRGR,CSV_PATH]="data/annotations/classification_aug0_nrgr/IARA_aug0_nrgr.csv"
TEST_DATA[IARA_AUG0_NRGR,PREDICTIONS_CSV_NAME]="iara-aug0-nrgr--predictions.csv"
TEST_DATA[IARA_AUG0_NRGR,BASE_DIR]="/mnt/DADOS/horimoto/traffic_lights/balanced/"
TEST_DATA[IARA_AUG0_NRGR,SUMMARY_NAME]="iara-aug0-nrgr--summary.txt"
TEST_DATA[VITORIA_TL_NRGR,CSV_PATH]="data/annotations/classification_aug0_nrgr/vitoria_2018_09_nrgr.csv"
TEST_DATA[VITORIA_TL_NRGR,PREDICTIONS_CSV_NAME]="vitoria-tl-nrgr--predictions.csv"
TEST_DATA[VITORIA_TL_NRGR,BASE_DIR]="/dados/datasets/traffic-lights/vitoria-tl-2018-09"
TEST_DATA[VITORIA_TL_NRGR,SUMMARY_NAME]="vitoria-tl-nrgr--summary.txt"

# # TL640.
# TL640_BASE_DIR="/dados/datasets/traffic_lights"
# TEST_DATA[TL640_GOOGLE_NRGR,CSV_PATH]="data/annotations/original_tl640/classification_nrgr/horimoto_google_nrgr.txt"
# TEST_DATA[TL640_GOOGLE_NRGR,PREDICTIONS_CSV_NAME]="tl640_google_nrgr--predictions.csv"
# TEST_DATA[TL640_GOOGLE_NRGR,BASE_DIR]="$TL640_BASE_DIR"
# TEST_DATA[TL640_GOOGLE_NRGR,SUMMARY_NAME]="tl640_google_nrgr--summary.txt"
# TEST_DATA[TL640_IARA_NRGR,CSV_PATH]="data/annotations/original_tl640/classification_nrgr/horimoto_IARA_nrgr.txt"
# TEST_DATA[TL640_IARA_NRGR,PREDICTIONS_CSV_NAME]="tl640_iara_nrgr--predictions.csv"
# TEST_DATA[TL640_IARA_NRGR,BASE_DIR]="$TL640_BASE_DIR"
# TEST_DATA[TL640_IARA_NRGR,SUMMARY_NAME]="tl640_iara_nrgr--summary.txt"
# TEST_DATA[TL640_LARA_NRGR,CSV_PATH]="data/annotations/original_tl640/classification_nrgr/horimoto_LARA_nrgr.txt"
# TEST_DATA[TL640_LARA_NRGR,PREDICTIONS_CSV_NAME]="tl640_lara_nrgr--predictions.csv"
# TEST_DATA[TL640_LARA_NRGR,BASE_DIR]="$TL640_BASE_DIR"
# TEST_DATA[TL640_LARA_NRGR,SUMMARY_NAME]="tl640_lara_nrgr--summary.txt"
# TEST_DATA[TL640_LISA_DAY_TEST_NRGR,CSV_PATH]="data/annotations/original_tl640/classification_nrgr/horimoto_LISA_day_test_nrgr.txt"
# TEST_DATA[TL640_LISA_DAY_TEST_NRGR,PREDICTIONS_CSV_NAME]="tl640_lisa_day_test_nrgr--predictions.csv"
# TEST_DATA[TL640_LISA_DAY_TEST_NRGR,BASE_DIR]="$TL640_BASE_DIR"
# TEST_DATA[TL640_LISA_DAY_TEST_NRGR,SUMMARY_NAME]="tl640_lisa_day_test_nrgr--summary.txt"
# TEST_DATA[TL640_LISA_DAY_TRAIN_NRGR,CSV_PATH]="data/annotations/original_tl640/classification_nrgr/horimoto_LISA_day_train_nrgr.txt"
# TEST_DATA[TL640_LISA_DAY_TRAIN_NRGR,PREDICTIONS_CSV_NAME]="tl640_lisa_day_train_nrgr--predictions.csv"
# TEST_DATA[TL640_LISA_DAY_TRAIN_NRGR,BASE_DIR]="$TL640_BASE_DIR"
# TEST_DATA[TL640_LISA_DAY_TRAIN_NRGR,SUMMARY_NAME]="tl640_lisa_day_train_nrgr--summary.txt"

# Y2018_10 NRGR.
TL640_BASE_DIR="/dados/datasets/traffic_lights"
TEST_DATA[Y2018_10_GOOGLE_AUG00_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/2018_10_val_google.txt"
TEST_DATA[Y2018_10_GOOGLE_AUG00_NRGR,BASE_DIR]="/dados/horimoto/traffic_lights/2018_10/balanced"
TEST_DATA[Y2018_10_IARA_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/IARA.txt"
TEST_DATA[Y2018_10_IARA_NRGR,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[Y2018_10_IARA_NRGR,DIFFICULT_CASES]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/IARA_hard.txt"
TEST_DATA[Y2018_10_LARA_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LARA.txt"
TEST_DATA[Y2018_10_LARA_NRGR,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[Y2018_10_LARA_NRGR,DIFFICULT_CASES]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LARA_hard.txt"
TEST_DATA[Y2018_10_LISA_DAY_TEST_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_day_test.txt"
TEST_DATA[Y2018_10_LISA_DAY_TEST_NRGR,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[Y2018_10_LISA_DAY_TRAIN_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_day_train.txt"
TEST_DATA[Y2018_10_LISA_DAY_TRAIN_NRGR,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[Y2018_10_LISA_DAY_TRAIN_NRGR,DIFFICULT_CASES]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_hard.txt"
TEST_DATA[Y2018_10_VITORIA,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/vitoria(2018_09).txt"
TEST_DATA[Y2018_10_VITORIA,BASE_DIR]="$TL640_BASE_DIR"

for TEST_NAME in "${TEST_NAMES[@]}"; do
	LOWER_NAME=$(echo -n "$TEST_NAME" | perl -pe 's/_/-/g;$_=lc')
	DETECTIONS_PATH="${RESULTS_DIR}/${LOWER_NAME}--detections.txt"
	SUMMARY_PATH="${RULE_RESULTS_DIR}/${LOWER_NAME}--summary.txt"
	echo -e "\nProcessing ${TEST_NAME} for rule ${RULE}..."
	if [[ ! -s "${DETECTIONS_PATH}" ]]; then
		cat "${TEST_DATA[$TEST_NAME,CSV_PATH]}" | \
			perl -pe "s/ .*//; s:^:${TEST_DATA[$TEST_NAME,BASE_DIR]}/:" | \
			darknet detector print "$DATA_CFG" "$NET_CFG" "$WEIGHTS" \
			> "${DETECTIONS_PATH}"
	fi
	if [[ ! -s "${SUMMARY_PATH}" ]]; then
		python "${SCRIPT_DIR}/flextop.py" "${TEST_DATA[$TEST_NAME,CSV_PATH]}" "$DETECTIONS_PATH" \
			--rule "$RULE"  \
			--bg0 \
			--base-dir "${TEST_DATA[$TEST_NAME,BASE_DIR]}" \
			--labels "$LABELS" \
			> "${SUMMARY_PATH}"
	fi
	if [[ -n "${TEST_DATA[$TEST_NAME,DIFFICULT_CASES]}" ]]; then
		echo -e "\nProcessing ${TEST_NAME} (difficult) for rule ${RULE}..."
		DIFFICULT_DETECTIONS_PATH="${RESULTS_DIR}/${LOWER_NAME}-difficult--detections.txt"
		DIFFICULT_SUMMARY_PATH="${RULE_RESULTS_DIR}/${LOWER_NAME}-difficult--summary.txt"
		# Filter the detections searching the difficult images.
		if [[ ! -s "$DIFFICULT_DETECTIONS_PATH" ]]; then
			python "${SCRIPT_DIR}/multpat.py" \
				<(cat "${TEST_DATA[$TEST_NAME,DIFFICULT_CASES]}" | perl -pe 's: .*::') \
				-f "$DETECTIONS_PATH" \
				> "$DIFFICULT_DETECTIONS_PATH"
		fi
		# Classify the difficult cases.
		if [[ ! -s "$DIFFICULT_SUMMARY_PATH" ]]; then
			python "${SCRIPT_DIR}/flextop.py" "${TEST_DATA[$TEST_NAME,DIFFICULT_CASES]}" "$DIFFICULT_DETECTIONS_PATH" \
				--rule "$RULE"  \
				--bg0 \
				--base-dir "${TEST_DATA[$TEST_NAME,BASE_DIR]}" \
				--labels "$LABELS" \
				> "${DIFFICULT_SUMMARY_PATH}"
		fi
	fi
done

# Concatenate all summaries into a single file.
echo "INFO: Concatenating summaries..."
DATASET_GROUP_LOWER=$(echo -n "$DATASET_GROUP" | perl -pe 's/_/-/g;$_=lc')
A_SUMMARY_PATH="${RULE_RESULTS_DIR}/a-summary-${DATASET_GROUP_LOWER}.txt"
if [ -f "$A_SUMMARY_PATH" ]; then
	rm "$A_SUMMARY_PATH"
fi

for TEST_NAME in "${TEST_NAMES[@]}"; do
	LOWER_NAME=$(echo -n "$TEST_NAME" | perl -pe 's/_/-/g;$_=lc')
	# SUMMARY_NAME="${LOWER_NAME}--summary.txt"
	SUMMARY_PATH="${RULE_RESULTS_DIR}/${LOWER_NAME}--summary.txt"

	echo >> "$A_SUMMARY_PATH"
	echo "## ${TEST_NAME}" >> "$A_SUMMARY_PATH"
	echo >> "$A_SUMMARY_PATH"
	cat "${SUMMARY_PATH}" >> "$A_SUMMARY_PATH"

	if [[ -n "${TEST_DATA[$TEST_NAME,DIFFICULT_CASES]}" ]]; then
		DIFFICULT_SUMMARY_PATH="${RULE_RESULTS_DIR}/${LOWER_NAME}-difficult--summary.txt"
			echo >> "$A_SUMMARY_PATH"
			echo "## ${TEST_NAME}_DIFFICULT" >> "$A_SUMMARY_PATH"
			echo >> "$A_SUMMARY_PATH"
			cat "${DIFFICULT_SUMMARY_PATH}" >> "$A_SUMMARY_PATH"
	fi
done
echo >> "$A_SUMMARY_PATH"

echo "INFO: All done."
