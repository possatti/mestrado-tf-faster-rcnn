#!/usr/bin/env bash

# source /home/lucaspossatti/venvs/tf/bin/activate
source /dados/virtualenvs/tf/bin/activate

# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s0
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s1
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s2
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s3
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s4
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s5
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s6
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s7
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s8
# bash possatti/training.sh 0 res101 google_aug0_nrg 70000 golf_s9

# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s0
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s1
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s2
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s3
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s4
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s5
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s6
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s7
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s8
# bash possatti/training.sh 0 res101 google_aug0_nrgy 70000 golf_s9

# bash possatti/training.sh 0 res101 google_aug0_nrgy 100000 juliett

# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s0
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s1
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s2
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s3
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s4
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s5
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s6
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s7
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s8
# bash possatti/training.sh 0 res101 google_aug0_nrgr 70000 golf_s9

bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s0
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s1
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s2
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s3
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s4
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s5
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s6
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s7
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s8
bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s9

# for i in $(seq 0 9); do
# 	bash possatti/run_xtop.sh --rule xtop 0 AUG0_AND_TL640_NRGR "output/res101/google_aug0_nrgr_train/golf_s${i}/res101_faster_rcnn_iter_70000.ckpt"
# 	bash possatti/run_xtop.sh --rule fat_tl 0 AUG0_AND_TL640_NRGR "output/res101/google_aug0_nrgr_train/golf_s${i}/res101_faster_rcnn_iter_70000.ckpt"
# 	bash possatti/run_xtop.sh --rule fat_twins 0 AUG0_AND_TL640_NRGR "output/res101/google_aug0_nrgr_train/golf_s${i}/res101_faster_rcnn_iter_70000.ckpt"
# done

test_all_xtop_rules() {
	RULES="xtop fat_tl fat_twins random_select"
	for rule in $RULES; do
		bash possatti/run_xtop.sh --rule "$rule" "$1" "$2" "$3"
	done
}

#bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 golf_s0
#test_all_xtop_rules 0 Y2018_10_NRGR "output/res101/y2018_10_google_aug00_nrgr_train/golf_s0/res101_faster_rcnn_iter_70000.ckpt"
# for i in $(seq 1 9); do
# 	bash possatti/training.sh 0 res101 y2018_10_google_aug00_nrgr 70000 "golf_s${i}"
# done
#for i in $(seq 1 5); do
	#test_all_xtop_rules 0 Y2018_10_NRGR "output/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/res101_faster_rcnn_iter_70000.ckpt"
#done

#bash possatti/run_xtop.sh --rule fat_tl 0 Y2018_10_NRGR "output/res101/y2018_10_google_aug00_nrgr_train/golf_s1/res101_faster_rcnn_iter_70000.ckpt"

# for i in $(seq 0 9); do
# 	bash possatti/run_xtop.sh --rule random_select 0 Y2018_10_NRGR \
# 		"output/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/res101_faster_rcnn_iter_70000.ckpt"
# done

# for i in $(seq 0 9); do
# 	# bash possatti/run_xtop.sh --rule random_select 0 Y2018_10_NRGR \
# 	# 	"output/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/res101_faster_rcnn_iter_70000.ckpt"
# 	test_all_xtop_rules 0 Y2018_10_NRGR "output/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/res101_faster_rcnn_iter_70000.ckpt"
# done

echo "Done stuff!"
