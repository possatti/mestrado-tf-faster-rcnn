#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('path', nargs='+', help='Use `-` for STDIN')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	for path in args.path:
		print(path)
		if path == '-':
			path = sys.stdin
		df = pd.read_csv(path, sep=' ', names=['path', 'label'])
		counting = np.unique(df['label'].values, return_counts=True)
		counting = list(zip(*counting))
		print('Total images:', len(df['label']))
		print('Samples per class:')
		for label, count in counting:
			print(' - {}: {} images'.format(label, count))
		print()

if __name__ == '__main__':
	main()
