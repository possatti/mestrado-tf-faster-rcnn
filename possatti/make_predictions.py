#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import _init_paths
from model.config import cfg, cfg_from_list, cfg_from_file

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import time
import cv2
import sys
import os
import re

from nets.vgg16 import vgg16
from nets.resnet_v1 import resnetv1

import testing

# Use `raw_input` as `input` for Python 2.
if re.search(r'^2\.', sys.version):
    input = raw_input

CLASSES = {
    'NRG': ['Red', 'Green'],
    'NRGY': ['Red', 'Green', 'Yellow'],
}

NETS = ['res101']

# def predict(sess, net, image, conf_thresh=0.8, nms_thresh=0.3, plt_debug=False):
#     if type(image) is str:
#         if not os.path.isfile(image):
#             raise ValueError('Couldn\'t load image from `{}`. File doesn\'t exist.')
#         im = cv2.imread(image)
#     elif type(image) is np.ndarray:
#         im = image
#     else:
#         raise TypeError('Expected a numpy ndarray, or an image path. Got `{}`.'.format(type(image)))

#     dets_d = testing.run_test(sess, net, im, N_CLASSES, conf_thresh=conf_thresh, nms_thresh=nms_thresh)

#     for cls_ind, class_name in enumerate(CLASSES):
#         if cls_ind != 0:
#             # centers: [(x,y,class),...]
#             dets = dets_d[cls_ind]
#             centers = np.empty(shape=(len(dets), 3), dtype=dets.dtype)
#             centers[:,0] = (dets[:,0] + dets[:,2])/2
#             centers[:,1] = (dets[:,1] + dets[:,3])/2
#             centers[:,2] = cls_ind
#             all_centers.append(centers)

def print_predictions(im_path, dets_d, labels):
    if len(dets_d) == 0 or sum([ len(dets_d[k]) for k in dets_d ]) == 0:
        print('{}: No object'.format(im_path))
    else:
        for class_num, class_name in zip(range(1, len(labels)+1), labels):
            for det in dets_d[class_num]:
                print('{}: {}-{} ({}) {} {} {} {}'.format(im_path, class_num, class_name, det[-1], *det[:-1]))

def main(args):
    class_names = CLASSES[args.labels]
    n_classes = len(class_names) + 1 # Count for BG
    sess, net = testing.load_network(args.ckpt, n_classes, args.net, tag=args.tag)

    # Load image list
    if args.image_list_txt == '-':
        image_paths = sys.stdin.read().split('\n')
    elif args.image_list_txt.endswith('.txt'):
        df = pd.read_csv(args.image_list_txt, sep=' ')
        image_paths = df.iloc[:,0]
    else:
        with open(args.image_list_txt, 'r') as f:
            image_paths = f.read().split('\n')
    if args.base_dir is not None:
        image_paths = [ os.path.join(args.base_dir, p) for p in image_paths ]

    for i, im_path in enumerate(image_paths):
        if re.search(r'^\s*$', im_path) or os.path.realpath(im_path)==os.path.realpath(args.base_dir):
            continue
        if not os.path.isfile(im_path):
            raise ValueError('Didn\'t find image `{}`'.format(im_path))
        im = cv2.imread(im_path)
        dets_d, inference_duration = testing.run_test(sess, net, im, n_classes, return_inference_duration=True)
        print_predictions(im_path, dets_d, class_names)
        if args.inference_duration:
            print('{}: Predicted in {} seconds.'.format(im_path, inference_duration))
        if args.progress:
            print('INFO: {} of {}.\r'.format(i+1, len(image_paths)), end='', file=sys.stderr)
    print(file=sys.stderr)

def parse_args():
    parser = argparse.ArgumentParser(description='Make predictions for Tensorflow Faster R-CNN')

    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--net', help='Network to use', choices=NETS, default='res101')
    parser.add_argument('--nms-thresh', type=float, default=0.3)
    parser.add_argument('--conf-thresh', type=float, default=0.8)
    parser.add_argument('--tag', default='default')
    parser.add_argument('--set', help='Space separated list o configuration keys and configuration values.')
    parser.add_argument('--cfg', dest='cfgs', action='append', help='''
        YAML file containing configuration. This option can be used multiple times.
        The configuration files will be applied in the order they were received
        from the command line.
        ''')
    parser.add_argument('--labels', type=lambda x: x.upper(), choices=CLASSES, default='NRG', help='Which labels should be used.')
    parser.add_argument('--base-dir', help='Base directory for the images.')
    parser.add_argument('--output-type', choices=['natural'], default='natural', help='How the predictions should be printed.')
    parser.add_argument('--progress', action='store_true', help='Print to STDERR how many images have been processed and the total.')
    parser.add_argument('--inference-duration', action='store_true', help='Print to STDERR the inference duration for each image.')
    parser.add_argument('ckpt', help='Path to the desired checkpoint (extension `.ckpt`, even though it doesn\'t exist).')
    parser.add_argument('image_list_txt', help='Text file with one image per line. Use `-` for stdin.')

    args = parser.parse_args()

    return args

if __name__ == '__main__':
    args = parse_args()

    if args.cfgs is not None:
        for cfg_file in args.cfgs:
            cfg_from_file(cfg_file)
            print('INFO: Loaded config `{}`.'.format(cfg_file), file=sys.stderr)
    if args.set is not None:
        cfg_from_list(args.set.split(' '))

    main(args)
