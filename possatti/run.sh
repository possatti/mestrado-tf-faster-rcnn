#!/usr/bin/env bash

## Set some bash options.
##  - http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit  # make script exit when a command fails.
# set -o nounset  # exit when script tries to use undeclared variables.
# set -o xtrace   # trace what gets executed.

PYTHON=python3

# # Make non trivial for FRCNN.
# NON_TRIVIAL_LISA_DAY_TEST_LIST="${HOME}/projects/traffic-lights/datasets/lisa_tl/lisa_day_test_non_trivial.list"
# NON_TRIVIAL_LISA_DAY_TRAIN_LIST="${HOME}/projects/traffic-lights/datasets/lisa_tl/lisa_day_train_non_trivial.list"
# for i in $(seq 0 9); do
# 	for rule in fat_tl fat_twins random_select xtop; do
# 		echo "i: ${i}; rule: ${rule}"
# 		multpat.py \
# 			"${NON_TRIVIAL_LISA_DAY_TEST_LIST}" \
# 			-f "${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}/y2018-10-lisa-day-test-nrgr--predictions.csv" \
# 			> "${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}/y2018-10-lisa-day-test-nrgr--predictions--non-trivial.csv"
# 		multpat.py \
# 			"${NON_TRIVIAL_LISA_DAY_TRAIN_LIST}" \
# 			-f "${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}/y2018-10-lisa-day-train-nrgr--predictions.csv" \
# 			> "${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}/y2018-10-lisa-day-train-nrgr--predictions--non-trivial.csv"
# 	done
# done

# for i in $(seq 0 9); do
# 	for rule in fat_tl fat_twins random_select xtop; do
# 		python possatti/metrics.py "${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}/y2018-10-lisa-day-test-nrgr--predictions--non-trivial.csv" \
# 			> "${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}/y2018-10-lisa-day-test-nrgr--summary--non-trivial.txt"
# 		python possatti/metrics.py "${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}/y2018-10-lisa-day-train-nrgr--predictions--non-trivial.csv" \
# 			> "${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}/y2018-10-lisa-day-train-nrgr--summary--non-trivial.txt"
# 	done
# done

## Make non trivial for YOLOv3.
# NON_TRIVIAL_LISA_DAY_TEST_LIST="${HOME}/projects/traffic-lights/datasets/lisa_tl/lisa_day_test_non_trivial.list"
# NON_TRIVIAL_LISA_DAY_TRAIN_LIST="${HOME}/projects/traffic-lights/datasets/lisa_tl/lisa_day_train_non_trivial.list"
# LISA_DAY_TEST_GT=/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_day_test.txt
# LISA_DAY_TRAIN_GT=/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_day_train.txt
# yolo_lisa_non_trivial() {
# 	_i="$1"
# 	_rule="$2"
# 	_set="$3"
# 	DETECTIONS_FILE="${HOME}/projects/traffic-lights/darknet/results_yolo/y2018_10_google_aug00_nrgr_train_s${_i}/yolov3-nrgr-5000-test/yolov3-nrgr-5000_final/y2018-10-lisa-day-${_set}-nrgr--detections.txt"
# 	PREDICTIONS_FILE="${HOME}/projects/traffic-lights/darknet/results_yolo/y2018_10_google_aug00_nrgr_train_s${_i}/yolov3-nrgr-5000-test/yolov3-nrgr-5000_final/${_rule}/y2018-10-lisa-day-${_set}-nrgr--predictions.csv"
# 	# SUMMARY_FILE="${HOME}/projects/traffic-lights/darknet/results_yolo/y2018_10_google_aug00_nrgr_train_s${_i}/yolov3-nrgr-5000-test/yolov3-nrgr-5000_final/${_rule}/y2018-10-lisa-day-${_set}-nrgr--summary.txt"
# 	PREDICTIONS_FILE_NON_TRIVIAL="${HOME}/projects/traffic-lights/darknet/results_yolo/y2018_10_google_aug00_nrgr_train_s${_i}/yolov3-nrgr-5000-test/yolov3-nrgr-5000_final/${_rule}/y2018-10-lisa-day-${_set}-nrgr--predictions--non-trivial.csv"
# 	SUMMARY_FILE_NON_TRIVIAL="${HOME}/projects/traffic-lights/darknet/results_yolo/y2018_10_google_aug00_nrgr_train_s${_i}/yolov3-nrgr-5000-test/yolov3-nrgr-5000_final/${_rule}/y2018-10-lisa-day-${_set}-nrgr--summary--non-trivial.txt"
# 	if [[ "$_set" == "train" ]]; then
# 		NON_TRIVIAL_LIST="$NON_TRIVIAL_LISA_DAY_TRAIN_LIST"
# 		GT_FILE="$LISA_DAY_TRAIN_GT"
# 	elif [[ "$_set" == "test" ]]; then
# 		NON_TRIVIAL_LIST="$NON_TRIVIAL_LISA_DAY_TEST_LIST"
# 		GT_FILE="$LISA_DAY_TEST_GT"
# 	else
# 		echo "Invalid set: ${_set}" 1>&2
# 		exit 1
# 	fi
# 	echo "INFO: Making '${PREDICTIONS_FILE}'." 1>&2
# 	if [[ ! -s "$PREDICTIONS_FILE" ]]; then
# 		python possatti/flextop.py \
# 			"$GT_FILE" \
# 			"$DETECTIONS_FILE" \
# 			--base-dir /dados/datasets/traffic_lights/ \
# 			--bg0 \
# 			--save-img-predictions "$PREDICTIONS_FILE"
# 	fi
# 	echo "INFO: Making '${PREDICTIONS_FILE_NON_TRIVIAL}'." 1>&2
# 	if [[ ! -s "$PREDICTIONS_FILE_NON_TRIVIAL" ]]; then
# 		python possatti/multpat.py \
# 			"${NON_TRIVIAL_LIST}" \
# 			-f "$PREDICTIONS_FILE" \
# 			> "$PREDICTIONS_FILE_NON_TRIVIAL"
# 	fi
# 	echo "INFO: Making '${SUMMARY_FILE_NON_TRIVIAL}'." 1>&2
# 	if [[ ! -s "$SUMMARY_FILE_NON_TRIVIAL" ]]; then
# 	python possatti/metrics.py \
# 		"$PREDICTIONS_FILE_NON_TRIVIAL" \
# 		> "$SUMMARY_FILE_NON_TRIVIAL"
# 	fi
# }
# for i in $(seq 0 9); do
# 	for rule in fat_tl fat_twins random_select xtop; do
# 		for set in test train; do
# 			echo "i: ${i}; rule: ${rule}; set: ${set}"
# 			yolo_lisa_non_trivial "$i" "$rule" "$set"
# 		done
# 	done
# done

## Showcase
# python possatti/make_predictions.py output/res101/y2018_10_google_aug00_nrgr_train/golf_s0/res101_faster_rcnn_iter_70000.ckpt --base-dir /dados/datasets/tl640 /dados/horimoto/traffic_lights/2018_10/txt_files/showcase.list --labels NRG --net res101 --cfg experiments/cfgs/res101.yml --cfg experiments/cfgs/res101/y2018_10_google_aug00_nrgr_train/golf_s0.yml --inference-duration > >(tee frcnn-showcase-detections.txt)

## Make variants from the results files.
# declare -A FRCNN_RESULTS_DS_STUB
# FRCNN_RESULTS_DS_STUB[IARA]='y2018-10-iara-nrgr'
# FRCNN_RESULTS_DS_STUB[LARA]='y2018-10-lara-nrgr'
# FRCNN_RESULTS_DS_STUB[LISA_DAY_TEST]='y2018-10-lisa-day-test-nrgr'
# FRCNN_RESULTS_DS_STUB[LISA_DAY_TRAIN]='y2018-10-lisa-day-train-nrgr'
# FRCNN_RESULTS_DS_STUB[VITORIA]='y2018-10-vitoria'
# declare -A VARIANT_STUB
# VARIANT_STUB[IARA]='IARA'
# VARIANT_STUB[LARA]='LARA'
# VARIANT_STUB[LISA_DAY_TEST]='LISA_day_test'
# VARIANT_STUB[LISA_DAY_TRAIN]='LISA_day_train'
# VARIANT_STUB[VITORIA]='vitoria(2018_09)'
# make_variant() {
# 	echo "DEBUG: i=$i" 1>&2
# 	echo "       rule=$rule" 1>&2
# 	echo "       ds=$ds" 1>&2
# 	echo "       variant=$variant" 1>&2
# 	VARIANT_TRAIN_CASE=$(echo "$variant" | perl -pe 's/_/-/g')
# 	VARIANT_LIST_PATH="$HOME/projects/traffic-lights/datasets/y2018_10_variations/${VARIANT_STUB[$ds]}_${variant}.list"
# 	RESULTS_DIR="${HOME}/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s${i}/${rule}"
# 	ORIGINAL_DS_PREDICTIONS_CSV="${RESULTS_DIR}/${FRCNN_RESULTS_DS_STUB[$ds]}--predictions.csv"
# 	VARIANT_PREDICTIONS_CSV="${RESULTS_DIR}/${FRCNN_RESULTS_DS_STUB[$ds]}-${VARIANT_TRAIN_CASE}--predictions.csv"
# 	VARIANT_SUMMARY_TXT="${RESULTS_DIR}/${FRCNN_RESULTS_DS_STUB[$ds]}-${VARIANT_TRAIN_CASE}--summary.txt"
# 	echo "  VARIANT_TRAIN_CASE:          $VARIANT_TRAIN_CASE" 1>&2
# 	echo "  VARIANT_LIST_PATH:           $VARIANT_LIST_PATH" 1>&2
# 	echo "  RESULTS_DIR:                 $RESULTS_DIR" 1>&2
# 	echo "  ORIGINAL_DS_PREDICTIONS_CSV: $ORIGINAL_DS_PREDICTIONS_CSV" 1>&2
# 	echo "  VARIANT_PREDICTIONS_CSV:     $VARIANT_PREDICTIONS_CSV" 1>&2
# 	echo "  VARIANT_SUMMARY_TXT:         $VARIANT_SUMMARY_TXT" 1>&2
# 	if [[ ! -s "$VARIANT_PREDICTIONS_CSV" ]]; then
# 		echo "DEBUG: multipat.py" 1>&2
# 		multpat.py \
# 			"${VARIANT_LIST_PATH}" \
# 			-f "${ORIGINAL_DS_PREDICTIONS_CSV}" \
# 			> "${VARIANT_PREDICTIONS_CSV}"
# 	fi
# 	if [[ ! -s "$VARIANT_SUMMARY_TXT" ]]; then
# 		echo "DEBUG: metrics.py" 1>&2
# 		$PYTHON possatti/metrics.py "${VARIANT_PREDICTIONS_CSV}" \
# 			> "${VARIANT_SUMMARY_TXT}"
# 	fi
# }

# for i in $(seq 0 9); do
# 	for rule in fat_tl fat_twins random_select xtop; do
# 		for ds in IARA LARA LISA_DAY_TEST LISA_DAY_TRAIN VITORIA; do
# 			for variant in 'only_none' 'without_none'; do
# 				make_variant
# 			done
# 		done
# 	done
# done

NET_CFG="/home/lucaspossatti/projects/tf-faster-rcnn/experiments/cfgs/res101.yml"
CFG="/home/lucaspossatti/projects/tf-faster-rcnn/experiments/cfgs/res101/y2018_10_google_aug00_nrgr_train/golf_s0.yml"
CHECKPOINT="/mnt/dados-lcad55/tf-faster-rcnn/output/res101/y2018_10_google_aug00_nrgr_train/golf_s0/res101_faster_rcnn_iter_70000.ckpt"
GT_FILE="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_day_train.txt"
# GT_FILE="./LISA_day_train_negatives.txt"
BASE_DIR="/dados/datasets/traffic_lights"
DEST_TXT="results_video/LISA_DAY_TRAIN.txt"
possatti/make_predictions.py \
	--net res101 --cfg "$NET_CFG" --cfg "$CFG" --labels NRG \
	--base-dir "$BASE_DIR" --progress \
	"$CHECKPOINT" <(cut -f1 -d' ' "$GT_FILE") \
	> "$DEST_TXT"
	# | tee "$DEST_TXT"
