#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from sklearn.metrics import confusion_matrix

import pandas as pd
import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)


def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('gt_col', type=int)
	parser.add_argument('pred_col', type=int)
	parser.add_argument('csvs', nargs='+')
	parser.add_argument('--sep', default=',')
	parser.add_argument('--nlabels', type=int)
	parser.add_argument('--label-names', help='Comma separated names.')
	parser.add_argument('-E', '--header',    dest=header, action='store_true')
	parser.add_argument('-e', '--no-header', dest=header, action='store_false')
	parser.set_defaults(header=False)
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	header_option = None
	if args.header:
		header_option = 0

	dfs = [ pd.read_csv(p, header=header_option, sep=args.sep) for p in csvs ]
	df = pd.concat(dfs, ignore_index=True)

	gts   = df[df.columns[args.gt_col]]
	preds = df[df.columns[args.pred_col]]

	label_nums = None
	label_names = None
	if args.labels is not None:
		label_nums = range(args.nlabels)
	if args.label_names is not None:
		label_names = args.label_names.split()
		label_nums = range(len(label_names))

	confmat = confusion_matrix(gts, preds, labels=label_nums)
    acc_micro = confmat.diagonal().sum() / confmat.sum()
    acc_per_class = confmat.diagonal() / confmat.sum(axis=1)
    acc_per_class_no_nan = acc_per_class[~np.isnan(acc_per_class)]
    acc_macro = acc_per_class_no_nan.mean()

    if label_nums is None:
    	label_nums = range(len(confmat))

    print('Total samples: {}'.format(len(df)))
    print('Confusion matrix:')
    print(confmat)
    print('Accuracy per class:')
    for label in label_nums:
    	name = label if label_names is None else label_names[label]
        print(' - {}: {}'.format(name, acc_per_class[label]))
    print('Micro accuracy: {}'.format(acc_micro))
    print('Macro accuracy: {}'.format(acc_macro))

if __name__ == '__main__':
	main()
