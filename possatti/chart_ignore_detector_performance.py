#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

import flextop

print(sys.version, file=sys.stderr)


DATASET_NAMES = [
    'IARA',
    'LARA',
    'LISA-day-train',
    'LISA-day-test',
]
DATASET_STUBS = {
    'IARA': 'iara',
    'LARA': 'lara',
    'LISA-day-train': 'lisa-day-train',
    'LISA-day-test': 'lisa-day-test',
}

# DATASET_NAMES =

RULES = [
    'xtop',
    'fat_tl',
    'fat_twins',
    'random_select',
]
HEURISTIC_NAME = {
    'xtop': '1H',
    'fat_tl': '1L',
    'fat_twins': '1H-2L',
    'random_select': 'R',
}

def parse_args():
    parser = argparse.ArgumentParser(description='')
    # parser.add_argument('arg')
    # parser.add_argument('-o', '--opt')
    args = parser.parse_args()
    return args

def assign(obj, **kwargs):
    for key in kwargs:
        obj[key] = kwargs[key]

def main():
    args = parse_args()

    frcnn_result_template = '/home/lucaspossatti/projects/tf-faster-rcnn/results/res101/y2018_10_google_aug00_nrgr_train/golf_s{i}/{rule}/y2018-10-{ds_stub}-nrgr--predictions.csv'

    print('INFO: Collecting files...', file=sys.stderr)
    accs = []
    for d, ds_name in enumerate(DATASET_NAMES):
        ds_stub = DATASET_STUBS[ds_name]
        for r, rule in enumerate(RULES):
            for i in range(10):
                # print("ds_name:\n{}".format(ds_name), file=sys.stderr) #!#
                # print("rule:\n{}".format(rule), file=sys.stderr) #!#
                # print("i:\n{}".format(i), file=sys.stderr) #!#
                predictions_csv_path = frcnn_result_template.format(i=i, rule=rule, ds_stub=ds_stub)
                predictions_df = pd.read_csv(predictions_csv_path)
                predictions_df.assign(ds_name=ds_name, rule=rule, i=i)

                # sub_mask = (predictions_df['label']!=0) & (predictions_df['prediction']!=0)
                sub_mask = (predictions_df['label']!=0) & (predictions_df['n_detections']>=3)
                sub = predictions_df.loc[sub_mask]


                # label_count = np.unique(sub['label'], return_counts=True)
                # prediction_count = np.unique(sub['prediction'], return_counts=True)
                # print("label_count:\n{}".format(label_count), file=sys.stderr) #!#
                # print("prediction_count:\n{}".format(prediction_count), file=sys.stderr) #!#

                d_metrics = flextop.calc_metrics(sub['label'].values, sub['prediction'].values)
                # print("d_metrics:\n{}".format(d_metrics), file=sys.stderr) #!#

                del d_metrics['confmat'], d_metrics['acc_per_class']
                assign(d_metrics, ds_name=ds_name, rule=rule, i=i)

                accs.append(d_metrics)
            print('INFO: {} of {}.\r'.format(i+r*10+d*len(RULES)*10 + 1, 10*len(RULES)*len(DATASET_NAMES)), end='', file=sys.stderr)
    print(file=sys.stderr)
    accs = pd.DataFrame(accs)
    print("accs:\n{}".format(accs), file=sys.stderr) #!#

    sns.set(
        context='paper', style='darkgrid', font_scale=1.1,
    )
    sns.set_palette('muted')

    sns.catplot(
        x='rule', y='acc_macro', data=accs,
        kind='box',
        col='ds_name',
    )
    plt.show()



if __name__ == '__main__':
    main()
