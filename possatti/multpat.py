#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from multiprocessing import Pool
from itertools import cycle, repeat

import argparse
import sys
import re
import os

if __name__ == '__main__':
	# I tryed to do things pretty using functions and not using globals, but it gets much slower.

	# Arguments and options.
	parser = argparse.ArgumentParser(description='Select lines from `--file-in` or `STDIN` that contain any line present in `patterns`.')
	parser.add_argument('patterns')
	parser.add_argument('-f', '--file-in')
	args = parser.parse_args()

	with open(args.patterns, 'r') as f:
		patterns = [ x.strip() for x in f.readlines() if x.strip() ]

	if args.file_in is None:
		input_file = sys.stdin
	else:
		input_file = open(args.file_in, 'r')
	input_lines = [ x.strip() for x in input_file.readlines() if x.strip() ]

	def check_input_line(line):
		# global patterns
		for pat in patterns:
			if pat in line:
				return line

	n_jobs = 16
	chunk_size = int(len(input_lines) / (n_jobs*2))

	pool = Pool(n_jobs)
	filtered_lines = list(pool.imap(check_input_line, input_lines, chunk_size))
	pool.terminate()

	for line in filtered_lines:
		if line:
			print(line)
