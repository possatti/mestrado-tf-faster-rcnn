#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

from flextop import LABELS, calculate_metrics

def parse_args():
    parser = argparse.ArgumentParser(description='')

    parser.add_argument('predictions_csv')
    parser.add_argument('--labels', type=lambda x: x.upper(), choices=LABELS, default='NRG')

    args = parser.parse_args()
    return args

def main():
    args = parse_args()

    df = pd.read_csv(args.predictions_csv, names='path label prediction n_detections'.split())
    calculate_metrics(df['label'], df['prediction'], ['None'] + LABELS[args.labels])

if __name__ == '__main__':
    main()
