NAMES=(IARA LARA LYSA_DAY_TEST LYSA_DAY_TRAIN VITORIA)

TXT_PATHS=(
    "/home/lucaspossatti/projects/tf-faster-rcnn/data/annotations/y2018_10/txt_files/val_test/IARA.txt"
    "/home/lucaspossatti/projects/tf-faster-rcnn/data/annotations/y2018_10/txt_files/val_test/LARA.txt"
    "/home/lucaspossatti/projects/tf-faster-rcnn/data/annotations/y2018_10/txt_files/val_test/LISA_day_test.txt"
    "/home/lucaspossatti/projects/tf-faster-rcnn/data/annotations/y2018_10/txt_files/val_test/LISA_day_train.txt"
    "/home/lucaspossatti/projects/tf-faster-rcnn/data/annotations/y2018_10/txt_files/val_test/vitoria(2018_09).txt"
)

if [[ "${#NAMES}" -eq "${#TXT_PATHS}" ]]; then
    echo "Lengths must be equal."
    exit 1
fi

for S in $(seq 0 9); do
    for N in $(seq 0 ${#NAMES}); do
        NAME="${NAMES[N]}"
        TXT_PATH="${TXT_PATHS[N]}"
        CKPT_PATH="output/res101/y2018_10_google_aug00_nrgr_train/golf_s${S}/res101_faster_rcnn_iter_70000.ckpt"
        CFG_PATH="experiments/cfgs/res101/y2018_10_google_aug00_nrgr_train/golf_s${S}.yml"
        DEST_DETECTIONS_PATH="results/res101/pro_rafael/golf_s0/${NAME}.txt"

        if [[ ! -s "$TXT_PATH" ]]; then
            echo "File does not exist: '$TXT_PATH'."
            exit 1
        fi

        echo ""
        echo "N: $N"
        echo "NAME: $NAME"
        echo "TXT_PATH: $TXT_PATH"

        python possatti/make_predictions.py \
            "$CKPT_PATH" \
            "$TXT_PATH" \
            --base-dir /dados/datasets/tl640 \
            --labels NRG --net res101 --progress \
            --cfg experiments/cfgs/res101.yml \
            --cfg "$CFG_PATH" \
            > "$DEST_DETECTIONS_PATH"
            # > >(tee "DEST_DETECTIONS_PATH")
    done
done
