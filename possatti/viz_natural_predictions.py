#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import matplotlib.pyplot as plt
import numpy as np
import argparse
import random
import sys
import os
import re

import flextop

print(sys.version, file=sys.stderr)

def draw(ax, im_path, img_detections, rule=None):
	ax.clear()
	print('Predictions for image {}:'.format(im_path))
	print(img_detections)
	im = plt.imread(im_path)
	ax.imshow(im)
	ax.set_axis_off()
	if rule:
		flextop.flex_badges(ax, img_detections, im.shape[1], im.shape[0], rule)
	else:
		flextop.draw_boxes(ax, img_detections, draw_text=False, lw=4)
	print()
	plt.draw()

def mult_in(l, v):
	"""Checks any string in `l` is contained in `v`."""
	for x in l:
		if x in v:
			return True
	return False

i = 0
def main():
	args = parse_args()

	random.seed(args.seed)

	predictions = flextop.parse_natural_predictions(args.txt, class_shift=args.shift)

	if args.skip_no_object:
		predictions = { k: predictions[k] for k in predictions if predictions[k] }

	if args.save_imgs:
		if not os.path.isdir(args.save_imgs):
			os.makedirs(args.save_imgs)

		fig = plt.figure(facecolor='pink')
		ax = plt.axes()
		ax.set_position([0, 0, 1, 1]) # <<< This is the only thing that makes the axes occupy the entire figure.
		for k in predictions:
			im_name = os.path.basename(k)
			dest_im = os.path.join(args.save_imgs, im_name)
			if len(args.only) > 0 and not mult_in(args.only, dest_im):
				print('Skipping...', file=sys.stderr)
				continue
			print('Saving image to:', dest_im, file=sys.stderr)
			draw(ax, k, predictions[k], rule=args.rule)
			plt.savefig(dest_im, bbox_inches=None, facecolor=fig.get_facecolor())
	else:
		shuffled_img_paths = list(predictions.keys())
		if len(args.only) > 0:
			shuffled_img_paths = [ p for p in shuffled_img_paths if mult_in(args.only, p) ]
		random.shuffle(shuffled_img_paths)
		# shuffled_img_paths.sort()

		fig, ax = plt.subplots(1, 1)
		img_iter = iter(shuffled_img_paths)
		def onpress(event):
			# print("event.key:", event.key, file=sys.stderr) #!#
			# next_im_path = next(img_iter)
			# draw(ax, next_im_path, predictions[next_im_path])
			global i
			if event.key == 'left':
				i = max(0, i-1)
			elif event.key == 'right':
				i = min(len(shuffled_img_paths)-1, i+1)
			new_im_path = shuffled_img_paths[i]
			draw(ax, new_im_path, predictions[new_im_path], rule=args.rule)

		draw(ax, shuffled_img_paths[0], predictions[shuffled_img_paths[0]], rule=args.rule)
		fig.canvas.mpl_connect('key_press_event', onpress)
		plt.show()

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('txt')
	parser.add_argument('--shift', default=0, type=int)
	parser.add_argument('--seed', type=int, default=7)
	parser.add_argument('--rule', choices=flextop.rules, action='append')
	parser.add_argument('-s', '--skip-no-object', action='store_true')
	parser.add_argument('-l', '--only', action='append')
	parser.add_argument('-a', '--all-badges', action='store_true')
	parser.add_argument('--save-imgs')
	args = parser.parse_args()
	if args.all_badges:
		args.rule = ['xtop', 'fat_tl', 'fat_twins']
	return args

if __name__ == '__main__':
	main()
