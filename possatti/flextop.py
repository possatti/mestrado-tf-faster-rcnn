#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module contains the rules for choosing from multiple traffic light bouding
boxes the on that should be obeyed.
"""

from __future__ import print_function, division
from sklearn.metrics import confusion_matrix
from collections.abc import Iterable
from multiprocessing import Pool
from PIL import ImageFont

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import random
import time
import cv2
import sys
import re
import os

# RE_NATURAL_PREDICTION = re.compile(r'(.+): (\d+)-(\w+) \(([\d.]+)\) (\d+) (\d+) (\d+) (\d+)')
RE_NATURAL_PREDICTION = re.compile(r'(?P<im_path>.+): (?P<class_num>\d+)-(?P<class_name>\w+) \((?P<score>[\d.]+)\) (?P<x1>[\d.]+) (?P<y1>[\d.]+) (?P<x2>[\d.]+) (?P<y2>[\d.]+)')
RE_NATURAL_DURATION = re.compile(r'(?P<im_path>.+): Predicted in (?P<duration>[\d.]+) seconds\.')
RE_NATURAL_NO_OBJECT = re.compile(r'(?P<im_path>.+): No object')

LABELS = {
    'NRG': ['Red', 'Green'],
    'NRGY': ['Red', 'Green', 'Yellow'],
}

random.seed(7)

def draw_prediction_fill(ax, pred, color_fill=(1.0, 0.75, 0.79, 0.5)): # color_fill = pink
    width = pred['x2'] - pred['x1']
    height = pred['y2'] - pred['y1']
    rect = plt.Rectangle(
        (pred['x1'], pred['y1']),
        width, height, fill=True, color=color_fill, linewidth=0.0
    )
    ax.add_patch(rect)

def make_iterable(x, n):
    if type(x) is str or not isinstance(x, Iterable):
        return [x,] * n
    else: # iterable and not str
        if len(x) != n:
            raise ValueError('The iterable doesn\'t have the expected length.')
        return x

def draw_boxes_generic(ax, detections, label=None, color='red', color_fill=None,
    lw=1, ls='-',
    badges=None, badge_spacing=10,
    marker=None, marker_size=10, marker_color='yellow', markeredgecolor='black', marker_offset=(0, 0)):
    """Draw boxes on image. Each box may have an individual style.

    Arguments:
        ax: Pyplot's Axes object.
        detections: list of {'x1', 'x2', 'y1', 'y2'}.
        label: A single label, or a list of corresponding labels that will accompany each bbox.
        color: A single color, or a list of corresponding colors that will be used in the bboxes' border.
        color_fill: A single color, or a list of corresponding colors that will fill the bboxes.
        lw: (single or list) the line's width.
        ls: (single or list) the line's style.
        marker: (single or list) marker that will be put on the top left corner (https://matplotlib.org/api/markers_api.html). May be `None`.
    """
    label = make_iterable(label, len(detections))
    color = make_iterable(color, len(detections))
    color_fill = make_iterable(color_fill, len(detections))
    lw = make_iterable(lw, len(detections))
    ls = make_iterable(ls, len(detections))
    marker = make_iterable(marker, len(detections))
    marker_size = make_iterable(marker_size, len(detections))
    marker_color = make_iterable(marker_color, len(detections))
    markeredgecolor = make_iterable(markeredgecolor, len(detections))
    badges = make_iterable(badges, len(detections))
    badge_spacing = make_iterable(badge_spacing, len(detections))
    print("badges:\n{}".format(badges), file=sys.stderr) #!#

    keydef = lambda d, key, default: d[key] if key in badge else default

    import matplotlib
    from matplotlib.font_manager import findfont, FontProperties
    font_path = findfont(FontProperties(family=matplotlib.rcParams['font.family']))

    for i, pred in enumerate(detections):
        width = pred['x2'] - pred['x1']
        height = pred['y2'] - pred['y1']
        rect = plt.Rectangle(
            (pred['x1'], pred['y1']),
            width, height,
            facecolor=color_fill[i], fill=color_fill[i] is not None,
            edgecolor=color[i], linewidth=lw[i], linestyle=ls[i],
        )
        ax.add_patch(rect)
        if label[i] is not None:
            # ax.text(pred['x1'], pred['y1'] - 2,
            #         label[i],
            #         bbox=dict(facecolor=color[i], alpha=0.5),
            #         fontsize=14, color='white')
            ax.text(pred['x1'], pred['y1'] - 2,
                    label[i],
                    fontsize=12, color=color[i])
        if badges[i] is not None:
            for badge in badges[i]:
                font = ImageFont.truetype(font_path, keydef(badge, 'fontsize', 10))
                badge['size_x'] = font.getsize(badge['text'])[0]
            xoffset = - (sum([ x['size_x'] for x in badges[i] ]) + badge_spacing[i]*(len(badges[i])-1)) / 2 + width/2
            for b, badge in enumerate(badges[i]):
                text_coord = np.array((pred['x1'] + xoffset, pred['y1'] - 15))
                rec_top = np.array((pred['x1']+width/2, pred['y1']))
                arrow_delta = rec_top - text_coord
                ax.arrow(*text_coord, *arrow_delta)
                ax.text(*text_coord, badge['text'],
                    color=keydef(badge, 'text_color', 'white'),
                    fontdict=dict(size=keydef(badge, 'fontsize', 10)),
                    bbox=dict(boxstyle=keydef(badge, 'style', 'round'), fc=keydef(badge, 'bbox_fc', 'blue'), ec=keydef(badge, 'bbox_ec', 'black')))
                xoffset += badge['size_x'] + badge_spacing[i]
        if marker[i]:
            ax.plot(pred['x1']+marker_offset[0], pred['y1']+marker_offset[1], linestyle=None, color=marker_color[i], marker=marker[i], markersize=marker_size[i], markeredgecolor=markeredgecolor[i])

def draw_boxes(ax, detections, class_names=['BG', 'Red', 'Green', 'Yellow'], class_colors=['black', 'red', 'green', 'yellow'], lw=1, draw_text=True):
    lw_spacing = int(lw / 2)
    print("lw_spacing:", lw_spacing, file=sys.stderr) #!#
    for pred in detections:
        width = pred['x2'] - pred['x1']
        height = pred['y2'] - pred['y1']
        class_name = class_names[pred['class_num']]
        class_color = class_colors[pred['class_num']]
        rect = plt.Rectangle(
            (pred['x1']-lw_spacing, pred['y1']-lw_spacing),
            width+lw_spacing*2, height+lw_spacing*2, fill=False,
            edgecolor=class_color, linewidth=lw
        )
        ax.add_patch(rect)
        if draw_text:
            ax.text(pred['x1'], pred['y1'] - 2,
                    '{:s} {:.3f}'.format(class_name, pred['score']),
                    bbox=dict(facecolor=class_color, alpha=0.5),
                    fontsize=14, color='white')

def flex_and_draw(ax, detections, im_width, im_height, rule_func, class_names=['BG', 'Red', 'Green', 'Yellow'], class_colors=['black', 'red', 'green', 'yellow'], lw=1, draw_text=True):
    image_class_pred, d = rule_func(detections, im_width, im_height, return_selected=True)

    markers = np.zeros(len(detections), dtype=str)
    markers[d['selected_index']] = '*'
    colorfy = np.vectorize(lambda i: class_colors[i])
    namify = np.vectorize(lambda i: class_names[i])
    colors = colorfy([ x['class_num'] for x in detections ])
    labels = namify([ x['class_num'] for x in detections ])

    draw_boxes_generic(ax, detections, label=labels if draw_text else None, color=colors, lw=lw, marker=markers, marker_offset=(-2,-2))

def flex_badges(ax, detections, im_width, im_height, use_rules=['xtop', 'fat_tl', 'fat_twins'], fontsize=18, class_names=['BG', 'Red', 'Green', 'Yellow'], class_colors=['black', 'red', 'green', 'yellow'], lw=1):
    # Make sure its a list, even if there is a single value.
    if (not isinstance(use_rules, Iterable)) or (type(use_rules) is str):
        use_rules = [use_rules]

    rule_funcs = [ rules[rule] for rule in use_rules ]

    rule_badges = {
        'xtop':      {'text': '1H',    'bbox_fc': 'tab:blue',   'bbox_ec': (1,1,1,0), 'fontsize': fontsize},#, 'size_x': font.getsize('1H')[0]},
        'fat_tl':    {'text': '1L',    'bbox_fc': 'tab:orange', 'bbox_ec': (1,1,1,0), 'fontsize': fontsize},#, 'size_x': font.getsize('1L')[0]},
        'fat_twins': {'text': '1H-2L', 'bbox_fc': 'tab:purple', 'bbox_ec': (1,1,1,0), 'fontsize': fontsize},#, 'size_x': font.getsize('1H-2L')[0]},
    }

    badges =  [ [] for _ in range(len(detections)) ]
    for r, rule, rule_f in zip(range(len(use_rules)), use_rules, rule_funcs):
        image_class_pred, d = rule_f(detections, im_width, im_height, return_selected=True)
        badges[d['selected_index']].append(rule_badges[rule])

    # badges = [[xtop_badge, fat_tl_badge, fat_twins_badge]] * len(detections) #!#
    colorfy = np.vectorize(lambda i: class_colors[i])
    namify = np.vectorize(lambda i: class_names[i])
    colors = colorfy([ x['class_num'] for x in detections ])
    labels = namify([ x['class_num'] for x in detections ])

    draw_boxes_generic(ax, detections, color=colors, lw=lw,
        badges=badges, badge_spacing=14) # b_s=10

def xtop(image_predictions, im_width, im_height, plot_ax=None, return_selected=False):
    """Predict the traffic light closest to the top-center as the roadway TL.

    Args:
        image_predictions: Dictionary with 'x1', 'y1', 'x2', 'y2', and 'class_num'.
            'class_num' is a number from '1' to 'n'.
    """

    top_coord = np.array([im_width/2, 0])
    selected_ind = None
    if len(image_predictions) == 0:
        prediction = 0
    elif len(image_predictions) == 1:
        selected_ind = 0
        prediction = image_predictions[selected_ind]['class_num']
    else:
        centers = []
        for pred in image_predictions:
            width = pred['x2'] - pred['x1']
            height = pred['y2'] - pred['y1']
            center_x = pred['x1'] + width / 2
            center_y = pred['y1'] + height / 2
            centers.append([center_x, center_y])
        centers = np.array(centers)
        distances = np.linalg.norm(centers - top_coord, axis=1)
        selected_ind = np.argmin(distances)
        prediction = image_predictions[selected_ind]['class_num']

    if plot_ax is not None:
        plot_ax.plot(top_coord[0], top_coord[1], 'bo')
        draw_boxes(plot_ax, image_predictions)
        plot_ax.plot(image_predictions[selected_ind][0], image_predictions[selected_ind][1], 'bx')

    d = {}
    if return_selected:
        d['selected_index'] = selected_ind
        return prediction, d
    return prediction

def fat_tl(image_predictions, im_width, im_height, plot_ax=None, return_selected=False):
    """Predict the biggest TL as the roadway TL."""

    selected_ind = None
    if len(image_predictions) == 0:
        prediction = 0
    elif len(image_predictions) == 1:
        prediction = image_predictions[0]['class_num']
        selected_ind = 0
    else:
        areas = []
        for pred in image_predictions:
            width = pred['x2'] - pred['x1']
            height = pred['y2'] - pred['y1']
            area = width * height
            areas.append(area)
        areas = np.array(areas)
        selected_ind = np.argmax(areas)
        prediction = image_predictions[selected_ind]['class_num']

    if plot_ax is not None:
        draw_prediction_fill(plot_ax, image_predictions[selected_ind])
        draw_boxes(plot_ax, image_predictions)

    d = {}
    if return_selected:
        d['selected_index'] = selected_ind
        return prediction, d
    return prediction


def fat_twins(image_predictions, im_width, im_height, plot_ax=None, n_biggest=2, return_selected=False):
    """Select the `n_biggest` biggest TLs and choose the one closer to the top-center as the roadway TL."""

    top_coord = np.array([im_width/2, 0])
    biggest_tls = []
    selected_ind = None
    if len(image_predictions) == 0:
        prediction = 0
    elif len(image_predictions) == 1:
        prediction = image_predictions[0]['class_num']
        selected_ind = 0
    else:
        centers = []
        areas = []
        for pred in image_predictions:
            width = pred['x2'] - pred['x1']
            height = pred['y2'] - pred['y1']
            center_x = pred['x1'] + width / 2
            center_y = pred['y1'] + height / 2
            centers.append([center_x, center_y])
            area = width * height
            areas.append(area)
        centers = np.array(centers)
        areas = np.array(areas)

        biggest_tls = np.argsort(areas)[-n_biggest:]
        biggest_tls_centers = centers[biggest_tls]
        biggest_tls_distances = np.linalg.norm(biggest_tls_centers - top_coord, axis=1)
        selected_ind = biggest_tls[np.argmin(biggest_tls_distances)]
        prediction = image_predictions[selected_ind]['class_num']

    if plot_ax is not None:
        plot_ax.plot(top_coord[0], top_coord[1], 'bo')
        for ind in biggest_tls:
            draw_prediction_fill(plot_ax, image_predictions[ind])
        draw_boxes(plot_ax, image_predictions)
        chosen_center = centers[selected_ind]
        plot_ax.plot(chosen_center[0], chosen_center[1], 'bx')

    d = {}
    if return_selected:
        d['selected_index'] = selected_ind
        return prediction, d
    return prediction

def random_select(image_predictions, im_width, im_height, plot_ax=None, return_selected=False):
    """Select the state of a random TL to be the state of the roadway.

    Args:
        image_predictions: Dictionary with 'x1', 'y1', 'x2', 'y2', and 'class_num'.
            'class_num' is a number from '1' to 'n'.
    """

    selected_ind = None
    if len(image_predictions) == 0:
        prediction = 0
    elif len(image_predictions) == 1:
        selected_ind = 0
        prediction = image_predictions[selected_ind]['class_num']
    else:
        selected_ind = random.randrange(0, len(image_predictions))
        prediction = image_predictions[selected_ind]['class_num']

    if plot_ax is not None:
        draw_boxes(plot_ax, image_predictions)
        if len(image_predictions) >= 1:
            plot_ax.plot(image_predictions[selected_ind][0], image_predictions[selected_ind][1], 'bx')

    d = {}
    if return_selected:
        d['selected_index'] = selected_ind
        return prediction, d
    return prediction

def parse_natural_predictions(predictions_path, class_shift=0):
    """
    Arguments:
        class_shift: Add `class_shift` to the class number. Useful for compensating
            background not being the first class or whatever.
    Returns:
        A dictionary with image paths as keys and a list of bound boxes. Each bounding
        box is like {'class_num', 'score', 'x1', 'y1', 'x2', 'y2', 'im_path'}
    """
    with open(predictions_path, 'r') as f:
        lines = f.readlines()

    predictions = {}
    for i, line in enumerate(lines):
        prediction_match = RE_NATURAL_PREDICTION.match(line)
        if prediction_match:
            g = prediction_match.groupdict()
            for att in ['x1', 'y1', 'x2', 'y2']:
                g[att] = float(g[att])
            for att in ['score']:
                g[att] = float(g[att])
            g['class_num'] = int(g['class_num']) + class_shift
            if g['im_path'] in predictions:
                predictions[g['im_path']].append(g)
            else:
                predictions[g['im_path']] = [g]
        else:
            no_obj_match = RE_NATURAL_NO_OBJECT.match(line)
            if no_obj_match:
                g = no_obj_match.groupdict()
                predictions[g['im_path']] = []
            else:
                duration_match = RE_NATURAL_DURATION.match(line)
                if duration_match:
                    continue
                elif re.match(r'^\s*$', line):
                    continue
                else:
                    print('WARN: Couldn\'t match string `{}`.'.format(lines[i]), file=sys.stderr)

    return predictions

rules = {
    'xtop': xtop,
    'fat_tl': fat_tl,
    'fat_twins': fat_twins,
    'random_select': random_select,
}

def calc_metrics(gts, preds, class_nums=None):
    if class_nums is not None:
        if not isinstance(class_nums, Iterable):
            raise ValueError('class_nums needs to be an iterable. Got `{}`.'.format(class_nums))
        if type(class_nums[0]) != int:
            raise ValueError('class_nums items need to be integers.')
    assert len(gts) == len(preds)
    confmat = confusion_matrix(gts, preds, labels=class_nums)
    acc_micro = confmat.diagonal().sum() / confmat.sum()
    np.seterr(invalid="ignore")
    acc_per_class = confmat.diagonal() / confmat.sum(axis=1)
    np.seterr(divide="warn")
    acc_per_class_no_nan = acc_per_class[~np.isnan(acc_per_class)]
    acc_macro = acc_per_class_no_nan.mean()
    # print("confmat:\n{}".format(confmat), file=sys.stderr) #!#
    # print("acc_macro:", acc_macro, file=sys.stderr) #!#

    return {
        'acc_macro': acc_macro,
        'acc_micro': acc_micro,
        'confmat': confmat,
        'acc_per_class': acc_per_class,
    }

def calculate_metrics(gts, preds, class_names=None):
    class_nums = None if class_names is None else range(len(class_names))
    d = calc_metrics(gts, preds, class_nums)

    if class_names is None:
        class_names = range(len(confmat))

    print('Total images: {}'.format(len(gts)))
    print('Confusion matrix:')
    print(d['confmat'])
    print('Accuracy per class:')
    for label, label_name in enumerate(class_names):
        print(' - {}: {}'.format(label_name, d['acc_per_class'][label]))
    print('Micro accuracy: {}'.format(d['acc_micro']))
    print('Macro accuracy: {}'.format(d['acc_macro']))

def flextop(args):
    if re.match(r'.*\.csv', args.gt):
        df = pd.read_csv(args.gt, names=['path', 'label_num'])
    elif re.match(r'.*\.txt', args.gt):
        df = pd.read_csv(args.gt, sep=' ', names=['path', 'label_num'])
    else:
        raise ValueError('Unrecognized extension for classification file `{}`'.format(args.gt))

    box_predictions = parse_natural_predictions(args.predictions_path, class_shift=(1 if args.bg0 else 0))

    heuristic_durations = []
    image_n_detections = []
    image_class_predictions = []
    for i, image_path in enumerate(df['path']):
        abspath = os.path.join(args.base_dir, image_path) if args.base_dir else image_path
        if abspath in box_predictions:
            im = cv2.imread(abspath)
            im_height, im_width, im_channels = im.shape

            heuristic_start_time = time.time()
            image_class_pred = rules[args.rule](box_predictions[abspath], im_width, im_height)
            heuristic_end_time = time.time()
            heuristic_duration = heuristic_end_time - heuristic_start_time

            image_class_predictions.append(image_class_pred)
            image_n_detections.append(len(box_predictions[abspath]))
            heuristic_durations.append(heuristic_duration)
        else:
            raise Exception('`{}` not in predictions.'.format(abspath))
        print('INFO: {} of {}.\r'.format(i+1, len(df)), end='', file=sys.stderr)
    print(file=sys.stderr)

    heuristic_durations = np.array(heuristic_durations)

    if args.save_img_predictions:
        img_predictions_df = pd.DataFrame({
            'path': df['path'],
            'label': df['label_num'],
            'prediction': image_class_predictions,
            'n_detections': image_n_detections,
            'heuristic_duration': heuristic_durations,
        })
        col_names = ['path', 'label', 'prediction', 'n_detections']
        img_predictions_df.to_csv(args.save_img_predictions, columns=col_names, index=False)

    avg_heuristic_duration = heuristic_durations.mean()
    print('Average heuristic duration: {}'.format(avg_heuristic_duration), file=sys.stderr)

    calculate_metrics(df['label_num'], image_class_predictions, ['None'] + LABELS[args.labels])

def calc(args):
    df = pd.read_csv(args.predictions_csv, names='path label prediction n_detections'.split())
    calculate_metrics(df['label'], df['prediction'], ['None'] + LABELS[args.labels])

def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('gt', help='CSV file with (path_to_image, gt_label) ')
    parser.add_argument('predictions_path')
    parser.add_argument('--rule', choices=rules, default='xtop')
    parser.add_argument('--base-dir')
    parser.add_argument('--bg0', action='store_true', help='Use when background should be class 0 on the predictions.')
    parser.add_argument('--labels', type=lambda x: x.upper(), choices=LABELS, default='NRG')
    parser.add_argument('--save-img-predictions')

    # subparsers = parser.add_subparsers(help='Available commands.', dest='command')
    # subparsers.required = False

    # calc_parser = subparsers.add_parser('calc', help='')
    # calc_parser.add_argument('predictions_csv')
    # calc_parser.add_argument('--labels', type=lambda x: x.upper(), choices=LABELS, default='NRG')
    # calc_parser.set_defaults(func=calc)

    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    flextop(args)


if __name__ == '__main__':
    main()
