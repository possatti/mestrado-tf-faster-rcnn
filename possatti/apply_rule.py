#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# import pandas as pd
import numpy as np
import argparse
import sys
import os

import flextop

def main():
    args = parse_args()
    predictions = flextop.parse_natural_predictions(args.predictions_path)

    for im_path in predictions:
        image_predictions = predictions[im_path]
        final_prediction, extra = flextop.rules[args.rule](image_predictions, args.im_width, args.im_height, return_selected=True)
        selected_index = extra['selected_index']
        print('{},{},{}'.format(im_path, selected_index, final_prediction))

def parse_args():
    parser = argparse.ArgumentParser(description='Make predictions for Tensorflow Faster R-CNN')

    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('predictions_path')
    parser.add_argument('--rule', choices=flextop.rules, default='xtop')
    parser.add_argument('--im-width', default=640)
    parser.add_argument('--im-height', default=480)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
