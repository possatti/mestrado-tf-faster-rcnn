#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from pprint import pprint

import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

SSS_COLNAMES = 'dataset micro_avg micro_std macro_avg macro_std micro_min micro_max macro_min macro_max'.split()


def parse_a_summary(summary_path):
	summary_data = {'dataset': [], 'micro_acc': [], 'macro_acc': []}
	with open(summary_path, 'r') as f:
		summary_txt = f.read()
	# print("summary_txt:\n{}".format(summary_txt)) #!#
	# re_expr = r"""
	# 	^##\s(\w+)$
	# 	.*?
	# 	^Micro accuracy: ([\d.]+)\n
	# 	Macro accuracy: ([\d.]+)$
	# """
	# _ = re.findall(re_expr, summary_txt, flags=re.MULTILINE|re.VERBOSE|re.DOTALL)
	re_expr = r'^##\s(\w+)$.*?^Micro accuracy: ([\d.]+)\nMacro accuracy: ([\d.]+)$'
	groups = re.findall(re_expr, summary_txt, flags=re.MULTILINE|re.DOTALL)
	summary_data['dataset'], summary_data['micro_acc'], summary_data['macro_acc'] = zip(*groups)
	return summary_data

def aggregate_summaries(summaries_d):
	aggr_data = {'summary': [], 'dataset': [], 'micro_acc': [], 'macro_acc': []}
	for summary in summaries_d:
		summary_data = summaries_d[summary]
		n = len(summary_data['dataset'])
		aggr_data['summary'] += [summary] * n
		aggr_data['dataset'] += summary_data['dataset']
		aggr_data['micro_acc'] += summary_data['micro_acc']
		aggr_data['macro_acc'] += summary_data['macro_acc']
	return aggr_data

def create_sss(aggregated_summaries):
	df = pd.DataFrame(aggregated_summaries)
	df['micro_acc'] = df['micro_acc'].apply(pd.to_numeric)
	df['macro_acc'] = df['macro_acc'].apply(pd.to_numeric)
	# print("df:\n{}".format(df)) #!#
	# print("df.dtypes:\n{}".format(df.dtypes)) #!#
	sss_data = { col: [] for col in SSS_COLNAMES }
	for dataset in df['dataset'].unique():
		rows = df['dataset'] == dataset
		dataset_micros = df.loc[rows, 'micro_acc']
		dataset_macros = df.loc[rows, 'macro_acc']

		sss_data['dataset'].append(dataset)
		sss_data['micro_avg'].append(dataset_micros.mean())
		sss_data['micro_std'].append(dataset_micros.std())
		sss_data['micro_min'].append(dataset_micros.min())
		sss_data['micro_max'].append(dataset_micros.max())

		sss_data['macro_avg'].append(dataset_macros.mean())
		sss_data['macro_std'].append(dataset_macros.std())
		sss_data['macro_min'].append(dataset_macros.min())
		sss_data['macro_max'].append(dataset_macros.max())
	return sss_data

def print_macros_micros(aggregated_summaries):
	df = pd.DataFrame(aggregated_summaries)
	df['micro_acc'] = df['micro_acc'].apply(pd.to_numeric)
	df['macro_acc'] = df['macro_acc'].apply(pd.to_numeric)
	for ds in df['dataset'].unique():
		subdf = df.loc[df['dataset']==ds, ['macro_acc', 'micro_acc']]
		print(ds)
		for row in subdf.itertuples():
			# Separated by tab for easy copy-pasting into spreadsheets.
			print('{}\t{}'.format(row.macro_acc, row.micro_acc))
		print()

def print_sss(sss_data):
	# TODO: Print it prettier maybe... I think this is good enough.
	sss_df = pd.DataFrame(sss_data)
	sss_df = sss_df[['dataset', 'macro_avg', 'macro_std', 'micro_avg', 'micro_std']] #!#
	print(sss_df)

def parse_args():
	parser = argparse.ArgumentParser(description='Summary of Summaries of Summaries, possibly the last summary.')
	parser.add_argument('summary_type', choices=['sss', 'accs'])
	parser.add_argument('a_summary', nargs='+')
	parser.add_argument('--accs-only', action='store_true')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()
	print('Summaries used:')
	for summary_path in args.a_summary:
		print(' - {}'.format(summary_path))
	print()
	summaries_d = { p: parse_a_summary(p) for p in args.a_summary }
	aggr_data = aggregate_summaries(summaries_d)
	if args.summary_type == 'sss':
		sss_data = create_sss(aggr_data)
		print_sss(sss_data)
	elif args.summary_type == 'accs':
		print_macros_micros(aggr_data)

if __name__ == '__main__':
	main()
