#!/bin/bash

# set -x # Print commands.
set -e # Stop on error.

export PYTHONUNBUFFERED="True"

usage() {
  echo "usage: $0 GPU_ID NET DATASET ITERS [TAG] [-t --test]" 1>&2
}

ARGS=()
DO_TEST="false"
while [[ "$#" -gt 0 ]]; do
  case "$1" in
    -t|--test)
      DO_TEST="true"
      shift
      ;;
    -h|--help)
      usage
      exit
      ;;
    *)
      ARGS+=("$1")
      shift
      ;;
  esac
done

# Check number of arguments.
ARG_N_MIN=4 # Inclusive
ARG_N_MAX=5 # Inclusive
if [ "${#ARGS[@]}" -lt "$ARG_N_MIN" -o "${#ARGS[@]}" -gt "$ARG_N_MAX" ]; then
  echo "Expected from $ARG_N_MIN to $ARG_N_MAX args (inclusive). Got ${#ARGS[@]}." 1>&2
  usage
  exit 1
fi

GPU_ID="${ARGS[0]}"
NET="${ARGS[1]}"
DATASET="${ARGS[2]}"
ITERS="${ARGS[3]}"
TAG="${ARGS[4]}"

# Check arguments.
if [ -z "$GPU_ID" ]; then
  echo "Missing arg: GPU_ID." 1>&2
  usage
  exit 1
fi
if echo "$NET" | perl -ne 'exit 1 if /^(vgg16|res50|res101|res152|)$/'; then
  echo "Missing arg: NET." 1>&2
  usage
  exit 1
fi
if [ -z "$DATASET" ]; then
  echo "Missing arg: DATASET." 1>&2
  usage
  exit 1
fi
# if [ "$ITERS" -lt "0" ]; then
if echo "$ITERS" | perl -e '<> =~ m/^\d+$/ ? exit 1 : exit 0'; then
  echo "Error: ITERS should be a positive integer"
  usage
  exit 1
fi
if [ -z "$TAG" ]; then
  TAG="default"
fi

case ${DATASET} in
  google_nrg)
    TRAIN_IMDB="google_nrg_train"
    TEST_IMDB="google_nrg_test"
    # STEPSIZE="[50000]" # Reduce the learning rate after n. iterations.
    # ITERS=70000 # 8120 training images * (+-)60 epochs
    ;;
  google_aug0_nrg)
    TRAIN_IMDB="google_aug0_nrg_train"
    TEST_IMDB="google_aug0_nrg_test"
    ;;
  google_aug0_nrgy)
    TRAIN_IMDB="google_aug0_nrgy_train"
    TEST_IMDB="google_aug0_nrgy_test"
    ;;
  y2018_10_google_aug00_nrgr)
    TRAIN_IMDB="y2018_10_google_aug00_nrgr_train"
    TEST_IMDB="y2018_10_google_aug00_nrgr_train" #!# Só pq não tem como colocar sem a validação
    ;;
  *)
    echo "Invalid choice of DATASET." 1>&2
    usage
    exit 1
    ;;
esac

echo "ARGS:"
echo " - GPU_ID: $GPU_ID"
echo " - NET: $NET"
echo " - DATASET: $DATASET"
echo " - ITERS: $ITERS"
echo " - TAG: $TAG"

# # I have yet to test this.
# question_yN() {
#   QUESTION="$1"
#   perl -e "print '${QUESTION}'; <> =~ m/^(y|yes)$/i ? exit 0 : exit 1"
# }

NET_CONFIG="experiments/cfgs/${NET}.yml"
TAG_CONFIG="experiments/cfgs/${NET}/${TRAIN_IMDB}/${TAG}.yml"
if [ ! -f "$TAG_CONFIG" ]; then
  echo "You need to create a YAML configuration file at '${TAG_CONFIG}'." 1>&2

  read -r -p "Do you want create a file with the defaults for this network? [y/N] " REPLY
  if [ "$REPLY" == "y" ]; then
    echo "Copying '${NET_CONFIG}' to '${TAG_CONFIG}'." 1>&2
    mkdir -p $(dirname "$NET_CONFIG")
    cp "$NET_CONFIG" "$TAG_CONFIG"
  else
    echo "Ok then... Bye."
    exit 1
  fi

  read -r -p "Do you want run the network with these defaults? [y/N] " REPLY
  if [ "$REPLY" != "y" ]; then
    echo "Ok then... Bye."
    exit 1
  fi
fi


NET_FINAL_CKPT=output/${NET}/${TRAIN_IMDB}/${TAG}/${NET}_faster_rcnn_iter_${ITERS}.ckpt

# Check that we have the imagenet weights.
IMAGENET_WEIGHTS="data/imagenet_weights/${NET}.ckpt"
if [ ! -f "$IMAGENET_WEIGHTS" ]; then
  echo "You don't have the imagenet weights on '${IMAGENET_WEIGHTS}'." 1>&2
  read -r -p "Do you want to run the training even though? [y/N] " REPLY
  if [ "$REPLY" != "y" ]; then
    echo "Ok then... Bye."
    exit 1
  fi
fi

LOG="experiments/logs/${NET}_${TRAIN_IMDB}_${TAG}_${NET}.txt.`date +'%Y-%m-%d_%H-%M-%S'`"
exec &> >(tee -a "$LOG")
echo "Logging output to $LOG"

if [ ! -f ${NET_FINAL_CKPT}.index ]; then
  CUDA_VISIBLE_DEVICES=${GPU_ID} time python ./tools/trainval_net.py \
    --net ${NET} \
    --tag ${TAG} \
    --weight "$IMAGENET_WEIGHTS" \
    --imdb ${TRAIN_IMDB} \
    --imdbval ${TEST_IMDB} \
    --iters ${ITERS} \
    --cfg "$NET_CONFIG" \
    --cfg "$TAG_CONFIG"
fi

if [[ "$DO_TEST" == "true" ]]; then
  SCRIPT_DIR=$(dirname $0)
  RUN_XTOP_SCRIPT="${SCRIPT_DIR}/run_xtop.sh"
  bash "$RUN_XTOP_SCRIPT" "$GPU_ID" "../$NET_FINAL_CKPT"
fi
