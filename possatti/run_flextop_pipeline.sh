#!/usr/bin/env bash

## Set some bash options.
##  - http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit  # make script exit when a command fails.
# set -o nounset  # exit when script tries to use undeclared variables.
# set -o xtrace   # trace what gets executed.

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

usage() {
	echo "usage: $0 yolo  DATASET_GROUP DATA_CFG NET_CFG WEIGHTS --rule [xtop,fat_tl,fat_twins] --dest RESULTS_DIR_BASE"
	echo "       $0 frcnn DATASET_GROUP CKPT_PATH --rule [xtop,fat_tl,fat_twins] --dest RESULTS_DIR_BASE"
}

usage_error() {
	echo "ERROR: $1" >&2
	echo "" >&2
	usage >&2
	exit 1
}

get_name() {
	# Strip path and extension from filename.
	echo -n $(echo -n "$1" | perl -ne 'm:/([^/]+)\.\w+$:; print $1')
}

declare -a ARGS
RULE="xtop"
RESULTS_DIR_BASE="."
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		-h|--help)
			usage
			exit
			;;
		--rule)
			RULE="$2"
			shift
			;;
		--dest)
			RESULTS_DIR_BASE="$2"
			shift
			;;
		*)
			ARGS+=("$1")
			;;
	esac
	shift
done


# Gather test data in an associative array.
declare -A TEST_DATA
# Y2018_10 NRGR.
TL640_BASE_DIR="/dados/datasets/traffic_lights"
TEST_DATA[Y2018_10_GOOGLE_AUG00_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/2018_10_val_google.txt"
TEST_DATA[Y2018_10_GOOGLE_AUG00_NRGR,BASE_DIR]="/dados/horimoto/traffic_lights/2018_10/balanced"
TEST_DATA[Y2018_10_IARA_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/IARA.txt"
TEST_DATA[Y2018_10_IARA_NRGR,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[Y2018_10_IARA_NRGR,DIFFICULT_CASES]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/IARA_hard.txt"
TEST_DATA[Y2018_10_LARA_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LARA.txt"
TEST_DATA[Y2018_10_LARA_NRGR,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[Y2018_10_LARA_NRGR,DIFFICULT_CASES]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LARA_hard.txt"
TEST_DATA[Y2018_10_LISA_DAY_TEST_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_day_test.txt"
TEST_DATA[Y2018_10_LISA_DAY_TEST_NRGR,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[Y2018_10_LISA_DAY_TRAIN_NRGR,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_day_train.txt"
TEST_DATA[Y2018_10_LISA_DAY_TRAIN_NRGR,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[Y2018_10_LISA_DAY_TRAIN_NRGR,DIFFICULT_CASES]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/LISA_hard.txt"
TEST_DATA[Y2018_10_VITORIA,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/vitoria(2018_09).txt"
TEST_DATA[Y2018_10_VITORIA,BASE_DIR]="$TL640_BASE_DIR"

# Txt for testing inference times.
TEST_DATA[INFERENCE_TXT,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/inference.txt"
TEST_DATA[INFERENCE_TXT,BASE_DIR]="$TL640_BASE_DIR"
TEST_DATA[INFERENCE_SAMPLES,CSV_PATH]="/dados/horimoto/traffic_lights/2018_10/txt_files/inference_samples.txt"
TEST_DATA[INFERENCE_SAMPLES,BASE_DIR]="$TL640_BASE_DIR"

declare -A TEST_GROUPS
# TEST_GROUPS[TL640_NRGR]="TL640_GOOGLE_NRGR TL640_IARA_NRGR TL640_LARA_NRGR TL640_LISA_DAY_TEST_NRGR TL640_LISA_DAY_TRAIN_NRGR"
TEST_GROUPS[Y2018_10_NRGR,DATASETS]="Y2018_10_GOOGLE_AUG00_NRGR Y2018_10_IARA_NRGR Y2018_10_LARA_NRGR Y2018_10_LISA_DAY_TEST_NRGR Y2018_10_LISA_DAY_TRAIN_NRGR Y2018_10_VITORIA"
TEST_GROUPS[Y2018_10_NRGR,LABELS]="NRG"
TEST_GROUPS[INFERENCE,DATASETS]="INFERENCE_TXT"
TEST_GROUPS[INFERENCE,LABELS]="NRG"
TEST_GROUPS[INFERENCE_SAMPLES,DATASETS]="INFERENCE_SAMPLES"
TEST_GROUPS[INFERENCE_SAMPLES,LABELS]="NRG"
TEST_GROUP_OPTIONS=$(echo "${!TEST_GROUPS[@]}" | perl -pe 's/(\w+),\w+ ?/$1\n/g' | sort -u | paste -sd' ')

echo "POSITIONAL ARGS: ${ARGS[@]}" 1>&2

# Check which framework was chosen.
if [[ "${ARGS[0]}" == yolo ]]; then
	FRAMEWORK=yolo
	NARGS="5"
elif [[ "${ARGS[0]}" == frcnn ]]; then
	FRAMEWORK=frcnn
	NARGS="3"
else
	usage_error "Choose 'yolo' or 'frcnn'."
fi

# Check correct number of positional args.
if [[ "${#ARGS[@]}" -ne "$NARGS" ]]; then
	usage_error "You need $(($NARGS-1)) other positional arguments besides ${FRAMEWORK}."
fi

# Name and check positional arguments.
DATASET_GROUP=${ARGS[1]}
if [[ "$TEST_GROUP_OPTIONS" != *"$DATASET_GROUP"* ]]; then
	usage_error "Invalid choice for DATASET_GROUP. Valid options are: ${TEST_GROUP_OPTIONS}."
fi
LABELS=${TEST_GROUPS[$DATASET_GROUP,LABELS]}
if [[ "$FRAMEWORK" == yolo ]]; then
	DATA_CFG="${ARGS[2]}"
	NET_CFG="${ARGS[3]}"
	WEIGHTS="${ARGS[4]}"
	if [[ ! -f "$DATA_CFG" ]]; then
		usage_error "DATA_CFG must be an existing file. Path: '$DATA_CFG'"
	fi
	if [[ ! -f "$NET_CFG" ]]; then
		usage_error "NET_CFG must be an existing file. Path: '$NET_CFG'"
	fi
	if [[ ! -f "$WEIGHTS" ]]; then
		usage_error "WEIGHTS must be an existing file. Path: '$WEIGHTS'"
	fi
	DATA_CFG_NAME=$(get_name "$DATA_CFG")
	NET_CFG_NAME=$(get_name "$NET_CFG")
	WEIGHTS_NAME=$(get_name "$WEIGHTS")
elif [[ "$FRAMEWORK" == frcnn ]]; then
	CKPT_PATH="${ARGS[2]}"
	if ! compgen -G "${CKPT_PATH}*" > /dev/null; then
		usage_error "CKPT_PATH doesn't exist. Path: '$CKPT_PATH'"
	fi
	if echo "$CKPT_PATH" | perl -e '<> =~ m|.*output/(\w+)/(\w+)/(\w+)(/.*)?$| ? exit 0 : exit 1'; then
		NET=$(echo "$CKPT_PATH" | perl -pe 's|.*output/(\w+)/(\w+)/(\w+)(/.*)?$|$1|')
		TRAIN_IMDB=$(echo "$CKPT_PATH" | perl -pe 's|.*output/(\w+)/(\w+)/(\w+)(/.*)?$|$2|')
		TAG=$(echo "$CKPT_PATH" | perl -pe 's|.*output/(\w+)/(\w+)/(\w+)(/.*)?$|$3|')
		CKPT_NAME=$(echo "$CKPT_PATH" | perl -pe 's|.*output/(\w+)/(\w+)/(\w+)(/.*)?$|$4|; s:^/::')

		# Build checkpoint path.
		CHECKPOINT_DIR="output/${NET}/${TRAIN_IMDB}/${TAG}"
		NET_CONFIG="experiments/cfgs/${NET}.yml"
		TAG_CONFIG="experiments/cfgs/${NET}/${TRAIN_IMDB}/${TAG}.yml"
		if echo "$CKPT_PATH" | perl -e '<> =~ m|.*output/\w+/\w+/\w+/(.+\.ckpt).*| ? exit 0 : exit 1'; then
			CHECKPOINT="$CKPT_PATH"
		else
			# Find newest checkpoint.
			CHECKPOINT=$(find "$CHECKPOINT_DIR" -name '*ckpt.meta' -printf '%T+ %p\n' | sort --reverse | head -n 1 | cut -d' ' -f2)
		fi
		CHECKPOINT=$(echo "$CHECKPOINT" | perl -lpe 's/(\.meta|\.index)$//')
	else
		usage_error "CKPT_PATH needs to be a full path of a checkpoint from the root directory. Or the directory containing the checkpoint."
	fi
fi

if [[ "$FRAMEWORK" == yolo ]]; then
	RESULTS_DIR="${RESULTS_DIR_BASE}/results_yolo_flex/${DATA_CFG_NAME}/${NET_CFG_NAME}/${WEIGHTS_NAME}"
elif [[ "$FRAMEWORK" == frcnn ]]; then
	RESULTS_DIR="${RESULTS_DIR_BASE}/results_frcnn_flex/${NET}/${TRAIN_IMDB}/${TAG}"
fi
RULE_RESULTS_DIR="${RESULTS_DIR}/${RULE}"

DATASET_GROUP_LOWER=$(echo -n "$DATASET_GROUP" | perl -pe 's/_/-/g;$_=lc')
A_SUMMARY_PATH="${RULE_RESULTS_DIR}/a-summary-${DATASET_GROUP_LOWER}.txt"
if [ -f "$A_SUMMARY_PATH" ]; then
	rm "$A_SUMMARY_PATH"
fi

mkdir -p "$RESULTS_DIR"
mkdir -p "$RULE_RESULTS_DIR"


for TEST_NAME in ${TEST_GROUPS[$DATASET_GROUP,DATASETS]}; do
	LOWER_NAME=$(echo -n "$TEST_NAME" | perl -pe 's/_/-/g;$_=lc')
	DETECTIONS_PATH="${RESULTS_DIR}/${LOWER_NAME}--detections.txt"
	PREDICTIONS_PATH="${RULE_RESULTS_DIR}/${LOWER_NAME}--predictions.txt"
	SUMMARY_PATH="${RULE_RESULTS_DIR}/${LOWER_NAME}--summary.txt"
	echo -e "\nProcessing ${TEST_NAME} for rule ${RULE}..."

	# Create detections.
	IMAGE_LIST=$(cat "${TEST_DATA[$TEST_NAME,CSV_PATH]}" | perl -pe "s/ .*//; s:^:${TEST_DATA[$TEST_NAME,BASE_DIR]}/:")
	if [[ ! -s "${DETECTIONS_PATH}" ]]; then
		if [[ "$FRAMEWORK" == yolo ]]; then
			STDERR_FILE=$(mktemp /tmp/darknet.XXXXXX)
			echo "STDERR_FILE: $STDERR_FILE" >&2
			echo "DETECTIONS_PATH: $DETECTIONS_PATH" >&2
			echo "$IMAGE_LIST" | \
				darknet detector print "$DATA_CFG" "$NET_CFG" "$WEIGHTS" \
				> "${DETECTIONS_PATH}" \
				2> "$STDERR_FILE"
			DETECTION_STATUS="$?"
			TOTAL_INFERENCE_TIME=$(perl -lne 'm/Predicted in ([\d.]) seconds./; print "$1"' < "$STDERR_FILE" | paste -sd+ | bc)
			echo "Total inference time: ${TOTAL_INFERENCE_TIME}" >&2
		elif [[ "$FRAMEWORK" == frcnn ]]; then
			if ! python -c 'import easydict'; then
				echo "Couldn't find module easydict. You probably forgot a virtualenv or something..." 1>&2
				exit 1
			fi
			STDERR_FILE=$(mktemp /tmp/frcnn.XXXXXX)
			echo "STDERR_FILE: $STDERR_FILE" >&2
			echo "DETECTIONS_PATH: $DETECTIONS_PATH" >&2
			python "${SCRIPT_DIR}/make_predictions.py" \
				"$CKPT_PATH" \
				<(echo "$IMAGE_LIST") \
				--labels "$LABELS" \
				--net "$NET" \
				--cfg "$NET_CONFIG" \
				--cfg "$TAG_CONFIG" \
				> "${DETECTIONS_PATH}" \
				2> "$STDERR_FILE"
			DETECTION_STATUS="$?"
			TOTAL_INFERENCE_TIME=$(perl -lne 'm/Predicted in ([\d.]) seconds./; print "$1"' < "$STDERR_FILE" | paste -sd+ | bc)
			echo "Total inference time: ${TOTAL_INFERENCE_TIME}" >&2
		fi
		if [[ "$DETECTION_STATUS" -ne 0 ]]; then
			mv "${DETECTIONS_PATH}" "${DETECTIONS_PATH}.broken"
			echo "ERROR: Failed to create detections for dataset ${TEST_NAME}, rule ${RULE}, using ${FRAMEWORK}." >&2
			exit 1
		fi
	fi
	# Check the detection file, if all images have been processed.
	echo "DETECTIONS_PATH: $DETECTIONS_PATH"
	IMAGE_LIST_N=$(echo "$IMAGE_LIST" | wc -l)
	DETECTION_IMAGES=$(cat "$DETECTIONS_PATH" | perl -pe 's/^(.+):.*/$1/' | sort -u)
	DETECTIONS_IMAGES_N=$(echo "$DETECTION_IMAGES" | wc -l)
	if [[ "$IMAGE_LIST_N" -ne "$DETECTIONS_N_IMAGES" ]]; then
		echo "ERROR: The detections file doesn't have all images." >&2
		diff <(echo "$IMAGE_LIST" | sort) <(echo "$DETECTION_IMAGES")
		exit 1
	fi
	# Create predictions file and summary.
	if [[ ! -s "${SUMMARY_PATH}" ]]; then
		python "${SCRIPT_DIR}/flextop.py" "${TEST_DATA[$TEST_NAME,CSV_PATH]}" "$DETECTIONS_PATH" \
			--rule "$RULE"  \
			--bg0 \
			--base-dir "${TEST_DATA[$TEST_NAME,BASE_DIR]}" \
			--labels "$LABELS" \
			--save-img-predictions "$PREDICTIONS_PATH" \
			> "${SUMMARY_PATH}"
	fi
	# Add to the summary.
	echo >> "$A_SUMMARY_PATH"
	echo "## ${TEST_NAME}" >> "$A_SUMMARY_PATH"
	echo >> "$A_SUMMARY_PATH"
	cat "${SUMMARY_PATH}" >> "$A_SUMMARY_PATH"
	# Proccess difficult cases.
	if [[ -n "${TEST_DATA[$TEST_NAME,DIFFICULT_CASES]}" ]]; then
		echo -e "\nProcessing ${TEST_NAME} (difficult) for rule ${RULE}..."
		DIFFICULT_DETECTIONS_PATH="${RESULTS_DIR}/${LOWER_NAME}-difficult--detections.txt"
		DIFFICULT_PREDICTIONS_PATH="${RULE_RESULTS_DIR}/${LOWER_NAME}-difficult--summary.txt"
		DIFFICULT_SUMMARY_PATH="${RULE_RESULTS_DIR}/${LOWER_NAME}-difficult--summary.txt"
		# Filter the detections searching the difficult images.
		if [[ ! -s "$DIFFICULT_DETECTIONS_PATH" ]]; then
			python "${SCRIPT_DIR}/multpat.py" \
				<(cat "${TEST_DATA[$TEST_NAME,DIFFICULT_CASES]}" | perl -pe 's: .*::') \
				-f "$DETECTIONS_PATH" \
				> "$DIFFICULT_DETECTIONS_PATH"
		fi
		# Classify the difficult cases.
		if [[ ! -s "$DIFFICULT_SUMMARY_PATH" ]]; then
			python "${SCRIPT_DIR}/flextop.py" "${TEST_DATA[$TEST_NAME,DIFFICULT_CASES]}" "$DIFFICULT_DETECTIONS_PATH" \
				--rule "$RULE"  \
				--bg0 \
				--base-dir "${TEST_DATA[$TEST_NAME,BASE_DIR]}" \
				--labels "$LABELS" \
				--save-img-predictions "$DIFFICULT_PREDICTIONS_PATH" \
				> "${DIFFICULT_SUMMARY_PATH}"
		fi
		# Add to summary too.
		echo >> "$A_SUMMARY_PATH"
		echo "## ${TEST_NAME}_DIFFICULT" >> "$A_SUMMARY_PATH"
		echo >> "$A_SUMMARY_PATH"
		cat "${DIFFICULT_SUMMARY_PATH}" >> "$A_SUMMARY_PATH"
	fi
done
echo >> "$A_SUMMARY_PATH"

echo "INFO: All done."
