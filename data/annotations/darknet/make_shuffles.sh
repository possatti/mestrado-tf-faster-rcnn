#!/bin/bash

## Set some bash options.
##  - http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit  # make script exit when a command fails.
# set -o nounset  # exit when script tries to use undeclared variables.
set -o xtrace   # trace what gets executed.

usage() {
	echo "  usage: $0 IMG_LIST_TXT DATA_FILE DEST_DIR"
}

ARGS=()
while [[ "$#" -gt 0 ]]; do
        case "$1" in
                -h|--help)
                        usage
                        exit
                        ;;
                --opt)  
                        OPT="$2"
                        shift 2
                        ;;
                *)
                        ARGS+=("$1")
                        shift
                        ;;
        esac
done

IMG_LIST_TXT="${ARGS[0]}"
DATA_FILE="${ARGS[1]}"
DEST_DIR="${ARGS[2]}"
DEST_DIR="$PWD/$DEST_DIR"

mkdir -p "$DEST_DIR"

#TXT_NAME=$(basename "$IMG_LIST_TXT")
TXT_BASENAME=$(echo -n "$IMG_LIST_TXT" | perl -ne 'm:(.*/)?(.+)\.\w+$:; print $2')
for i in $(seq 0 9); do
	SHUFFLE_TXT_PATH="$DEST_DIR/${TXT_BASENAME}_s${i}.txt"
	SHUFFLE_DATA_PATH="$DEST_DIR/${TXT_BASENAME}_s${i}.data"
	shuf "$IMG_LIST_TXT" > "$SHUFFLE_TXT_PATH"
	cat "$DATA_FILE" | perl -pe "s:^(\\s*train\\s*=).*$:\$1 $SHUFFLE_TXT_PATH:" > "$SHUFFLE_DATA_PATH"
	echo "Saving shuffle '$SHUFFLE_TXT_PATH'" >&2
done
