#!/usr/bin/env bash

## Set some bash options.
##  - http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit  # make script exit when a command fails.
#set -o nounset  # exit when script tries to use undeclared variables.
set -o xtrace   # trace what gets executed.

SCRIPT_DIR=$(dirname $0)

GOOGLE_AUG0_BASE="/mnt/DADOS/horimoto/traffic_lights/balanced"
TRAIN_CSV="$SCRIPT_DIR/../detection_nrg/google_tl/google_tl_aug0_nrg_train.csv"
TEST_CSV="$SCRIPT_DIR/../detection_nrg/google_tl/google_tl_aug0_nrg_test.csv"

DEST="/dados/datasets/darknet-traffic-lights/google_aug0"
TRAIN_TXT="$DEST/google_aug0_nrg_train.txt"
TEST_TXT="$DEST/google_aug0_nrg_test.txt"
DATA_FILE="$DEST/google_aug0_nrg.data"
NRG_NAMES="$DEST/nrg.names"
BACKUP_DIR="$DEST/backup"
NETS_DIR="$DEST/nets"

mkdir -p "$DEST"
mkdir -p "$BACKUP_DIR"
mkdir -p "$NETS_DIR"

cat > "$DATA_FILE" << EOF
classes = 2
train   = $TRAIN_TXT
valid   = $TEST_TXT
names   = $NRG_NAMES
backup  = $BACKUP_DIR
eval    = $TEST_TXT
EOF

echo -e 'Red\nGreen\n' > "$NRG_NAMES"

python "$SCRIPT_DIR/../scripts/detection_csv_to_darknet.py" \
	-csv "$TRAIN_CSV" \
	-names "$NRG_NAMES" \
	-base "$GOOGLE_AUG0_BASE" \
	-images "$DEST/images/" \
	-labels "$DEST/labels/" \
	-txt "$TRAIN_TXT" \
	--force

python "$SCRIPT_DIR/../scripts/detection_csv_to_darknet.py" \
	-csv "$TEST_CSV" \
	-names "$NRG_NAMES" \
	-base "$GOOGLE_AUG0_BASE" \
	-images "$DEST/images/" \
	-labels "$DEST/labels/" \
	-txt "$TEST_TXT" \
	--force

# Create yolov2 config.
DARKNET_DIR="$HOME/projects/lcad06-darknet"
YOLOV2_CFG="$DARKNET_DIR/cfg/yolov2-voc.cfg"
N_CLASSES=2
N_FILTERS=$(echo "5*($N_CLASSES+5)" | bc)
cp "$YOLOV2_CFG" "$NETS_DIR"
echo "In the cfg file, remember to change the beggining for training; change [yolo]classes to $N_CLASSES, and the last convolutional filters to num*(classes+5)=$N_FILTERS"
