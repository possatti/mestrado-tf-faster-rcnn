#!/usr/bin/env bash

## Set some bash options.
##  - http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit  # make script exit when a command fails.
#set -o nounset  # exit when script tries to use undeclared variables.
set -o xtrace   # trace what gets executed.

usage() {
  echo " Usage: $0"
  echo
  echo ' Arguments:'
  # echo '   labels - One of [NRG, NRGY, NRGR].'
  echo
  echo ' Options:'
  echo '   -h --help'
  echo '      Shows the usage.'
  exit
}

ARGS=()
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		-h|--help)
			usage
			exit
			;;
		--opt)
			OPT="$2"
			shift 2
			;;
		*)
			ARGS+=("$1")
			shift
			;;
	esac
done

SCRIPT_DIR=$(dirname $0)

BASE_DIR="/dados/horimoto/traffic_lights/2018_10/balanced"
TRAIN_CSV="$SCRIPT_DIR/../y2018_10/detection_nrgr/y2018_10_google_aug00_nrgr_train.csv"
TEST_CSV="$SCRIPT_DIR/../y2018_10/detection_nrgr/y2018_10_google_aug00_nrgr_test_temp.csv" # We don't have a testing set totally ready yet!

# DEST="/dados/datasets/darknet_y2018_10/google_aug00"
DEST="/dados/datasets/darknet_y2018_10/google_aug00"
TRAIN_TXT="$DEST/y2018_10_google_aug00_nrgr_train.txt"
TEST_TXT="$DEST/y2018_10_google_aug00_nrgr_test_temp.txt"
DATA_FILE="$DEST/y2018_10_google_aug00_nrgr.data"
TEST_DATA_FILE="$DEST/y2018_10_google_aug00_nrgr_test_temp.data"
NRG_NAMES="$DEST/nrg.names"
BACKUP_DIR="$DEST/backup"
NETS_DIR="$DEST/nets"

mkdir -p "$DEST"
mkdir -p "$BACKUP_DIR"
mkdir -p "$NETS_DIR"

cat > "$DATA_FILE" << EOF
classes = 2
train   = $TRAIN_TXT
valid   = $TRAIN_TXT
names   = $NRG_NAMES
backup  = $BACKUP_DIR
eval    = $TRAIN_TXT
EOF

cat > "$TEST_DATA_FILE" << EOF
classes = 2
train   = $TRAIN_TXT
valid   = $TEST_TXT
names   = $NRG_NAMES
backup  = $BACKUP_DIR
eval    = $TEST_TXT
EOF

cat > "$NRG_NAMES" << EOF
Red
Green
EOF

python "$SCRIPT_DIR/../scripts/detection_csv_to_darknet.py" \
	-csv "$TRAIN_CSV" \
	-names "$NRG_NAMES" \
	-base "$BASE_DIR" \
	-images "$DEST/images/" \
	-labels "$DEST/labels/" \
	-txt "$TRAIN_TXT" \
	--force

python "$SCRIPT_DIR/../scripts/detection_csv_to_darknet.py" \
	-csv "$TEST_CSV" \
	-names "$NRG_NAMES" \
	-base "$BASE_DIR" \
	-images "$DEST/images/" \
	-labels "$DEST/labels/" \
	-txt "$TEST_TXT" \
	--force

# # Create yolov2 config.
# DARKNET_DIR="$HOME/projects/lcad06-darknet"
# YOLOV2_CFG="$DARKNET_DIR/cfg/yolov2-voc.cfg"
# N_CLASSES=2
# N_FILTERS=$(echo "5*($N_CLASSES+5)" | bc)
# cp "$YOLOV2_CFG" "$NETS_DIR"
# echo "In the cfg file, remember to change the beggining for training; change [yolo]classes to $N_CLASSES, and the last convolutional filters to num*(classes+5)=$N_FILTERS"
