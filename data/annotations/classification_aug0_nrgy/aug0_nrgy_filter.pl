use strict;
use warnings;

## Print only the lines that are aug0 and that have the desired label. The input
## is a txt file separed by space, and the output is a csv file.
## @possatti

my $unexpected_label_warning = 0;

while (<STDIN>) {
	# Split on spaces.
	my $line = $_;
	chomp($line);
	my ($path, $label) = split(/\s+/, $line);

	# Only prints ok instances.
	my $ok = 1; # true

	# Check that the label is correct.
	if ($label =~ m/^[0123]$/) {
		# OK
	} elsif ($label =~ m/^\d[sp]$/) {
		if (! $unexpected_label_warning) {
			$unexpected_label_warning = 1;
			print STDERR "WARN: Found an unexpected label: $label.\n";
		}
		$ok = 0;
	} else {
		$ok = 0;
	}

	# Check it is aug0.
	if ($path !~ m/aug0\./) {
		$ok = 0;
	}

	if ($ok) {
		print("$path,$label\n");
	}
}
