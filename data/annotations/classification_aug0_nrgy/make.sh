#!/usr/bin/env bash

SCRIPT_DIR=$(dirname $0)
cd "$SCRIPT_DIR"

# Google
cat ../original/bal_google.txt ../original/bal_google_yellow.txt | python ../scripts/google_train_test.py test > bal_google_nrgy_test.txt
cat bal_google_nrgy_test.txt | perl aug0_nrgy_filter.pl > google_aug0_nrgy_test.csv

# IARA
cat ../original/bal_IARA.txt ../original/bal_IARA_yellow.txt | perl aug0_nrgy_filter.pl > IARA_aug0_nrgy.csv

cd -