#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from collections import namedtuple

import pandas as pd
import argparse
import random
import json
import sys
import re
import os

print(sys.version, file=sys.stderr)

SCRIPT_DIR = os.path.dirname(__file__)
TL_STATUSES = {
	1: 'Red',
	2: 'Green',
}

IMAGE_EXTENSIONS = ['.jpg', '.jpeg', '.png']
CLASS_NAME_TRANSLATION = {
	# 'OffTrafficLight': 'OffTrafficLight',
	'RedTrafficLight': 'RedTrafficLight',
	'GreeTrafficLight': 'GreenTrafficLight',
	'YellowTrafficLight': 'YellowTrafficLight',
}

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--detection-csv', default=os.path.join(SCRIPT_DIR, 'iara_traffic_light_detection_nrg.csv'))
	parser.add_argument('--iara-traffic-light', default='/dados/datasets/traffic-lights/IARA_traffic_light')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	random.seed(7)

	images_dir_path = os.path.join(args.iara_traffic_light, 'images')
	labels_dir_path = os.path.join(args.iara_traffic_light, 'labels')

	colnames = 'path x1 y1 x2 y2 class_name'.split()
	coltypes = [str, int, int, int, int, str]

	df_data = { name: [] for name in colnames }

	image_filenames = os.listdir(images_dir_path)
	# image_filenames = random.sample(image_filenames, 50) #!#
	for i, filename in enumerate(image_filenames):
		print('INFO: {} of {}.\r'.format(i+1, len(image_filenames)), end='', file=sys.stderr)
		basename, file_extension = os.path.splitext(filename)
		if file_extension.lower() in IMAGE_EXTENSIONS:
			image_rel_path = os.path.join('IARA_traffic_light', 'images', filename)
			image_abs_path = os.path.join(images_dir_path, filename)
			txt_abs_path = os.path.join(labels_dir_path, basename+'.txt')

			if os.path.isfile(txt_abs_path):
				with open(txt_abs_path, 'r') as f:
					lines = f.readlines()
				for line in lines:
					line = line.strip()
					if line is '':
						continue
					try:
						class_name, _, _, _, x1, y1, x2, y2, _, _, _, _, _, _, _ = line.split()
					except ValueError as e:
						print("line:", line, file=sys.stderr) #!#
						raise e
					if class_name in CLASS_NAME_TRANSLATION:
						class_name = CLASS_NAME_TRANSLATION[class_name]
						x1, y1, x2, y2 = int(float(x1)), int(float(y1)), int(float(x2)), int(float(y2))
						df_data['path'].append(image_rel_path)
						df_data['x1'].append(x1)
						df_data['y1'].append(y1)
						df_data['x2'].append(x2)
						df_data['y2'].append(y2)
						df_data['class_name'].append(class_name)
					else:
						# print('class_name {} is not on the dictionary.'.format(class_name), file=sys.stderr)
						pass
			else:
				print('WARN: {} doesn\'t exist.'.format(txt_abs_path), file=sys.stderr)

	df = pd.DataFrame(df_data, columns=colnames)
	df.to_csv(args.detection_csv, index=False, header=False)

if __name__ == '__main__':
	main()
