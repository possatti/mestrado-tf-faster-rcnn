#!/usr/bin/env bash

## Set some bash options.
##  - http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit  # make script exit when a command fails.
#set -o nounset  # exit when script tries to use undeclared variables.
# set -o xtrace   # trace what gets executed.

GOOGLE_TL_BASE="/dados/datasets/traffic-lights/google-tl"
GOOGLE_TL_AUG0_BASE="/mnt/DADOS/horimoto/traffic_lights/balanced"

SCRIPT_DIR=$(dirname $0)
cd "$SCRIPT_DIR"

# ------------------------------------------------------------------------------
# Create files by simply converting all entry on the original csvs.
# ------------------------------------------------------------------------------

# if [ ! -f google_tl_aug0_nrg_train.csv ]; then
# 	cat google_tl_nrg_train.csv | perl -pe 's:REGIONS/(\w+)/(\d{3})/(P\w+)\.jpg:Google_automatics/REGIONS/$1/$2/IMG/SIGNAL/$3/aug0.png:' > google_tl_aug0_nrg_train.csv
# fi
# if [ ! -f google_tl_aug0_nrg_test.csv ]; then
# 	cat google_tl_nrg_test.csv | perl -pe 's:REGIONS/(\w+)/(\d{3})/(P\w+)\.jpg:Google_automatics/REGIONS/$1/$2/IMG/SIGNAL/$3/aug0.png:' > google_tl_aug0_nrg_test.csv
# fi


# ------------------------------------------------------------------------------
# Create files filtering out the images that do not exist.
# ------------------------------------------------------------------------------

PERL_FILTER_E="s:REGIONS/(\w+)/(\d{3})/(P\w+)\.jpg:Google_automatics/REGIONS/\$1/\$2/IMG/SIGNAL/\$3/aug0.png:; @p=split(/,/); print if -f \"$GOOGLE_TL_AUG0_BASE/@p[0]\";"
# PERL_FILTER_E="s:REGIONS/(\w+)/(\d{3})/(P\w+)\.jpg:Google_automatics/REGIONS/\$1/\$2/IMG/SIGNAL/\$3/aug0.png:; @p=split(/,/); print \"$GOOGLE_TL_AUG0_BASE/@p[0]\";" #!#

cat google_tl_nrg_train.csv | perl -lne "$PERL_FILTER_E" > google_tl_aug0_nrg_train.csv
cat google_tl_nrg_test.csv | perl -lne "$PERL_FILTER_E" > google_tl_aug0_nrg_test.csv

# ------------------------------------------------------------------------------
# Check if all images really exist.
# ------------------------------------------------------------------------------

BASE="$GOOGLE_TL_AUG0_BASE"

# PERL_COUNTER_E="\$c=\$n=0; while (<>) { if (-f '${GOOGLE_TL_AUG0_BASE}/\$_') {\$c++} else {\$n++} }; print \"Existing files: \$c\n\"; print \"Missing files: \$n\n\";"
read -r -d '\0' PERL_COUNTER_E << EOF
\$c=\$n=0;
while (<>) {
	s/,.*//;
	chomp;
	\$impath = "${BASE}/\$_";
	if (-f \$impath) {
		\$c++;
	}
	else {
		\$n++;
		print "Missing file: '\$impath'."
	}
};
print " - Existing files: \$c";
print " - Missing files: \$n";
\0
EOF

# echo "PERL_COUNTER_E: $PERL_COUNTER_E" #!#
# echo #!#

# echo -e "\nGOOGLE TL TRAIN: "
# cat google_tl_nrg_train.csv | perl -lne "$PERL_COUNTER_E"
# echo -e "\nGOOGLE TL TEST: "
# cat google_tl_nrg_test.csv | perl -lne "$PERL_COUNTER_E"

echo -e "\nGOOGLE TL AUG0 TRAIN: "
cat google_tl_aug0_nrg_train.csv | perl -le "$PERL_COUNTER_E"
echo -e "\nGOOGLE TL AUG0 TEST: "
cat google_tl_aug0_nrg_test.csv | perl -le "$PERL_COUNTER_E"

cd -