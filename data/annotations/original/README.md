
## Sources

The origin of the files.

From `10.9.8.10:/dados/horimoto/traffic_lights/txt_files/val_test/`:
 - google_val.txt (I'll probably use bal_google_val-bbox.txt instead)

From `10.9.8.54:/mnt/DADOS/horimoto/traffic_lights/balanced/`:
 - bal_google.txt
 - bal_google_val-bbox.txt
 - bal_IARA.txt
 - bal_LARA.txt
 - bal_LISA_day_test.txt
 - bal_LISA_day_train.txt
 - horimoto_IARA.txt **probably**
 - horimoto_LARA.txt **probably**
 - horimoto_LISA_day_test.txt **probably**
 - horimoto_LISA_day_train.txt **probably**
 - bal_google_yellow.txt (I think I am the author)
 - bal_IARA_yellow.txt (I think I am the author)
