#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import pandas as pd
import numpy as np
import argparse
import glob
import sys
import os
import re

print(sys.version, file=sys.stderr)

STRIP_AUG_RE = re.compile(r'(.*)/aug0\.png')
# IMAGE_EXTENSIONS = ['.jpg', '.png']

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--txt-path', default=os.path.join(os.path.dirname(__file__), 'traffic_lights_ambiguous.txt'))
	parser.add_argument('--save-csv-path', default=os.path.join(os.path.dirname(__file__), 'traffic_lights_ambiguous.csv'))
	parser.add_argument('--base-dir', default='/dados/datasets/traffic-lights')
	args = parser.parse_args()
	return args

def find_ext(basepath):
	"""Receives a path without the file extension, and returns the extension that makes it a valid path."""
	glob_pattern = basepath + '.*'
	abs_paths = glob.glob(glob_pattern)
	if len(abs_paths) == 0:
		return None
	else:
		file_basename, file_extension = os.path.splitext(abs_paths[0])
		return file_extension

def main():
	args = parse_args()
	df = pd.read_csv(args.txt_path, sep=' ', names=['augpath', 'label'])

	# df = df.sample(n=50) #!#
	# df = df.append({'augpath': 'aaaaaa/aug0.png', 'label': 2}, ignore_index=True) #!#
	# print("len(df):", len(df), file=sys.stderr) #!#
	# print("df.tail():\n{}".format(df.tail())) #!#

	colnames = ['path', 'label']
	new_data = { name: [] for name in colnames }
	failed_images = []
	print(file=sys.stderr)
	for row in df.itertuples():
		print('\x1b[A\x1b[2KINFO: {} of {}'.format(row.Index+1, len(df)), file=sys.stderr)
		base_rel_path = STRIP_AUG_RE.search(row.augpath).group(1)
		base_abs_path = os.path.join(args.base_dir, base_rel_path)
		ext = find_ext(base_abs_path)
		if ext is None:
			failed_images.append(row.augpath)
			print('WARN: Couldn\'t find {}.'.format(row.augpath), file=sys.stderr)
		else:
			complete_rel_path = base_rel_path + ext
			new_data['path'].append(complete_rel_path)
			new_data['label'].append(row.label)

	print('Couldn\'t find {} images of {}.'.format(len(failed_images), len(df)))

	new_df = pd.DataFrame(new_data, columns=colnames)
	new_df.to_csv(args.save_csv_path, index=False, header=False)

if __name__ == '__main__':
	main()
