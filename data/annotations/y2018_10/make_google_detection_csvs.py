#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import json
import sys
import os
import re

print(sys.version, file=sys.stderr)

RE_TEST_SET = re.compile(r'REGIONS/(Belo_Horizonte/002|Belo_Horizonte/000|Belo_Horizonte/001|Belo_Horizonte/003|Brasilia/002|Brasilia/000|Brasilia/001|Sao_Paulo/006|Sao_Paulo/013|Sao_Paulo/001|Sao_Paulo/003|Sao_Paulo/019)/')
RE_TL_PATH = re.compile(r'REGIONS/(?P<region>\w+)/(?P<subregion>\d{3})/(?P<name>.+)\.jpg')
COLNAMES = ['path', 'x1', 'y1', 'x2', 'y2', 'class_name']

CLASSIFICATION_MAPS = {
	'NRG':  {0: 'None', 1:'Red', 2:'Green'},
	'NRGY': {0: 'None', 1:'Red', 2:'Green', 3:'Yellow'},
	'NRGR': {0: 'None', 1:'Red', 2:'Green', 3:'Red'},
}

DETECTION_MAPS = {
	'NRG':  {1:'Red', 2:'Green'},
	'NRGY': {1:'Red', 2:'Green', 3:'Yellow'},
	'NRGR': {1:'Red', 2:'Green', 3:'Red'},
}

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('--labels', choices=['NRG', 'NRGY', 'NRGR'], default='NRGR')
	# parser.add_argument('--classification-txt', default='/dados/horimoto/traffic_lights/2018_10/txt_files/train/2018_10_bal_google-bbox.txt')
	parser.add_argument('--classification-txt', default='/dados/horimoto/traffic_lights/2018_10/txt_files/val_test/2018_10_val_google.txt')
	parser.add_argument('--tl-annotations', default='/dados/datasets/traffic-lights/google-tl/tl-annotations.json')
	parser.add_argument('--base-dir', default='/dados/horimoto/traffic_lights/2018_10/balanced')
	parser.add_argument('--ignore-missing-detection-gt', action='store_true')
	parser.add_argument('--save-train')
	parser.add_argument('--save-test')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	classification_data = pd.read_csv(args.classification_txt, sep=' ', names=['path', 'label'])

	with open(args.tl_annotations, 'r') as f:
		tl_data = json.load(f)

	missing_images = []
	detections = {
		'train': { c: [] for c in COLNAMES },
		'test': { c: [] for c in COLNAMES },
	}

	valid_images_with_invalid_tls_only = []
	not_in_classification_list = []
	ignoring_no_tl_counter = 0
	ignoring_others_counter = 0
	for i, entry in enumerate(tl_data):
		set_ = 'test' if RE_TEST_SET.search(entry['filepath']) else 'train'
		match = RE_TL_PATH.match(entry['filepath'])
		if match:
			g = match.groupdict()
			aug00_path = 'Google_automatics/REGIONS/{region}/{subregion}/IMG/SIGNAL/{name}/aug00.png'.format(**g)
		else:
			raise ValueError('filepath doesn\'t match: `{}`'.format(entry['filepath']))
		if aug00_path in classification_data['path'].values:
			class_row = classification_data.loc[classification_data['path']==aug00_path]
			assert len(class_row) == 1, 'Multiple entries in classification list for {}'.format(aug00_path)
			class_row = class_row.squeeze()
			assert CLASSIFICATION_MAPS[args.labels][entry['main_traffic_light_status']] == CLASSIFICATION_MAPS[args.labels][class_row['label']], 'Labels from tl-annotations.json and the classification file are different for `{}`'.format(aug00_path)
			if os.path.isfile(os.path.join(args.base_dir, aug00_path)):
				if entry['main_traffic_light_status'] in [1,2,3]:
					valid_tls = 0
					invalid_tls = 0
					for tl in entry['traffic_lights']:
						if tl['status'] in DETECTION_MAPS[args.labels]: # Filters out 4 (others), for example.
							valid_tls += 1
							detections[set_]['path'].append(aug00_path)
							detections[set_]['x1'].append(tl['bbox'][0][0])
							detections[set_]['y1'].append(tl['bbox'][0][1])
							detections[set_]['x2'].append(tl['bbox'][1][0])
							detections[set_]['y2'].append(tl['bbox'][1][1])
							detections[set_]['class_name'].append(DETECTION_MAPS[args.labels][tl['status']])
						else:
							invalid_tls += 1
					if valid_tls + invalid_tls == 0:
						if args.ignore_missing_detection_gt:
							print('Ignoring images with missing annotation ground truth: `{}`'.format(entry['filepath']), file=sys.stderr)
						else:
							raise ValueError('RGY images should have bounding box annotations. `{}` doesn\'t'.format(entry['filepath']))
					elif valid_tls == 0:
						valid_images_with_invalid_tls_only.append(entry['filepath'])
				elif entry['main_traffic_light_status'] == 0:
					ignoring_no_tl_counter += 1
				elif entry['main_traffic_light_status'] > 3:
					ignoring_others_counter += 1
			else:
				missing_images.append(aug00_path)
		else:
			not_in_classification_list.append(aug00_path)
		print('INFO: {} of {}.\r'.format(i+1, len(tl_data)), end='', file=sys.stderr)
	print(file=sys.stderr)

	df_train = pd.DataFrame(detections['train'])
	df_test = pd.DataFrame(detections['test'])
	if args.save_train:
		df_train.to_csv(args.save_train, header=False, columns=COLNAMES, index=False)
	else:
		if len(df_train) > 0:
			print('Training samples:')
			print(df_train)
	if args.save_test:
		df_test.to_csv(args.save_test, header=False, columns=COLNAMES, index=False)
	else:
		if len(df_test) > 0:
			print('Testing samples:')
			print(df_test)

	class_aug00s = [ p for p in classification_data['path'] if 'aug00.png' in p ]
	class_aug00s_set = set(class_aug00s)
	assert len(class_aug00s) == len(class_aug00s_set), 'Duplicated images on the classification file.'
	train_aug00s_set = set(df_train['path'].values)
	test_aug00s_set = set(df_test['path'].values)
	overflow_train = class_aug00s_set - train_aug00s_set
	overflow_test = class_aug00s_set - test_aug00s_set
	# for p in overflow: #!#
	# 	print('OVERFLOW: {}'.format(p)) #!#
	print("Classification images set count:", len(class_aug00s_set), file=sys.stderr)
	print("Training images set count:", len(train_aug00s_set), file=sys.stderr)
	print('Training overflow count: {}'.format(len(overflow_train)), file=sys.stderr)
	print('Testing overflow count: {}'.format(len(overflow_test)), file=sys.stderr)
	print()

	print('Valid images with invalid tls only:')
	for path in valid_images_with_invalid_tls_only:
		print(' - {}'.format(path))
	print('Number of valid images with invalid tls only: {}'.format(len(valid_images_with_invalid_tls_only)))
	print()

	print('main_tl == NO_TL  ignored:', ignoring_no_tl_counter, file=sys.stderr)
	print('main_tl == OTHERS ignored:', ignoring_others_counter, file=sys.stderr)
	print()

	print('Training images: {}'.format(len(df_train['path'].unique())), file=sys.stderr)
	print('Testing images: {}'.format(len(df_test['path'].unique())), file=sys.stderr)
	print('Missing images: {}'.format(len(missing_images)), file=sys.stderr)

if __name__ == '__main__':
	main()
