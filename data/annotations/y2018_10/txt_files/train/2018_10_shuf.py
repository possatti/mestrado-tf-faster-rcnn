import random

N_CLASSES = 3

for i in range(10):
	random.seed((i + 1)*1000000 + 201810)

	for txt_file in ['2018_10_bal_google-bbox.txt']:
		with open(txt_file, 'r') as labels:
			lines = labels.read().splitlines()
			random.shuffle(lines)
			lines_per_class = [[] for c in range(N_CLASSES)]
			for line in lines:
				values = line.split()
				lines_per_class[int(values[1])].append(line)
			for j in range(len(lines_per_class[0])):
				for c in range(N_CLASSES):
					with open('2018_10_seed' + str(i) + txt_file[7:], 'a') as shuffled:
						shuffled.write(lines_per_class[c][j] + '\n')
