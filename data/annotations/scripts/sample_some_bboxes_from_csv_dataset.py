#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import pandas as pd
import argparse
import sys
import cv2
import os

print(sys.version, file=sys.stderr)

def draw_dets(ax, class_name, dets, color='white'):
    """Draw detected bounding boxes."""

    for i in range(len(dets)):
        bbox = dets[i, :4]
        score = dets[i, -1]

        ax.add_patch(
            plt.Rectangle(
                (bbox[0], bbox[1]),
                bbox[2] - bbox[0],
                bbox[3] - bbox[1], fill=False,
                edgecolor=color, linewidth=3.5
            )
        )
        ax.text(bbox[0], bbox[1] - 2,
                '{:s} {:.3f}'.format(class_name, score),
                bbox=dict(facecolor=color, alpha=0.5),
                fontsize=14, color='white')

def draw_box(ax, image, x1, y1, width, height, color='white'):
    """Draw detected bounding boxes."""

    if type(image) is str:
    	# image = cv2.imread(image)
    	image = plt.imread(image)

    ax.imshow(image)

    ax.add_patch(
        plt.Rectangle(
            (x1, y1),
            width,
            height, fill=False,
            edgecolor=color, linewidth=3.5
        )
    )

COLNAMES = 'path x1 y1 x2 y2 class_name'.split()

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('csv_file')
	parser.add_argument('--base-dir')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	if not os.path.isfile(args.csv_file):
		raise ValueError('File doesn\'t exist: '.format(args.csv_file))

	df = pd.read_csv(args.csv_file, names=COLNAMES)

	min_area = None
	min_area_path = None

	df['width'] = df.x2 - df.x1
	df['height'] = df.y2 - df.y1
	df['area'] = df.width * df.height
	df['ratio'] = df.height / df.width

	# min_area_sample = df.iloc[df['area'].argmin()]
	# max_area_sample = df.iloc[df['area'].argmax()]
	# print("min_area_sample:", min_area_sample, file=sys.stderr) #!#
	# print("max_area_sample:", max_area_sample, file=sys.stderr) #!#

	df_sample = df.sample(n=5)

	if args.base_dir is not None:
		min_abs = os.path.join(args.base_dir, min_area_sample['path'])
		max_abs = os.path.join(args.base_dir, max_area_sample['path'])
	fig, ax = plt.subplots(1, 1)
	draw_box(ax, min_abs, min_area_sample['x1'], min_area_sample['y1'], min_area_sample['width'], min_area_sample['height'])
	plt.show()
	fig, ax = plt.subplots(1, 1)
	draw_box(ax, max_abs, max_area_sample['x1'], max_area_sample['y1'], max_area_sample['width'], max_area_sample['height'])
	plt.show()


	for row in df_sample.itertuples():
		fig, ax = plt.subplots(1, 1)
		abs_path = row.path
		if args.base_dir is not None:
			abs_path = os.path.join(args.base_dir, abs_path)
		draw_box(ax, abs_path, row.x1, row.y1, row.width, row.height)
		plt.show()



	# for row in df.itertuples():
	# 	width = row.x2 - row.x1
	# 	height = row.y2 - row.y1
	# 	area = width
	# 	print("row:", row, file=sys.stderr) #!#
	# 	print("width:", width, file=sys.stderr) #!#
	# 	print("height:", height, file=sys.stderr) #!#
	# 	exit()

if __name__ == '__main__':
	main()
