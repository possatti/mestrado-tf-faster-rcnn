#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from PIL import Image

import pandas as pd
import numpy as np
import argparse
import sys
import os
import re

print(sys.version, file=sys.stderr)

COLNAMES = 'path x1 y1 x2 y2 class_name'.split()

def arg_list(arg, sep=r', '):
	pieces = re.split(sep, arg)
	return pieces

def parse_args():
	parser = argparse.ArgumentParser(description='')
	# Inputs
	parser.add_argument('-csv', required=True, help='CSV file containing annotations for an image dataset.')
	parser.add_argument('-names', required=True, help='`.names` file (ordered, one class name per line).')
	parser.add_argument('-base', required=True, help='Base directory for where the images are.')
	# Outputs
	parser.add_argument('-images', required=True, help='Directory that will contain the images.')
	parser.add_argument('-labels', required=True, help='Directory that will contain the labels in the Darknet format.')
	parser.add_argument('-txt', required=True, help='Path to where the darknet txt will be created.')
	parser.add_argument('--force', action='store_true', help='Overwrite existing files if necessary.')
	# TODO: `--cut-dir` could be used when the paths are absolute. In order for
	# us to know which part should be cut in order to copy to `dest_dir`.
	# parser.add_argument('--cut-dir')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	# Check if we will need to overwrite anything.
	if os.path.isfile(args.txt):
		if not args.force:
			print('ERROR: File `{}` already exists. Use `--force` if you want to overwrite it.'.format(args.txt), file=sys.stderr)
			exit(1)
	if os.path.isdir(args.images):
		# Make sure the directory is empty.
		dir_is_empty = len(os.listdir(args.images)) == 0
		if not (dir_is_empty or args.force):
			print('You\'re such a bufoon!! The directory you provided is not empty, and I refuse to work in these conditions.', file=sys.stderr)
			print('Farewell!', file=sys.stderr)
			exit(1)
	else:
		print('INFO: The destination directory doesn\'t exist. Creating it.', file=sys.stderr)
		os.makedirs(args.images)

	df = pd.read_csv(args.csv, names=COLNAMES)
	df['abspath'] = [ os.path.join(args.base, p) for p in df['path'] ]

	img_abspaths = df['abspath'].unique()

	dest_images_dir = args.images
	dest_labels_dir = args.labels

	with open(args.names, 'r') as f:
		class_names = [ l.strip() for l in f.readlines() ]

	# List that will compose the "image_list.txt".
	image_list = []

	for i, origin_img_abspath in enumerate(img_abspaths):
		# Get image information.
		im = Image.open(origin_img_abspath)
		im_width = im.width
		im_height = im.height
		im.close()

		# Copy the original image file to the destination.
		img_relpath = os.path.relpath(origin_img_abspath, start=args.base)
		img_relpath = re.sub(r'/', '-', img_relpath) # So that we don't get a bunch of subdirs.
		dest_img_abspath = os.path.join(dest_images_dir, img_relpath)
		dest_img_dir = os.path.dirname(dest_img_abspath)
		if not os.path.isdir(dest_img_dir):
			os.makedirs(dest_img_dir)
		if os.path.lexists(dest_img_abspath):
			os.remove(dest_img_abspath)
		os.symlink(origin_img_abspath, dest_img_abspath)
		image_list.append(dest_img_abspath)

		# Create annotations.
		img_relbasepath, _ = os.path.splitext(img_relpath)
		dest_txt_abspath = os.path.join(dest_labels_dir, img_relbasepath+'.txt')
		dest_txt_dir = os.path.dirname(dest_txt_abspath)
		if not os.path.isdir(dest_txt_dir):
			os.makedirs(dest_txt_dir)
		with open(dest_txt_abspath, 'w') as txt_f:
			img_entries_df = df.loc[df['abspath']==origin_img_abspath]
			for entry in img_entries_df.itertuples():
				box_width = entry.x2 - entry.x1
				box_height = entry.y2 - entry.y1
				box_center_x = entry.x1 + box_width/2
				box_center_y = entry.y1 + box_height/2

				dark_width  = box_width / im_width
				dark_height = box_height / im_height
				dark_x      = box_center_x / im_width
				dark_y      = box_center_y / im_height

				if entry.class_name in class_names:
					# 0 is the first class, not bg.
					class_num = class_names.index(entry.class_name)
				else:
					raise ValueError('Unexpected class `{}` is not in {}.'.format(entry.class_name, class_names))

				txt_f.write('{} {} {} {} {}\n'.format(class_num, dark_x, dark_y, dark_width, dark_height))

		# Print progress.
		print('INFO: Processed {} of {} images.\r'.format(i+1, len(img_abspaths)), end='', file=sys.stderr)
	print(file=sys.stderr)

	# Save image list file.
	with open(args.txt, 'w') as f:
		f.write('\n'.join(image_list)+'\n')

if __name__ == '__main__':
	main()
