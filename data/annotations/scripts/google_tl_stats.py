#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import sys
import os

print(sys.version, file=sys.stderr)


def parse_args():
	SCRIPT_DIR = os.path.dirname(__file__)
	parser = argparse.ArgumentParser(description='')
	# parser.add_argument('--google-tl', default='/dados/datasets/traffic-lights/google-tl')
	# parser.add_argument('--json-annotations', default='/dados/datasets/traffic-lights/google-tl/tl-annotations_horimoto_2018-04-24.json')
	parser.add_argument('--detection-csv', default=os.path.join(SCRIPT_DIR, 'detection_nrg', 'google_tl', 'google_tl_detection_nrg_train.csv'))
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	colnames = ['path', 'x1', 'y1', 'x2', 'y2', 'class_name']
	df = pd.read_csv(args.detection_csv, names=colnames)
	# df = df.sample(n=100) #!#
	df['width'] = df['x2'] - df['x1']
	df['height'] = df['y2'] - df['y1']
	df['area'] = df['width'] * df['height']
	df['ratio'] = df['height'] / df['width']

	fig, axes = plt.subplots(2, 2)
	inds = [
		(0,0),
		(0,1),
		(1,0),
		(1,1),
	]
	attributes = ['width', 'height', 'area', 'ratio']
	att_bins = [
		range(0, 40, 1),
		range(0, 80, 1),
		range(0, 2000, 50),
		range(0, 7, 1),
	]
	for ax_inds, att, bins in zip(inds, attributes, att_bins):
		ax = axes[ax_inds]
		# ax.hist(df[att])
		ax.hist(df[att], bins=bins)
		ax.set_title(att)
	plt.show()

if __name__ == '__main__':
	main()
