#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import random
import sys
import os
import re

print('Deprecated!!!', file=sys.stderr)
print('Deprecated!!!', file=sys.stderr)
print('Deprecated!!!', file=sys.stderr)


ANNOTATION_COLNAMES = ['class_num', 'dark_x', 'dark_y', 'dark_width', 'dark_height']
RE_IMAGE = re.compile(r'.+\.(jpg|jpeg|png)$', flags=re.IGNORECASE)

COLORS = {
	# 'NRG': ['black', 'red', 'green'],
	# 'NRGY': ['black', 'red', 'green', 'yellow'],
	'NRG': ['red', 'green'],
	'NRGY': ['red', 'green', 'yellow'],
	'HSV': matplotlib.cm.get_cmap('hsv'),
}

def parse_ini(ini_path):
	with open(ini_path, 'r') as f:
		lines = f.readlines()

	# Remove comments and empty lines.
	lines = [ re.sub(r'#.*', '', l).strip() for l in lines ]
	lines = [ l for l in lines if not re.match(r'^\s*$', l) ]

	# Split keys and values.
	d = {}
	for line in lines:
		key, value = [ x.strip() for x in line.split('=') ]
		d[key] = value
	return d

def find(dir_path, regex=r'.*'):
	if type(regex) is str:
		regex = re.compile(regex)
	file_list = []
	for root, dirs, files in os.walk(dir_path):
		for filename in files:
			filepath = os.path.join(root, filename)
			if regex.match(filepath):
				file_list.append(filepath)
	return file_list

def plot_image_annotations(image_path, labels_path, class_names, class_colors=None):
	fig, ax = plt.subplots(1, 1)

	n_classes = len(class_names)
	if class_colors is None:
		hsv_cmap = matplotlib.cm.get_cmap('hsv')
		class_colors = [ hsv_cmap(x) for x in np.linspace(0, 1, n_classes+1) ]

	# Draw image.
	im = plt.imread(image_path)
	im_height, im_width, im_channels = im.shape
	ax.imshow(im)

	# Draw annotations.
	annotations_df = pd.read_csv(labels_path, sep=' ', names=ANNOTATION_COLNAMES)
	for ann in annotations_df.itertuples():
		top = (ann.dark_y - ann.dark_height/2) * im_height
		left = (ann.dark_x - ann.dark_width/2) * im_width
		width = ann.dark_width * im_width
		height = ann.dark_height * im_height

		class_color = class_colors[ann.class_num]
		# print("class_names:\n{}".format(class_names), file=sys.stderr) #!#
		# print("ann.class_num:", ann.class_num, file=sys.stderr) #!#
		class_name = class_names[ann.class_num]

		rect = plt.Rectangle((left, top), width, height,
			fill=False, edgecolor=class_color, linewidth=2#3.5
		)
		ax.add_patch(rect)

		ax.text(left, top - 2,
				'{:s}'.format(class_name),
				bbox={'facecolor': class_color, 'alpha':0.5},
				fontsize=14, color='white')

	# #!#
	# rect = plt.Rectangle((10, 10), im_width-20, im_height-20,
	# 	fill=False, edgecolor='purple', linewidth=3.5
	# )
	# ax.add_patch(rect)

	plt.show()

def main():
	args = parse_args()

	print('Showing some {} random images.'.format(args.k_images))
	print('Using seed {}.'.format(args.seed))
	random.seed(args.seed)

	base_dir = os.getcwd()

	# Read the `.names`.
	with open(args.dot_names, 'r') as f:
		class_names = [ l.strip() for l in f.readlines() if not re.match(r'^\s+$', l) ]
	n_classes = len(class_names)
	class_colors = COLORS[args.colors]
	if type(class_colors) is matplotlib.colors.LinearSegmentedColormap:
		class_colors = [ class_colors(x) for x in np.linspace(0, 1, n_classes+1) ]

	# Read the `.data`.
	config = parse_ini(args.dot_data)

	# Get list of images for the dataset.
	image_list = []
	if 'train' in config:
		with open(config['train'], 'r') as f:
			train_images = [ os.path.join(base_dir, l.strip()) for l in f.readlines() ]
		image_list += train_images
	if 'valid' in config:
		with open(config['valid'], 'r') as f:
			valid_images = [ os.path.join(base_dir, l.strip()) for l in f.readlines() ]
		image_list += valid_images

	sample_images = random.sample(image_list, k=args.k_images)
	sample_labels = [ re.sub(r'images/(.+)\.(png|jpg)', r'labels/\1.txt', p) for p in sample_images ]

	for image_path, labels_path in zip(sample_images, sample_labels):
		print('Image `{}`.'.format(image_path))
		print('Labels `{}`.'.format(labels_path))
		print()
		plot_image_annotations(image_path, labels_path, class_names, class_colors=class_colors)

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('dot_data')
	parser.add_argument('dot_names')
	parser.add_argument('-k', '--k-images', type=int, default=5, help='Show only k images. (default=5)')
	parser.add_argument('-s', '--seed', type=int, default=7, help='Seed for the random stuff')
	parser.add_argument('-c', '--colors', type=lambda x: x.upper(), choices=COLORS, default='HSV',
		help='Which colors should be used to draw the bounding boxes.')
	args = parser.parse_args()
	return args

if __name__ == '__main__':
	main()
