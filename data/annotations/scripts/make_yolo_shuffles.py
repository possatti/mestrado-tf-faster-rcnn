#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from collections import OrderedDict

import argparse
import random
import sys
import re
import os

def parse_data_file(data_file_path):
	data = OrderedDict()
	with open(data_file_path, 'r') as f:
		lines = [ re.sub(r'#.*', '', l) for l in f.readlines() ]
		lines = [ l.split('=') for l in lines if not re.match(r'^\s*$', l) ]
		for l in lines:
			data[l[0].strip()] = l[1].strip()
	return data

def write_data_file(data_file_path, data_d):
	with open(data_file_path, 'w') as f:
		for key in data_d:
			f.write('{} = {}\n'.format(key, data_d[key]))

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('data_file')
	parser.add_argument('dest_dir')
	parser.add_argument('-n', '--n-shuffles', type=int, default=10)
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	data_file_basename, _ = os.path.splitext(os.path.basename(args.data_file))
	original_data = parse_data_file(args.data_file)
	train_txt = original_data['train']
	with open(train_txt, 'r') as f:
		train_images = f.readlines()

	if not os.path.isdir(args.dest_dir):
		os.makedirs(args.dest_dir)

	for i in range(args.n_shuffles):
		shuffled_txt_path = os.path.abspath(os.path.join(args.dest_dir, '{}_shuffle_{}.txt'.format(data_file_basename, i)))
		shuffled_data_path = os.path.abspath(os.path.join(args.dest_dir, '{}_shuffle_{}.data'.format(data_file_basename, i)))
		random.seed(i)
		shuffled_images = random.sample(train_images, len(train_images))
		shuffled_data = original_data.copy()
		shuffled_data['train'] = shuffled_txt_path
		shuffled_data['backup'] = os.path.join(original_data['backup'], 'shuffle_{}'.format(i))
		if not os.path.isdir(shuffled_data['backup']):
			os.makedirs(shuffled_data['backup'])
		write_data_file(shuffled_data_path, shuffled_data)
		with open(shuffled_txt_path, 'w') as f:
			f.writelines(shuffled_images)


if __name__ == '__main__':
	main()
