#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import argparse
import sys
import re
import os


REGIONS_RE = re.compile(r'REGIONS/\w+/\d{3}/')
TEST_SET_RE = re.compile(r'REGIONS/(Belo_Horizonte/002|Belo_Horizonte/000|Belo_Horizonte/001|Belo_Horizonte/003|Brasilia/002|Brasilia/000|Brasilia/001|Sao_Paulo/006|Sao_Paulo/013|Sao_Paulo/001|Sao_Paulo/003|Sao_Paulo/019)/')

SETS = [
	'train',
	'test',
]


def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('set', choices=SETS)
	parser.add_argument('-i', '--input-file')
	parser.add_argument('-o', '--output-file')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	input_file = sys.stdin
	if args.input_file is not None:
		input_file = open(args.input_file, 'r')

	output_file = sys.stdout
	if args.output_file is not None:
		output_file = open(args.output_file, 'r')

	for line in input_file.readlines():
		if REGIONS_RE.search(line):
			belongs_to_test = TEST_SET_RE.search(line) is not None
			if (belongs_to_test and args.set == 'test') or \
				(not belongs_to_test and args.set == 'train'):
				output_file.write(line)

if __name__ == '__main__':
	main()

