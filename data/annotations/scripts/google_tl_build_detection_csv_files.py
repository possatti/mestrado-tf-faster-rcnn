#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from collections import namedtuple

import pandas as pd
import argparse
import json
import sys
import re
import os

print(sys.version, file=sys.stderr)

SCRIPT_DIR = os.path.dirname(__file__)

TL_STATUSES = {
	'NRG': {
		1: 'Red',
		2: 'Green',
	},
	'NRGY': {
		1: 'Red',
		2: 'Green',
		3: 'Yellow',
	},
}

TEST_SET_RE = re.compile(r'(Belo_Horizonte/002|Belo_Horizonte/000|Belo_Horizonte/001|Belo_Horizonte/003|Brasilia/002|Brasilia/000|Brasilia/001|Sao_Paulo/006|Sao_Paulo/013|Sao_Paulo/001|Sao_Paulo/003|Sao_Paulo/019)')

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('json_annotations')
	parser.add_argument('labels', choices=TL_STATUSES)
	parser.add_argument('save_dir')
	parser.add_argument('-f', '--force', action='store_true')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	if not os.path.isdir(args.save_dir):
		os.makedirs(args.save_dir)

	train_csv_path = os.path.join(args.save_dir, 'google_tl_{}_train.csv'.format(args.labels.lower()))
	test_csv_path = os.path.join(args.save_dir, 'google_tl_{}_test.csv'.format(args.labels.lower()))
	needs_force = False
	if os.path.isfile(train_csv_path):
		print('The file `{}` already exists.'.format(train_csv_path))
		needs_force = True
	if os.path.isfile(test_csv_path):
		print('The file `{}` already exists.'.format(test_csv_path))
		needs_force = True
	if needs_force and not args.force:
		print('If you want to overwrite the files, use the `--force` option.')
		exit(1)

	with open(args.json_annotations, 'r') as f:
		data = json.load(f)

	colnames = 'path x1 y1 x2 y2 class_name'.split()
	coltypes = [str, int, int, int, int, str]

	train_rows = { name: [] for name in colnames }
	test_rows = { name: [] for name in colnames }

	tl_statuses = TL_STATUSES[args.labels]

	print(file=sys.stderr)
	for i, entry in enumerate(data):
		print('\x1b[A\x1b[2KINFO: {} of {}'.format(i+1, len(data)), file=sys.stderr)
		for tl in entry['traffic_lights']:
			# Filter in only the desired status.
			if tl['status'] in tl_statuses:
				# Split train and test.
				if TEST_SET_RE.search(entry['filepath']):
					rows = test_rows
				else:
					rows = train_rows
				rows['path'].append(entry['filepath'])
				rows['x1'].append(tl['bbox'][0][0])
				rows['y1'].append(tl['bbox'][0][1])
				rows['x2'].append(tl['bbox'][1][0])
				rows['y2'].append(tl['bbox'][1][1])
				rows['class_name'].append(tl_statuses[tl['status']])

	df_train = pd.DataFrame(train_rows, columns=colnames)
	df_test = pd.DataFrame(test_rows, columns=colnames)

	df_train.to_csv(train_csv_path, index=False, header=False)
	df_test.to_csv(test_csv_path, index=False, header=False)

if __name__ == '__main__':
	main()
