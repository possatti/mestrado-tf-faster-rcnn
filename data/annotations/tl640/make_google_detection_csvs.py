#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from subprocess import call

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse
import json
import sys
import os
import re

print(sys.version, file=sys.stderr)

RE_TEST_SET = re.compile(r'REGIONS/(Belo_Horizonte/002|Belo_Horizonte/000|Belo_Horizonte/001|Belo_Horizonte/003|Brasilia/002|Brasilia/000|Brasilia/001|Sao_Paulo/006|Sao_Paulo/013|Sao_Paulo/001|Sao_Paulo/003|Sao_Paulo/019)/')
RE_TL_PATH = re.compile(r'REGIONS/(?P<region>\w+)/(?P<subregion>\d{3})/(?P<name>.+)\.jpg')
COLNAMES = ['path', 'x1', 'y1', 'x2', 'y2', 'class_name']

DETECTION_MAPS = {
	'NRG':  {1:'Red', 2:'Green'},
	'NRGY': {1:'Red', 2:'Green', 3:'Yellow'},
	'NRGR': {1:'Red', 2:'Green', 3:'Red'},
}

def parse_args():
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('labels', choices=['NRG', 'NRGY', 'NRGR'])
	parser.add_argument('--tl-annotations', default='/dados/datasets/traffic-lights/google-tl/tl-annotations.json')
	parser.add_argument('--base-dir', default='/dados/datasets/tl640')
	parser.add_argument('--save-train')
	parser.add_argument('--save-test')
	args = parser.parse_args()
	return args

def main():
	args = parse_args()

	with open(args.tl_annotations, 'r') as f:
		data = json.load(f)

	missing_images = []
	detections = {
		'train': { c: [] for c in COLNAMES },
		'test': { c: [] for c in COLNAMES },
	}
	for i, entry in enumerate(data):
		set_ = 'test' if RE_TEST_SET.search(entry['filepath']) else 'train'
		match = RE_TL_PATH.match(entry['filepath'])
		if match:
			g = match.groupdict()
			tl640_path = 'Google_automatics/REGIONS/{region}/{subregion}/IMG/SIGNAL/{name}.png'.format(**g)
		else:
			raise ValueError('filepath doesn\'t match: `{}`'.format(entry['filepath']))

		if os.path.isfile(os.path.join(args.base_dir, tl640_path)):
			for tl in entry['traffic_lights']:
				if tl['status'] in DETECTION_MAPS[args.labels]:
					detections[set_]['path'].append(tl640_path)
					detections[set_]['x1'].append(tl['bbox'][0][0])
					detections[set_]['y1'].append(tl['bbox'][0][1])
					detections[set_]['x2'].append(tl['bbox'][1][0])
					detections[set_]['y2'].append(tl['bbox'][1][1])
					detections[set_]['class_name'].append(DETECTION_MAPS[args.labels][tl['status']])
		else:
			missing_images.append(tl640_path)
		print('INFO: {} of {}.\r'.format(i+1, len(data)), end='', file=sys.stderr)
	print(file=sys.stderr)


	df_train = pd.DataFrame(detections['train'])
	df_test = pd.DataFrame(detections['test'])
	if args.save_train:
		df_train.to_csv(args.save_train, header=False, columns=COLNAMES, index=False)
	else:
		print(df_train)
	if args.save_test:
		df_test.to_csv(args.save_test, header=False, columns=COLNAMES, index=False)
	else:
		print(df_test)

	print('Training images: {}'.format(len(df_train['path'].unique())), file=sys.stderr)
	print('Testing images: {}'.format(len(df_test['path'].unique())), file=sys.stderr)
	print('Missing images: {}'.format(len(missing_images)), file=sys.stderr)

if __name__ == '__main__':
	main()
